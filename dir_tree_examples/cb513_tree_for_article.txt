.
├── CASP13_cross_validation_for_CB513
│   ├── fold0
│   │   └── pred_casp13_fold0_ex1.txt
│   ├── fold1
│   │   └── pred_casp13_fold1_ex5.txt
│   ├── fold2
│   │   └── pred_casp13_fold2_ex8.txt
│   ├── fold3
│   │   └── pred_casp13_fold3_ex9.txt
│   ├── fold4
│   │   └── pred_casp13_fold4_ex10.txt
│   ├── fold5
│   │   └── pred_casp13_fold5_ex10.txt
│   ├── fold6
│   │   └── pred_casp13_fold6_ex10.txt
│   ├── fold7
│   │   └── pred_casp13_fold7_ex10.txt
│   ├── fold8
│   │   └── pred_casp13_fold8_ex10.txt
│   └── fold9
│       └── pred_casp13_fold9_ex10.txt
├── CASP13_pred_for_CB513
│   ├── fold0
│   │   ├── pred_casp13_fold0_ex1.txt
│   │   ├── pred_casp13_fold0_ex10.txt
│   │   ├── pred_casp13_fold0_ex2.txt
│   │   ├── pred_casp13_fold0_ex3.txt
│   │   ├── pred_casp13_fold0_ex4.txt
│   │   ├── pred_casp13_fold0_ex5.txt
│   │   ├── pred_casp13_fold0_ex6.txt
│   │   ├── pred_casp13_fold0_ex7.txt
│   │   ├── pred_casp13_fold0_ex8.txt
│   │   └── pred_casp13_fold0_ex9.txt
│   ├── fold1
│   │   ├── pred_casp13_fold1_ex1.txt
│   │   ├── pred_casp13_fold1_ex10.txt
│   │   ├── pred_casp13_fold1_ex2.txt
│   │   ├── pred_casp13_fold1_ex3.txt
│   │   ├── pred_casp13_fold1_ex4.txt
│   │   ├── pred_casp13_fold1_ex5.txt
│   │   ├── pred_casp13_fold1_ex6.txt
│   │   ├── pred_casp13_fold1_ex7.txt
│   │   ├── pred_casp13_fold1_ex8.txt
│   │   └── pred_casp13_fold1_ex9.txt
│   ├── fold2
│   │   ├── pred_casp13_fold2_ex1.txt
│   │   ├── pred_casp13_fold2_ex10.txt
│   │   ├── pred_casp13_fold2_ex2.txt
│   │   ├── pred_casp13_fold2_ex3.txt
│   │   ├── pred_casp13_fold2_ex4.txt
│   │   ├── pred_casp13_fold2_ex5.txt
│   │   ├── pred_casp13_fold2_ex6.txt
│   │   ├── pred_casp13_fold2_ex7.txt
│   │   ├── pred_casp13_fold2_ex8.txt
│   │   └── pred_casp13_fold2_ex9.txt
│   ├── fold3
│   │   ├── pred_casp13_fold3_ex1.txt
│   │   ├── pred_casp13_fold3_ex10.txt
│   │   ├── pred_casp13_fold3_ex2.txt
│   │   ├── pred_casp13_fold3_ex3.txt
│   │   ├── pred_casp13_fold3_ex4.txt
│   │   ├── pred_casp13_fold3_ex5.txt
│   │   ├── pred_casp13_fold3_ex6.txt
│   │   ├── pred_casp13_fold3_ex7.txt
│   │   ├── pred_casp13_fold3_ex8.txt
│   │   └── pred_casp13_fold3_ex9.txt
│   ├── fold4
│   │   ├── pred_casp13_fold4_ex1.txt
│   │   ├── pred_casp13_fold4_ex10.txt
│   │   ├── pred_casp13_fold4_ex2.txt
│   │   ├── pred_casp13_fold4_ex3.txt
│   │   ├── pred_casp13_fold4_ex4.txt
│   │   ├── pred_casp13_fold4_ex5.txt
│   │   ├── pred_casp13_fold4_ex6.txt
│   │   ├── pred_casp13_fold4_ex7.txt
│   │   ├── pred_casp13_fold4_ex8.txt
│   │   └── pred_casp13_fold4_ex9.txt
│   ├── fold5
│   │   ├── pred_casp13_fold5_ex1.txt
│   │   ├── pred_casp13_fold5_ex10.txt
│   │   ├── pred_casp13_fold5_ex2.txt
│   │   ├── pred_casp13_fold5_ex3.txt
│   │   ├── pred_casp13_fold5_ex4.txt
│   │   ├── pred_casp13_fold5_ex5.txt
│   │   ├── pred_casp13_fold5_ex6.txt
│   │   ├── pred_casp13_fold5_ex7.txt
│   │   ├── pred_casp13_fold5_ex8.txt
│   │   └── pred_casp13_fold5_ex9.txt
│   ├── fold6
│   │   ├── pred_casp13_fold6_ex1.txt
│   │   ├── pred_casp13_fold6_ex10.txt
│   │   ├── pred_casp13_fold6_ex2.txt
│   │   ├── pred_casp13_fold6_ex3.txt
│   │   ├── pred_casp13_fold6_ex4.txt
│   │   ├── pred_casp13_fold6_ex5.txt
│   │   ├── pred_casp13_fold6_ex6.txt
│   │   ├── pred_casp13_fold6_ex7.txt
│   │   ├── pred_casp13_fold6_ex8.txt
│   │   └── pred_casp13_fold6_ex9.txt
│   ├── fold7
│   │   ├── pred_casp13_fold7_ex1.txt
│   │   ├── pred_casp13_fold7_ex10.txt
│   │   ├── pred_casp13_fold7_ex2.txt
│   │   ├── pred_casp13_fold7_ex3.txt
│   │   ├── pred_casp13_fold7_ex4.txt
│   │   ├── pred_casp13_fold7_ex5.txt
│   │   ├── pred_casp13_fold7_ex6.txt
│   │   ├── pred_casp13_fold7_ex7.txt
│   │   ├── pred_casp13_fold7_ex8.txt
│   │   └── pred_casp13_fold7_ex9.txt
│   ├── fold8
│   │   ├── pred_casp13_fold8_ex1.txt
│   │   ├── pred_casp13_fold8_ex10.txt
│   │   ├── pred_casp13_fold8_ex2.txt
│   │   ├── pred_casp13_fold8_ex3.txt
│   │   ├── pred_casp13_fold8_ex4.txt
│   │   ├── pred_casp13_fold8_ex5.txt
│   │   ├── pred_casp13_fold8_ex6.txt
│   │   ├── pred_casp13_fold8_ex7.txt
│   │   ├── pred_casp13_fold8_ex8.txt
│   │   └── pred_casp13_fold8_ex9.txt
│   └── fold9
│       ├── pred_casp13_fold9_ex1.txt
│       ├── pred_casp13_fold9_ex10.txt
│       ├── pred_casp13_fold9_ex2.txt
│       ├── pred_casp13_fold9_ex3.txt
│       ├── pred_casp13_fold9_ex4.txt
│       ├── pred_casp13_fold9_ex5.txt
│       ├── pred_casp13_fold9_ex6.txt
│       ├── pred_casp13_fold9_ex7.txt
│       ├── pred_casp13_fold9_ex8.txt
│       └── pred_casp13_fold9_ex9.txt
├── CASP13_runAll.sh
├── CB513_cross_validation
│   ├── fold0
│   │   └── pred_test_fold0.txt
│   ├── fold1
│   │   └── pred_test_fold1.txt
│   ├── fold2
│   │   └── pred_test_fold2.txt
│   ├── fold3
│   │   └── pred_test_fold3.txt
│   ├── fold4
│   │   └── pred_test_fold4.txt
│   ├── fold5
│   │   └── pred_test_fold5.txt
│   ├── fold6
│   │   └── pred_test_fold6.txt
│   ├── fold7
│   │   └── pred_test_fold7.txt
│   ├── fold8
│   │   └── pred_test_fold8.txt
│   └── fold9
│       └── pred_test_fold9.txt
├── CB513_runAll.sh
├── CB513_test_pred
│   ├── fold0
│   │   ├── pred_test_fold0_ex1.txt
│   │   ├── pred_test_fold0_ex10.txt
│   │   ├── pred_test_fold0_ex2.txt
│   │   ├── pred_test_fold0_ex3.txt
│   │   ├── pred_test_fold0_ex4.txt
│   │   ├── pred_test_fold0_ex5.txt
│   │   ├── pred_test_fold0_ex6.txt
│   │   ├── pred_test_fold0_ex7.txt
│   │   ├── pred_test_fold0_ex8.txt
│   │   └── pred_test_fold0_ex9.txt
│   ├── fold1
│   │   ├── pred_test_fold1_ex1.txt
│   │   ├── pred_test_fold1_ex10.txt
│   │   ├── pred_test_fold1_ex2.txt
│   │   ├── pred_test_fold1_ex3.txt
│   │   ├── pred_test_fold1_ex4.txt
│   │   ├── pred_test_fold1_ex5.txt
│   │   ├── pred_test_fold1_ex6.txt
│   │   ├── pred_test_fold1_ex7.txt
│   │   ├── pred_test_fold1_ex8.txt
│   │   └── pred_test_fold1_ex9.txt
│   ├── fold2
│   │   ├── pred_test_fold2_ex1.txt
│   │   ├── pred_test_fold2_ex10.txt
│   │   ├── pred_test_fold2_ex2.txt
│   │   ├── pred_test_fold2_ex3.txt
│   │   ├── pred_test_fold2_ex4.txt
│   │   ├── pred_test_fold2_ex5.txt
│   │   ├── pred_test_fold2_ex6.txt
│   │   ├── pred_test_fold2_ex7.txt
│   │   ├── pred_test_fold2_ex8.txt
│   │   └── pred_test_fold2_ex9.txt
│   ├── fold3
│   │   ├── pred_test_fold3_ex1.txt
│   │   ├── pred_test_fold3_ex10.txt
│   │   ├── pred_test_fold3_ex2.txt
│   │   ├── pred_test_fold3_ex3.txt
│   │   ├── pred_test_fold3_ex4.txt
│   │   ├── pred_test_fold3_ex5.txt
│   │   ├── pred_test_fold3_ex6.txt
│   │   ├── pred_test_fold3_ex7.txt
│   │   ├── pred_test_fold3_ex8.txt
│   │   └── pred_test_fold3_ex9.txt
│   ├── fold4
│   │   ├── pred_test_fold4_ex1.txt
│   │   ├── pred_test_fold4_ex10.txt
│   │   ├── pred_test_fold4_ex2.txt
│   │   ├── pred_test_fold4_ex3.txt
│   │   ├── pred_test_fold4_ex4.txt
│   │   ├── pred_test_fold4_ex5.txt
│   │   ├── pred_test_fold4_ex6.txt
│   │   ├── pred_test_fold4_ex7.txt
│   │   ├── pred_test_fold4_ex8.txt
│   │   └── pred_test_fold4_ex9.txt
│   ├── fold5
│   │   ├── pred_test_fold5_ex1.txt
│   │   ├── pred_test_fold5_ex10.txt
│   │   ├── pred_test_fold5_ex2.txt
│   │   ├── pred_test_fold5_ex3.txt
│   │   ├── pred_test_fold5_ex4.txt
│   │   ├── pred_test_fold5_ex5.txt
│   │   ├── pred_test_fold5_ex6.txt
│   │   ├── pred_test_fold5_ex7.txt
│   │   ├── pred_test_fold5_ex8.txt
│   │   └── pred_test_fold5_ex9.txt
│   ├── fold6
│   │   ├── pred_test_fold6_ex1.txt
│   │   ├── pred_test_fold6_ex10.txt
│   │   ├── pred_test_fold6_ex2.txt
│   │   ├── pred_test_fold6_ex3.txt
│   │   ├── pred_test_fold6_ex4.txt
│   │   ├── pred_test_fold6_ex5.txt
│   │   ├── pred_test_fold6_ex6.txt
│   │   ├── pred_test_fold6_ex7.txt
│   │   ├── pred_test_fold6_ex8.txt
│   │   └── pred_test_fold6_ex9.txt
│   ├── fold7
│   │   ├── pred_test_fold7_ex1.txt
│   │   ├── pred_test_fold7_ex10.txt
│   │   ├── pred_test_fold7_ex2.txt
│   │   ├── pred_test_fold7_ex3.txt
│   │   ├── pred_test_fold7_ex4.txt
│   │   ├── pred_test_fold7_ex5.txt
│   │   ├── pred_test_fold7_ex6.txt
│   │   ├── pred_test_fold7_ex7.txt
│   │   ├── pred_test_fold7_ex8.txt
│   │   └── pred_test_fold7_ex9.txt
│   ├── fold8
│   │   ├── pred_test_fold8_ex1.txt
│   │   ├── pred_test_fold8_ex10.txt
│   │   ├── pred_test_fold8_ex2.txt
│   │   ├── pred_test_fold8_ex3.txt
│   │   ├── pred_test_fold8_ex4.txt
│   │   ├── pred_test_fold8_ex5.txt
│   │   ├── pred_test_fold8_ex6.txt
│   │   ├── pred_test_fold8_ex7.txt
│   │   ├── pred_test_fold8_ex8.txt
│   │   └── pred_test_fold8_ex9.txt
│   └── fold9
│       ├── pred_test_fold9_ex1.txt
│       ├── pred_test_fold9_ex10.txt
│       ├── pred_test_fold9_ex2.txt
│       ├── pred_test_fold9_ex3.txt
│       ├── pred_test_fold9_ex4.txt
│       ├── pred_test_fold9_ex5.txt
│       ├── pred_test_fold9_ex6.txt
│       ├── pred_test_fold9_ex7.txt
│       ├── pred_test_fold9_ex8.txt
│       └── pred_test_fold9_ex9.txt
├── CB513_train_pred
│   ├── pred_train_fold0_ex1.txt
│   ├── pred_train_fold1_ex1.txt
│   ├── pred_train_fold2_ex1.txt
│   ├── pred_train_fold3_ex1.txt
│   ├── pred_train_fold4_ex1.txt
│   ├── pred_train_fold5_ex1.txt
│   ├── pred_train_fold6_ex1.txt
│   ├── pred_train_fold7_ex1.txt
│   ├── pred_train_fold8_ex1.txt
│   └── pred_train_fold9_ex1.txt
├── CB513_view_results.sh
├── build_dir_tree.sh
├── final_results_CASP13_for_CB513.txt
├── final_results_CB513.txt
└── q3_sov_scripts
    ├── afterSOV.py
    ├── calc_Q3.py
    ├── ensembles.py
    ├── externalRules.py
    ├── prepare_SVM_files.py
    ├── runSOV.c
    ├── sov.c
    └── train_SVM.py

46 directories, 244 files
