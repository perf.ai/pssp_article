> 1EDNA_1-21
CSCSSLMDKECVYFCHLDIIW
CCCCCCCHHHCCCCCCCCCCC
CCCCCCCCCEEEEEEHHCEEC
> 1HUPA_88-111
AASERKALQTEMARIKKWLTFSLG
CCHHHHHHHHHHHHHHHHHHHHCC
CCCHHHHHHHHHHHHHHHHEECCC
> 2MLTB_1-26
GIGAVLKVLTTGLPALISWIKRKRQQ
CHHHHHHHHHHHHHHHHHHHHHHHHC
CCCEEEEEHHCCCHHHHHHHHHCCCC
> 1MRTA_31-61
KSCCSCCPVGCAKCSQGCICKEASDKCSCCA
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
> 9APIB_359-394
SIPPEVKFNKPFVFLMIEQNTKSPLFMGKVVNPTQK
CCCCEEECCCCEEEEEEECCCCCEEEEEEECCCCCC
CCCCEEECCCCEEEEEECCCCCCEEEEECCCCCCCC
> 1EDMC_46-84
VDGDQCESNPCLNGGSCKDDINSYECWCPFGFEGKNCEL
CCCCCCCCCCCCCCCEEEECCCCEEEECCCCCCCCCCCC
CCCCCCCCCCCCCCCEEECCCCCEEEECCCCCCCCCCCC
> 1BDSA_1-43
AAPCFCSGKPGRGDLWILRGTCPGGYGYTSNCYKWPNICCYPH
CCCCCCCCCCCCCEEECCCCCCCCCCCCCCEEEECCEEEECCC
CCCCECCCCCCCCCEEEECCCCCCCCCEECCCCCCCCEECCCC
> 1GLNA_323-370
SLEEVAERVKPFLREAGLSWESEAYLRRAVELMRPRFDTLKEFPEKAR
CHHHHHHHCHHHHHHCCCCCCCHHHHHHHHHHHHHHCCCHHHHHHHCH
CHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHEHCCHHCEHHCCCCCCC
> 1MOFA_46-98
DLREVEKSISNLEKSLTSLSEVVLQNRRGLDLLFLKEGGLCAALKEECAFYAD
CHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCHHHCCHHHHHCCCCCCCCC
CHHHHHHHHHHHHHHHHHHHHHHHHHHHCHCHHHHHCCCEEECCCCEEEEECC
> 1RPOA_1-61
MTKQEKTALNMARFIRSQTLTLLEKLNELADAADEQADICESLHDHADELYRSCLARFGDD
CCHHHHHHHHHHHHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCC
CCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCC
> 1PTXA_1-64
VKDGYIVDDVNCTYFCGRNAYCNEECTKLKGESGYCQWASPYGNACYCYKLPDHVRTKGPGRCH
CEEEEEECCCCCECCCCCHHHHHHHHHHCCCCEEEEEEEECCEEEEEEEEECCCCCECCCCCCC
CCCCEEECCCCCEEEECCCCCCCCCCCCCCCCCCEEECCCCCCCEEEECCCCCCCCEECCCCCC
> 1CC5A_5-87
GGGARSGDDVVAKYCNACHGTGLLNAPKVGDSAAWKTRADAKGGLDGLLAQSLSGLNAMPPKGTCADCSDDELKAAIGKMSGL
CCCCCCCHHHHHHCCHHHHCCCCCCCCCCCCHHHHHHHHHHHCCCCCCHHHHHHCECCECCCCCCCCCCHHHHHHHHHHHHCC
CCCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCHCHCHHHHHHCCHHHHHHHHHCCCCCCCCCCCCCCCCHHHHHHHHHHHHCC
> 1ISAB_1-83
SFELPALPYAKDALAPHISAETIEYHYGKHHQTYVTNLNNLIKGTAFEGKSLEEIIRSSEGGVFNNAAQVWNHTFYWNCLAPN
CCCCCCCCCCCCCCCCCCCHHHHHHHCCCHHHHHHHHHHHHHCCCHHHHCCHHHHHHHCCHHHHHHHHHHHHHHHHHHCECCC
CCCCCCCCCCHCCCCHHCHHHHEEEEHCCCHHHHHHHHHHHHHCHHHHCCCHHHHHHHHCCCHHECCCCCHHHHHHHHHCCCC
> 1CEIA_3-87
LKNSISDYTEAEFVQLLKEIEKENVAATDDVLDVLLEHFVKITEHPDGTDLIYYPSDNRDDSPEGIVKEIKEWRAANGKPGFKQG
CCCCHHHCEHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHCCCCCCHHHHCCCCCCCCCHHHHHHHHHHHHHHCCCCCCECC
CCCCHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHEECCCCCCCEEEECCCCCCCCCCCEEEEHHHHHHHCCCCCCCCC
> 1LPBA_6-90
GIIINLDEGELCLNSAQCKSNCCQHDTILSLSRCALKARENSECSAFTLYGVYYKCPCERGLTCEGDKSLVGSITNTNFGICHNV
CCCCCECECCECCCHHHECCCCEECCCCCCCCEECCCECCCCEEECCCCCCEECCCCECCCEEEECCCCHHHHHHCCCEEEEEEC
CEEEECCCCCEHCCCCCCCCCCCCCCCCCCHHHHHHCCCCCCCCCCEEEEEEEEHCCCCCCCEECCCCEEEEEEECCCCCEEECC
> 1CYOA_3-87
AVKYYTLEEIQKHNNSKSTWLILHYKVYDLTKFLEEHPGGEEVLREQAGGDATENFEDVGHSTDARELSKTFIIGELHPDDRSKI
CCCEECHHHHCCCEECCEEEEEECCEEEECCCCCCCCCCCCHHHHHHCCCECHHHHHHCCCCHHHHHHHCCCEEEEECHHHHHHC
CCCEECHHHHHHHCCCCCEEEEEECEEEEECCCHHHCCCCCHHHHHHCCCCCHHHHHHCCCCHHHHHHHHHHECCEECCCCCCCC
> 1SVBA_300-395
KGLTYTMCDKTKFTWKRAPTDSGHDTVVMEVTFSGTKPCRIPVRAVAHGSPDVNVAMLITPNPTIENNGGGFIEMQLPPGDNIIYVGELSHQWFQK
CCCCCCECCCCCEEEEEEEEECCCCCEEEEEEECCCCCEECCEEEEECCECCCCCCEECCCCCEEECCEECEEEEECCCEEEEEEECCEEEEEEEC
CCCEEEECCCCCEEEEECCCCCCCCEEEEEEEECCCCCCEEEEEEECCCCCCCCCEEEEECCCEEECCCCEEEEECCCCCCCEEEECCHHEHECCC
> 1STFI_6-125
MMSGAPSATQPATAETQHIADQVRSQLEEKYNKKFPVFKAVSFKSQVVAGTNYFIKVHVGDEDFVHLRVFQSLPHENKPLTLSNYQTNKAKHDELTYF
CCECCCCCCEECCHHHHHHHHCCHHHHHHHHCCCCCCCEEEEEEEEECCEEEEEEEEECCCCCEEEEEEEEECHHHCCCCEEEEEEEEECCCCCCCCC
CCCCCCCCCCCCCHHHHHHHHHHHHHHHHHCCCCHHEEEEEEEEEEEECCCEEEEEEECCCCCEEEEEEECCCCCCCCCCEEEEECCCCCCCCHECCC
> 1HNFA_4-104
TNALETWGALGQDINLDIPSFQMSDDIDDIKWEKTSDKKKIAQFRKEKETFKEKDTYKLFKNGTLKIKHLKTDDQDIYKVSIYDTKGKNVLEKIFDLKIQE
CCCEEEEEECCCCEEECCCCCCCCCCEEEEEEEEHHHCCEEEEECHHHCEEECCCCEEECCCCCEEECCCCHHHCEEEEEEEEECCCCEEEEEEEEEEEEC
CCCEEEEEHCCCCCCCCCCCCCCCCHHHHHHHECCCCCCEEHHHHHCCCCCCCCCHEEEHCCCCHHHHHHHCCCCCCEEEEEECCCCCHEHHHHHHHEHCC
> 2HFTA_107-211
NLGQPTIQSFEQVGTKVNVTVEDERTLVRRNNTFLSLRDVFGKDLIYTLYYWKSSGKKTAKTNTNEFLIDVDKGENYCFSVQAVIPSRTVNRKSTDSPVECMG
ECCCCCEEEEEEECCEEEEEECCCEEEEEECCEEEEHHHHHHHHCEEEEEEECCCCCEEEEECCCEEEEECCCCCCCEEEEEEECCCCCCCCECCCCCCEECC
CCCCCCEEEEECCCCEEEEEEECCCCCCCCCCCEEEHHHHHHHCHHEEEEEECCCCCCEEECCCCEEEEECCCCCCEEEEEEEEECCHHHCCCCCCCHHEECC
> 1ILKA_10-115
NSCTHFPGNLPNMLRDLRDAFSRVKTFFQMKDQLDNLLLKESLLEDFKGYLGCQALSEMIQFYLEEVMPQAENQDPDIKAHVNSLGENLKTLRLRLRRCHRFLPCE
CCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCHHHHHHHHCCCHHHHHHHHHHHHHHCCHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHCCCCCHHH
CCCCCCCCCHHHHHHHHHHHHHHHHHHHEHHCCCCCHHCHHHHHHCCCCCCHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCHCHHHHHHHHHHHHHHHHCCCCCCC
> 1NOLA_1-115
TLHDKQIRICHLFEQLSSATVHSDRLKNVGKLQPGAIFSCFHPDHLEEARHLYEVFWEAGDFNDFIEIAKEARTFVNEGLFAFAAEVAVLHRDDCKGLYVPPVQEIF
CHHHHHHHHHHHCCCCCCCCCCCHHHCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHCHHHCCCCCCCHHHHC
CCCHHHHHHHHHHCCCCCCCCCHHHHHCCCCCCCCCEEEECCHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCCHHEEEEEEEEEEHCCCCCCCECCCHHECC
> 1CTNA_24-132
AAPGKPTIAWGNTKFAIVEVDQAATAYNNLVKVKNAADVSVSWNLWNGDTGTTAKILLNGKEAWSGPSTGSSGTANFKVNKGGRYQMQVALCNADGCTASDATEIVVAD
CCCCCCEECCCCCEEECEEECCCCCCHHHHEEECCCEEEEEEEECCCCCCCCEEEEEECCEEEEEEECCCCEEEEEEEECCCEEEEEEEEEEECCEEEECCCEEEEEEC
CCCCCCEECHCCCCEEEEEECCHHHHHHHHEEECCCCEEEEEEEEECCCCCCEEEEEECCCEECCCCCCCCCCEEEEEECCCCEEEEEEEEECCCCCCCCCCEEEEECC
> 1ECLA_280-404
PGAPFITSTLQQAASTRLGFGVKKTMMMAQRLYEAGYITYMRTDSTNLSQDAVNMVRGYISDNFGKKYLPESPNQYAHEAIRPSDVNVMAESLKDMEADAQKLYQLIWRQFVACQMT
CCCCECHHHHHHHHHHHHCCCHHHHHHHHHHHHHCCCECCCCCCCCCCCHHHHHHHHHHHHHHCCHHHCCCCCCCCCCCCECECCCCCCHHHCCCCCHHHHHHHHHHHHHHHHHCCC
CCCCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHCCCEECCCCCCCCCCHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHCCCCCHHHHHHHHHHHHHHHCCCC
> 7RSAA_1-124
KETAAAKFERQHMDSSTSAASSSNYCNQMMKSRNLTKDRCKPVNTFVHESLADVQAVCSQKNVACKNGQTNCYQSYSTMSITDCRETGSSKYPNCAYKTTQANKHIIVACEGNPYVPVHFDASV
CCCHHHHHHHHHECCCCCCCCCCCHHHHHHHHCCCCCCCCCCEEEEECCCHHHHHHHHHCEEECCCCCCCCEEECCCCEEEEEEEECCCCECCECCEEEEEEEECEEEEEECCCCEEEEEEEEC
CCCHHHHHHHHEECCCCCCCCCCCHHHHHHHHHCCCCCCCCCEEEEECCCHHHHHHECCCCCCCCCCCCCCCECCCCCEEEEEEECCCCCCCCCCCCCCCCCCCEEEEEECCCCCEEEEECCCC
> 2ENDA_2-138
TRINLTLVSELADQHLMAEYRELPRVFGAVRKHVANGKRVRDFKISPTFILGAGHVTFFYDKLEFLRKRQIELIAECLKRGFNIKDTTVQDISDIPQEFRGDYIPHEASIAISQARLDEKIAQRPTWYKYYGKAIYA
CCCCCCCHHHCCHHHHHHHHHHCCHHHHHHHHHHHCCCCHHHCCCCCCCCCCCCCCHHHCCEHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHECCCCCCHHHHHHHHHHHHHHHHHCHHHCCECCECCCC
CCEEECCHHHHHHHHHHHHHHHCHCHHHHHHHHHHCCCCCCCCCCCCCEECCCCEEEEEHHHHHHHHHHHHHHHHHHHHCCCCCCCCCECCCCCCCHCCCCCCCCCHHHHHHHHHHHHHHHHCCCCEEEECCCHEEC
> 1CPCL_33-174
DGNKRMDVVNRITGNSSTIVANAARSLFAEQPQLIAPGGNAYTSRRMAACLRDMEIILRYVTYAIFAGDASVLDDRCLNGLKETYLALGTPGSSVAVGVQKMKDAALAIAGDTNGITRGDCASLMAEVASYFDKAASAVA
CHHHHHHHHHHHHHCHHHHHHHHHHHHHHHCHHHCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHCCCCHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHHHHHCCC
CCHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCHHHHHHHHHCEEEEEEEEEEEEECCCCCCHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCC
> 2HHMB_5-146
WQECMDYAVTLARQAGEVVCEAIKNEMNVMLKSSPVDLVTATDQKVEKMLISSIKEKYPSHSFIGEESVAAGEKSILTDNPTWIIDPIDGTTNFVHRFPFVAVSIGFAVNKKIEFGVVYSCVEGKMYTARKGKGAFCNGQKL
CHHHHHHHHHHHHHHHHHHHHHCCCCCCEEEEEECCEEEEHHHHHHHHHHHHHHHHHCCCCEEEEHHHHHCCCCCCCCCCCEEEEEEEECHHHHHHCCCCCEEEEEEEECCEEEEEEEEECCCCEEEEEECCCEEEECCEEC
HHHHHHHHHHHHHHHHHHHHHHHCCCCCEEECCCCCCCEHHHHHHHHHHHHHHHHHHCCCCCEECECCCCCCCCCCCCCCEEEEECCCCCCHHHHCCCCCEEEEEEEECCCCEEEEEEECCCCCCEEEEHCCCCEEECCCCC
> 2TMVP_1-154
SYSITTPSQFVFLSSAWADPIELINLCTNALGNQFQTQQARTVVQRQFSEVWKPSPQVTVRFPDSDFKVYRYNAVLDPLVTALLGAFDTRNRIIEVENQANPTTAETLDATRRVDDATVAIRSAINNLIVELIRGTGSYNRSSFESSSGLVWTS
CCCCCCCCCHHHHCCCEECHHHHHHHHHHHCCCCCCCCCHHHHHHHHHHHCCCCCCCCCCCCCCCCCEEECCCCCHHHHHHHHHCCCCCCCCCCCCCCCCCCCHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHCCCEECHHHHHHHHCCCCCC
CCCCCCCCCEEEEHHCCCCCHHHHHHHHHCCCCHHHHHCCHHHHHHHHHHHCCCCCCCCEECCCCCEEEEEECCCCHHHHHHHHHCCCCCCCEEEECCCCCCCCCHHHHHCCCHCCHHHHHHHHHHHHHHHHCCCCEEECCCHHHHHCCCEECC
> 3MDDB_242-395
GAGFKIAMGTFDKTRPPVAAGAVGLAQRALDEATKYALERKTFGKLLAEHQGISFLLADMAMKVELARLSYQRAAWEIDSGRRNTYYASIAKAYAADIANQLATDAVQVFGGNGFNTEYPVEKLMRDAKIYQIYEGTAQIQRIIIAREHIGRYK
CCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCEECCEEHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCHHHCCECCCCCHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHCCC
CCCHHHHHHCCCCCCEEEHHHHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHEHCCCCCCHHCCHHHHHHHHHHHEECCCCHHHHHHHHHHHHHHCCC
> 1HJRD_1-158
AIILGIDPGSRVTGYGVIRQVGRQLSYLGSGCIRTKVDDLPSRLKLIYAGVTEIITQFQPDYFAIEQVFMAKNADSALKLGQARGVAIVAAVNQELPVFEYAARQVKQTVVGIGSAEKSQVQHMVRTLLKLPANPQADAADALAIAITHCHVSQNAMQ
CCEEEEECCCCCEEEEEEEEECCEEEEEEEEEECCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCCHHHHHHHHHHHHHHHHHHHHCCCCEEEEEHHHHHHHHCCCCCCCHHHCHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHHCCCCCC
CEEEECCCCCCCCCEEEEECCCCCEEEEECCEEECCCCCHHHHHHHHHHHHHHHHHHCCCCHEEEHHHHHCCCCCCHEEHHHHHHHHHHHHHHCCCCEEEECCHHHHHHECCCCCCCHHHHHHHHHHHHCCCCCCCCCHHHHHHHHHHHHHHHCCCCC
> 2CMDA_147-312
TTLDIIRSNTFVAELKGKQPGEVEVPVIGGHSGVTILPLLSQVPGVSFTEQEVADLTKRIQNAGTEVVEAKAGGGSATLSMGQAAARFGLSLVRALQGEQGVVECAYVEGDGQYARFFSQPLLLGKNGVEERKSIGTLSAFEQNALEGMLDTLKKDIALGQEFVNK
CHHHHHHHHHHHHHHHCCCHHHCCCCEEECCCCCCEEECHHHCCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCCEEEEEEECCCCCCCEEEEEEEEECCEEEEECCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHC
CCCCHHHHHHHHHHHCCCCCCCEEEEEEECCCCCCEEEEHECCCCCCCCHHHHHHHHHHHHCCHHHHHHHCCCCCCEEEHHHHHHHHHHHHHHHHHCCCCCCEEEEEECCCCCCCEEEEEEEEECCCCCEEEEECCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCC
> 1BCXA_1-185
ASTDYWQNWTDGGGIVNAVNGSGGNYSVNWSNTGNFVVGKGWTTGSPFRTINYNAGVWAPNGNGYLTLYGWTRSPLIEYYVVDSWGTYRPTGTYKGTVKSDGGTYDIYTTTRYNAPSIDGDRTTFTQYWSVRQSKRPTGSNATITFTNHVNAWKSHGMNLGSNWAYQVMATCGYQSSGSSNVTVW
CCCCEEEEEECCCCEEEEEECCCCEEEEEEECCCEEEEEEEECCCCCCCEEEEEEEEEEEECCEEEEEEEEEECCCEEEEEEEEECCCCCCCEEEEEEEECCEEEEEEEEEEEEEECCCCCEEEEEEEEEEECCCCCCCCCEEEEHHHHHHHHHHCCCCCCCEEEEEEEEEEEECCEEEEEEEEC
CCCCHHEEHCCCCCCEEEECCCCCEEEEEEECCCCEEECCCCCCCCCCCEEEEECCCCCCCCCCEEEEECCCCCCCHEEEEEECCCCCCCCCCCCCEEEECCCEEEEEEECCCCCCCHCCCCCCCEEEEEECCCCCCCCCCCEEEEHHHHHHHHHHCCCCCCCCCHEEEEEECCCCCCCCEEEEC
> 1DLCA_301-497
RDVLTDPIVGVNNLRGYGTTFSNIENYIRKPHLFDYLHRIQFHTRFQPGYYGNDSFNYWSGNYVSTRPSIGSNDIITSPFYGNKSSEPVQNLEFNGEKVYRAVANTNLAVWPSAVYSGVTKVEFSQYNDQTDEASTQTYDSKRNVGAVSWDSIDQLPPETTDEPLEKGYSHQLNYVMCFLMQGSRGTIPVLTWTHKS
CEEECCCCCCCCCCCCCCCCHHHHHCCCCCCCCCEEEEEEEEEEEEECCCCCCCCEEEEEEEEEEEEECCCCCCCEECCCEECCCCCCCEEEECCCCEEEEEEEEEEEEEECCEEEEEEEEEEEEEEECCCCEEEEEEEECCCCCCEEEEEHHHCCCCCCCCCCHHHHCCEEEEEEEEEECHHHCCEEEEEEEEECC
CEEECCCCECCCCCCCCCCCHHHHHHHCCCCCHHHHHHHEEEEECCCCCCCCCCCCEEECCCEEEEEECCCCCCCEECCCCCCCCCCCCEEEECCCCCEEEEECCHHCCCCCCCCCCCEEEEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHEEEEEECCCCCEEEEEEEECCC
> 2ALPA_15-245
ANIVGGIEYSINNASLCSVGFSVTRGATKGFVTAGHCGTVNATARIGGAVVGTFAARVFPGNDRAWVSLTSAQTLLPRVANGSSFVTVRGSTEAAVGAAVCRSGRTTGYQCGTITAKNVTANYAEGAVRGLTQGNACMGRGDSGGSWITSAGQAQGVMSGGNVQSNGNNCGIPASQRSSLFERLQPILSQYGLSLVTG
CEEEECCEEEECCCEEEECCEEEEECCEEEEEECHHHCCCCCEEEECCEEEEEEEEEECCECCEEEEEECCCCEEEEEEEECCEEEECCECCCCCCCCEEEEEECCCEEEEEEEEEEEEEEEECCEEEEEEEEECCCCECCCCCCEEECCCCEEEEEEEEECCCCCCECCCCCHHHCCEEEEEHHHHHHHHCCEECCC
CCEECCCEEEECCCCEEEEEEEECCCCCCEEEECCCCCCCCCEEEECCCCCCEEEECCCCCCCEEEEEECCCCCCCCEECCCCCCEEEECCCCCCCCCHEECCCCCCCCECCEEEECCCCECCCCCCEEEEEECCECCCCCCCCCEEECCCCCEEEEEECCCCCCCCCCCCCCCCCCCEEEECHHHHHHHCCCEEEEC
> 1G6NA_7-206
TDPTLEWFLSHCHIHKYPSKSTLIHQGEKAETLYYIVKGSVAVLIKDEEGKEMILSYLNQGDFIGELGLFEEGQERSAWVRAKTACEVAEISYKKFRQLIQVNPDILMRLSAQMARRLQVTSEKVGNLAFLDVTGRIAQTLLNLAKQPDAMTHPDGMQIKITRQEIGQIVGCSRETVGRILKMLEDQNLISAHGKTIVVY
CCHHHHHHHCCCEEEEECCCCEEECCCCECCEEEEEEECCEEEEEECCCCCEEEEEEECCCCEECCCCCCCCCCECCCEEEECCCEEEEEEEHHHHHHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHHHHHHHCCCCCCEECCCCEEECCCHHHHHHHHCCCHHHHHHHHHHHHHCCCEEEECCEEEEC
CCHHHHHHHHHHHHEECCCCCEEEECCCCCCHEEEEECCEEEEEEECCCCHHEEEEECCCCCHHHEHHHHCCCCCCCCEEEEHCHEEEEEECHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHHCCCCCCCCCEEEECCHHHHHHHCCCCHHHHHHHHHHHHHCCEEEECCCEEEEE
> 1FUAA_1-206
MERNKLARQIIDTCLEMTRLGLNQGTAGNVSVRYQDGMLITPTGIPYEKLTESHIVFIDGNGKHEEGKLPSSEWRFHMAAYQSRPDANAVVHNHAVHCTAVSILNRSIPAIHYMIAAAGGNSIPCAPYATFGTRELSEHVALALKNRKATLLQHHGLIACEVNLEKALWLAHEVEVLAQLYLTTLAITDPVPVLSDEEIAVVLEKF
CCHHHHHHHHHHHHHHHHHCCCCCCCCCEEEEEECCEEEECECCCCHHHCCHHHCEEECCCCCECCCCCECCCHHHHHHHHHHCCCCCEEEEECCHHHHHHHHHCCCECCCCHHHHHHCCCCECEECCCCCCCHHHHHHHHHHHCCCCEEEECCCEEEEEECCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHC
CHHHHHHHHHHHHHHHHHHHCCCCCCCCCEEEECCCCEEEECCCCCCCCCCCHHEEEECCCCCEECCCCCCCCCHHHHHHHHHCCCCEEEEEECCHHHHHHHHHCCCCCCCHHHHHHHCCCCCEEECCCCCCCHHHHHHHHHHHCCCCEEEECCCCEEEECCCHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCHHHHHHHHHCC
> 1AORB_1-211
MYGNWGRFIRVNLSTGDIKVEEYDEELAKKWLGSRGLAIYLLLKEMDPTVDPLSPENKLIIAAGPLTGTSAPTGGRYNVVTKSPLTGFITMANSGGYFGAELKFAGYDAIVVEGKAEKPVYIYIKDEHIEIRDASHIWGKKVSETEATIRKEVGSEKVKIASIGPAGENLVKFAAIMNDGHRAAGRGGVGAVMGSKNLKAIAVEGSKTVPI
CCCCCCEEEEEECCCCEEEEEECCHHHHHHHCCHHHHHHHHHHHHCCCCCCCCCCCCCEEEEECCCCCCCCCCCCCEEEEEECCCCCCEEEEEECCCHHHHHHHHCCCEEEEECCCCCCEEEEEECCEEEEEECCCCCCCCHHHHHHHHHHHCCCCCCEEEECCHHHHCCCCCECEEECCCEEECCCCHHHHHHHCCEEEEEEECCCCCCC
CCCCCCCEEEEECCCCCEEEECCCHHHHHHHCCCHHHHHHHHHHHCCCCCCCCCCCCCEEEEECCCCCCCCCCCCCEEEEEECCCCCCCCCCCCCCCHHHHHHHCCCCEEEEECCCCCCEEEEECCCCEEEEECHHHCCCCCHHHHHHHHHHCCCCCCEEEEECCCCCCCCEEEEECCCCCCECCCCCCHHHHCHCCCEEEEECCCCCCCC
> 3ECAB_1-212
LPNITILATGGTIAGGGDSATKSNYTVGKVGVENLVNAVPQLKDIANVKGEQVVNIGSQDMNDNVWLTLAKKINTDCDKTDGFVITHGTDTMEETAYFLDLTVKCDKPVVMVGAMRPSTSMSADGPFNLYNAVVTAADKASANRGVLVVMNDTVLDGRDVTKTNTTDVATFKSVNYGPLGYIHNGKIDYQRTPARKHTSDTPFDVSKLNELP
CCEEEEEEEECHHHCEECCCCCCCEECCCCCHHHHHHHCHHHHHCCEEEEEEEEEECHHHCCHHHHHHHHHHHHHCHHHCCCEEEECCCCCHHHHHHHHHHHCCCCCCEEEECCCCCCCCCCCCHHHHHHHHHHHHCCHHHCCCCCEEEECCEEEECCCEEECECCCCCCEEECCCCCCEEEECCEEEECCCCCCCCHHHCCCCCCCCCCCC
CCCEEEEECCCEEECCCCCCCCCCCCCCCCCHHHHHHHHHHHCCCCCEEHHHEHCCCCCCCCHHHHHHHHHHHHHHHCCCCEEEEEECCCCHHHHHHHHHHHHCCCCCEEEEECCCCCCCCCCCCHHHHHHHHHHHCCCCCCCCCEEEEECCCCECCCEEEEECCCCCCHHCCCCCCCCEEEECCCEEEECCCCCCCCCCCCEEECCCCCCC
> 1SFTB_15-244
DAIYDNVENLRRLLPDDTHIMAVVKANAYGHGDVQVARTALEAGASRLAVAFLDEALALREKGIEAPILVLGASRPADAALAAQQRIALTVFRSDWLEEASALYSGPFPIHFHLKMDTGMGRLGVKDEEETKRIVALIERHPHFVLEGLYTHFATADEVNTDYFSYQYTRFLHMLEWLPSRPPLVHCANSAASLRFPDRTFNMVRFGIAMYGLAPSPGIKPLLPYPLKEA
HHHHHHHHHHHHHCCCCCEEEEECHHHHHHCCHHHHHHHHHHHCCCEEEECCHHHHHHHHHCCCCCCEEECCCCCHHHHHHHHHCCEEEEECCHHHHHHHHHHCCCCCCEEEEEEECCCCCCCCECCHHHHHHHHHHHHCCCCEEEEEEECCCCCCCCCCCHHHHHHHHHHHHHHHHCCCCCCEEECCCHHHHHHCCCCCCCEEEECHHHHCCCCCCCCHHHCCCCCCCC
HHHHHHHHHHHHHCCCCCEEEEEEHHCHCCCCHHHHHHHHHHCCCCEEEHHHHHHHHHHHHHCCCCCEEEECCCCCCHHHHHHHCCCEEEECCHHHHHHHHHHHCCCCCCEEEEEECCCCCCCCCCCHHHHHHHHHHHHHCCCCCEEEEEEECCCCCCCCCHHHHHHHHHHHHHHHHCCCCCCEEEHHCCHHHHCCCCHCCCCECCEEEEECCCCCCCCCCCCCCCCCCC
> 1ZNBB_18-249
AQKSVKISDDISITQLSDKVYTYVSLAEIEGMVPSNGMIVINNHQAALLDTPINDAQTETLVNWVADSLHAKVTTFIPNHWHGDCIGGLGYLQKKGVQSYANQMTIDLAKEKGLPVPEHGFTDSLTVSLDGMPLQCYYLGGGHATDNIVVWLPTENILFGGCMLKDNQATSIGNISDADVTAWPKTLDKVKAKFPSARYVVPGHGDYGGTELIEHTKQIVNQYIESTSKP
CCCCEEEECCEEEEECCCCEEEEEEEEECCCEEEEEEEEEEECCEEEEECCCCCHHHHHHHHHHHHHCCCCEEEEEECCCCCHHHHCCHHHHHHCCCEEEEEHHHHHHHHHHCCCCCCEEECCEEEEEECCEEEEEECCCCCCCCCCCEEEECCCCEEEEEEEEECCCCCCCCCCCCCCCCCHHHHHHHHHHHCCCCCEEEEEECCCECCHHHHHHHHHHHHHHHHCCCC
CCCHHHHCCCEEEEEECCCEEEEEECCCCCCCCCCCEEEEECCCCEEEEECCCCHHHHHHHHHHHHHHCCCCEEEEEEECCCCCHCCCHHHHHHCCCEEECCCCHHHHHHHCCCCCCCCEECCCCEEEECCCEEEEEECCCCCCCCCEEEEECCCCEEEEEEEEECCCCCCCCCCCCCCHHHHHHHHHHHHHCCCCCEEEECCCCCCCCHHHHHHHHHHHHHHHHHHCCC
> 1FUQB_140-393
SNDVFPTAMHVAALLALRKQLIPQLKTLTQTLNEKSRAFADIVKIGRTHLQDATPLTLGQEISGWVAMLEHNLKHIEYSLPHVAELALGGTAVGTGLNTHPEYARRVADELAVITCAPFVTAPNKFEALATCDALVQAHGALKGLAASLMKIANDVRWLASGPRCGIGEISIPENEPMPGKVNPTQCEALTMLCCQVMGNDVAINMGGASGNFELNVFRPMVIHNFLQSVRLLADGMESFNKHCAVGIEP
HHHHHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHCCCCEEEEEECCEEEEEEEHHHHHHHHHHHHHHHHHHHHHHHHHHCEECCCCCCCCCCCCCCCCHHHHHHHHHHHHHCCCCEECCCHHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCEECCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHCCCECCECCCHHHHHHHHHHHHHHHHHHHHHHHHHCHHHCEE
CCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCEECCCEECCCCCEEEHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCHHHHHHHHHHHHHCCCCCCCCCCHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCEECCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHCCCCHHCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCC
> 1QRDB_1-273
AVRRALIVLAHAERTSFNYAMKEAAVEALKKKGWEVVESDLYAMNFNPLISRNDITGEPKDSENFQYPVESSLAYKEGRLSPDIVAEQKKLEAADLVIFQFPLYWFGVPAILKGWFERVLVAGFAYTYATMYDKGPFQNKKTLLSITTGGSGSMYSLQGVHGDMNVILWPIQSGILRFCGFQVLEPQLVYSIGHTPPDARVQVLEGWKKRLETVWEESPLYFAPSSLFDLNFQAGFLLKKEVQEEQKKNKFGLSVGHHLGKSIPADNQIKARK
CCCEEEEEECCCCCCCHHHHHHHHHHHHHHHCCCEEEEEEHHHHCCCCCCCHHHECCCCCCHHHCCHHHHHHHHHHHCCECHHHHHHHHHHHHCCEEEEEEECECCECCHHHHHHHHHHCCECCCECCCCCHHHCCCCCCEEEEEEECCCCHHHHCCCCCCCCHHHHHHHHHHHHHHCCCCEECCCEEECCHHHCCHHHHHHHHHHHHHHHCCHHHCCCCCCCCHHHEECCCCCCCEECHHHHHHHCCCCECCCCCCCCCCECCCCCCCCCCC
CCCEEEEEECCCCCCCCHHHHHHHHHHHHHHCCCEEEEEEHHHCCCCCCCCHHHHHCCCCCCCCCCCCHHHHHHCHCCCCCHHHHHHHHHHHHCCEEEEEECCHCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCEEEEEEECCCCCCCCCHHCCCCCHHHHHHHHHHHHHCECCCCCCCEEEEECCCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCHHCHHHCCHHHHCCCCCEEEEHHCCCCCCCCCHHHCCC
> 1YPTB_189-468
YGPEARAELSSRLTTLRNTLAPATNDPRYLQACGGEKLNRFRDIQCRRQTAVRADLNANYIQVGNTRTIACQYPLQSQLESHFRMLAENRTPVLAVLASSSEIANQRFGMPDYFRQSGTYGSITVESKMTQQVGLGDGIMADMYTLTIREAGQKTISVPVVHVGNWPDQTAVSSEVTKALASLVDQTAETKRNMYESKGSSAVADDSKLRPVIHCRAGVGRTAQLIGAMCMNDSRNSQLSVEDMVSQMRVQRNGIMVQKDEQLDVLIKLAEGQGRPLLNS
CCHHHHHHHHHHHHHHHHHCCCCCCCCCECCCCCCCCCCCCCCCCCECCCECCCCCCEEEEEECCEEEEEECCCCHHHHHHHHHHHHHCCCCCEEECCCHHHHHCHHHCCCCCCCCCEEECCEEEEEEEEEEEEEECCEEEEEEEEEEEECCCCCEEEEEEEECCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHCCCHHHHCCCCCCCEEECCCCCCHHHHHHHHHHHCCHHHCCCCHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHCCCCCCC
CCCCCCCHHHHHHHHHHCCCCCCCCCCHHCCCCCCCCCCCCCCCCCCCCCCEEHHECHCHCCCCCCEEEEECCCCCCCHHHHHHEEHCCCCEEEEEEEHHHHCCCCEECCCCCCCCCCCCCCEEEEEEEEEEECCCCEEEEEEEEEEECCCCCCCEEEEEEEECCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCEEEECCCCCCCCEEEEEHHHHHHHCCCCEEEHHHHHHHHHHCCCCCEECHHHHHHHHHHHHHHHHHHHCC
> 1NAL4_4-294
NLRGVMAALLTPFDQQQALDKASLRRLVQFNIQQGIDGLYVGGSTGEAFVQSLSEREQVLEIVAEEGKGKIKLIAHVGCVTTAESQQLAASAKRYGFDAVSAVTPFYYPFSFEEHCDHYRAIIDSADGLPMVVYNIPALSGVKLTLDQINTLVTLPGVGALKQTSGDLYQMEQIRREHPDLVLYNGYDEIFASGLLAGADGGIGSTYNIMGWRYQGIVKALKEGDIQTAQKLQTECNKVIDLLIKTGVFRGLKTVLHYMDVVSVPLCRKPFGPVDEKYQPELKALAQQLMQ
CCCCEEEECCCCECCCCCECHHHHHHHHHHHHHCCCCEEEECCCCCCHHHCCHHHHHHHHHHHHHHHCCCCEEEEECCCCCHHHHHHHHHHHHHHCCCEEEEECCCCCCCCHHHHHHHHHHHHHHCCCCCEEEEECHHHHCCCCCHHHHHHHHCCCCEEEEEECCCCHHHHHHHHHHCCCCEEEECCHHHHHHHHHHCCCEEEECCHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHCHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHCC
CCCCEEEEECCCCCCCCCCCHHHHHHHHHHHHHCCCCEEEECCCCCCCCCCCHHHHHHHHHHHHHHCCCCCEEEEECCCCCHHHHHHHHHHHHHCCCCEEEECCCCCCCCCHHHHHHHHHHHHHHCCCCCEEEECCCCCCCCCCCHHHHHHHHCCCCEEEEECCCCCHHHHHHHHHHCCCEEEECCCCHHHHHHHHHCCCCEEEEHHHHCHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHC
> 1GHSB_1-306
IGVCYGVIGNNLPSRSDVVQLYRSKGINGMRIYFADGQALSALRNSGIGLILDIGNDQLANIAASTSNAASWVQNNVRPYYPAVNIKYIAAGNEVQGGATQSILPAMRNLNAALSAAGLGAIKVSTSIRFDEVANSFPPSAGVFKNAYMTDVARLLASTGAPLLANVYPYFAYRDNPGSISLNYATFQPGTTVRDQNNGLTYTSLFDAMVDAVYAALEKAGAPAVKVVVSESGWPSAGGFAASAGNARTYNQGLINHVGGGTPKKREALETYIFAMFNENQKTGDATERSFGLFNPDKSPAYNIQF
CEEECCCCCCCCCCHHHHHHHHHHHCCCEEEECCCCHHHHHHCCCCCCEEEEECCHHHHHHHHHCHHHHHHHHHHHCCCCCCCCEEEEEEEEECCCHHHHHHHHHHHHHHHHHHHHHCCCCCEEEEEEEHHHEECCCCHHHCEECCCHHHHHHHHHHHHCCCEEEECCHHHHHHHCCCCCCHHHHCCCCCCCEECCCCCCEECCHHHHHHHHHHHHHHHCCCCCCCEEEEECCCCCCCCCCCCHHHHHHHHHHHHHHHHHCECCECCCCCEEEECCECCCCCCCCHHHHCCCCECCCCCECCCCCC
CCEEECCCCCCCCCHHHHHHHHHHCCCCEEEEECCCCHHHHHCCCCCCEEEEECCCCCHHHHCCCHHHHHHHHHHHCCCCCCCCEEEEEEECCCCCCCCCCCHHHHHHHHHHHHHHHCCCCEEEEEEHHHHHHCCCCCCCCCCCCCHHHHHHHHHHCCCCCCEEEECCCHHHHCCCCCCCCCCEECCCCCCCEECCCCCCEEEHHHHHHHHHHHHHHHHHCCCCEEEEEEECCCCCCCCCCCCHHHHHHHHHHHHHHHCCCCCCCCCCCHEEEHHHHCCCCCCCCCHHHEECEECCCCCCEECCCC
> 1SESA_105-421
PWPGAPVGGEEANREIKRVGGPPEFSFPPLDHVALMEKNGWWEPRISQVSGSRSYALKGDLALYELALLRFAMDFMARRGFLPMTLPSYAREKAFLGTGHFPAYRDQVWAIAETDLYLTGTAEVVLNALHSGEILPYEALPLRYAGYAPAFRSEAGSFGKDVRGLMRVHQFHKVEQYVLTEASLEASDRAFQELLENAEEILRLLELPYRLVEVATGDMGPGKWRQVDIEVYLPSEGRYRETHSCSALLDWQARRANLRYRDPEGRVRYAYTLNNTALATPRILAMLLENHQLQDGRVRVPQALIPYMGKEVLEPCG
CCCCCCCCCHHHCEEEEEECCCCCCCCCCCCHHHHHHHHCCECCCHHHHHCCCCCCEECHHHHHHHHHHHHHHHHHHHCCCEEEECCCEEEHHHHHHHCCCCCCHHHCCEECCCCEEECCCCHHHHHHCCCCCEEEHHHCCEEEEEEEEEECCCCCCCCCCCCCCCCCCEEEEEEEEEEECCCHHHHHHHHHHHHHHHHHHHHHCCCCEEEEECCHHHHCCCCCEEEEEEEEECCCCEEEEEEEEEEEECHHHHHHCCEEECCCCCEEECEEEEEEEEEECHHHHHHHHHHECCCCCEECCHHHHHHHCCCEECCCC
CCCCCCCCCCCCCEEEEECCCCCCCCCCCCCHHHHHHHHCCCHHHHHHCCCCCEEECCCCHHHHHHHHHHHHHHHHHHHCHHHCCCCHHCCHHHHHHCCCCCCCHHHEEEECCCCEECCCCCCCCHHHHHHHHHCCHHHCCHHHHCCCCCEHHHHCCCCCCCCCEEEEHHHHHHHEEEECCCCHHHHHHHHHHHHHHHHHHHHHCCCCEEEEECCCCCCCCCCCCECEHEEECCCCCHHHHHCCCCCCCCCCCHHHCEEEECCCCCCCEEEEECCCCCCCHHHHHEEEHCCCCCCCEEECCHHHCCCCCCCEEECCC
> 1MRRB_1-340
AYTTFSATKNDQLKEPMFFGQPVQVARYDQQKYDIFEKLIEKQLSFFWRPEEVDVSRDRIDYQALPEHEKHIFISNLKYQTLLDSIQGRSPNVALLPLISIPELETWVETWAFSETIHSRSYTHIIRNIVNDPSVVFDDIVTNEQIQKRAEGISSYYDELIEMTSYWHLLGEGTHTVNGKTVTVSLRELKKKLYLCLMSVNALEAIRFYVSFACSFAFAERELMEGNAKIIRLIARDEALHLTGTQHMLNLLRSGADDPEMAEIAEECKQECYDLFVQAAQQEKDWADYLFRDGSMIGLNKDILCQYVEYITNIRMQAVGLDLPFNTRSNPIPWINTWLV
CCCCCCCCCCCCCCCCCECCCCCCCCCCCCCCCHHHHHHHHHHHHCCCCHHHCCCCCHHHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHECCHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCEEEEECCEEEEECHHHHHHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCECCECHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCHHHHHCCC
CCEEECCCCCCHHHHHHCCCCCCCEEECCCCCCHHHHHHHHHHHHCCCCHCECCCHCCHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCHHHHHEC
> 3EOJA_8-366
TTTAHSDYEIILEGGSSSWGQVKGRAKVNVPAAIPLLPTDCNIRIDAKPLDAQKGVVRFTTKIESVVDSVKNTLNVEVDIANETKDRRIAVGEGSLSVGDFSHSFSFEGSVVNMYYYRSDAVRRNIPNPIYMQGRQFHDILMKVPLDNNDLVDTWEGFQQSISGGGANFGDWIREFWFIGPAFAAINEGGQRISPIVVNSSNVEGKGPVGVTRWKFSHAGSGVVDSISRWTELFPVEQLNKPASIEGGFRSDSQGIEVKVDGNLPGVSRDAGGGLRRILNHPLIPLVHHGMVGKFNDFTVDTQLKIVLPKGYKIRYAAPQFRSQNLEEYRWSGGAYARWVEHVCKGGTGQFEVLYAQ
CCEEEEEEEEEECCCCCCCEEEEEEEEECCCCCCCCCCEEEEEEEEEEECCCCCCCEEEEEEEEEEECCEEEEEEEEEEEEECCCCEEEEEEEEEEEECCEEEEEEEEEEEEEEECCCCHHHHCCCCCCCCCCEEEEEEEEEEEECCCHHHHHHHHHHHHHHHHCHHHHHHHHHHHHCCCCHHHHHHHCCEEECCCEEEEEEEECCCCEEEEEEEEEEEECCCCHHHHCCCCCCCHHHCCCCEEEEEEEEECCCCEEEEEEEEECCCEEEEECCEEEECCCCHHHHHHHHCCCCCCCCEEEEEEEEECCCCCEEEEEECCCCEEECCEEEEECHHHHHHHHHHHCCCCCCCEEEEEC
CCCCCCCHEEEECCCCCCCEEEEEEEEECCCCCCCCCCCCCCEEEECCCCCCCCCCEEEEEEEHEEEHCHCCCEEEEEEECCCCCCCEEEECCCEEEECCCCEEEEECCCEEEEEEEEHHHCHCCCCCCCHECCCCHHEEEEECCCCCCCHHHHHHHHHHHCCCCCCCCCCHHHHHEEECHHHCCHHHCCHHCCCEEEECCCCCCCCCEEEEEEEECCCCCCEEEHHHHHHHHCHHHCCCCCCHHECCCCCCCCCEEEEECCCCCCCEECCCCCHHHHHCCCCCCEEECCCCCCCCCCCECEEEEEEECCCCEEEECCCCCHHHHHHHHEECCHHHHHHHHEECCCCCCCEEEEEEC
> 2AATA_13-408
MFENITAAPADPILGLADLFRADERPGKINLGIGVYKDETGKTPVLTSVKKAEQYLLENETTKNYLGIDGIPEFGRCTQELLFGKGSALINDKRARTAQTPGGTGALRVAADFLAKNTSVKRVWVSNPSWPNHKSVFNSAGLEVREYAYYDAENHTLDFDALINSLNEAQAGDVVLFHGCCHNPTGIDPTLEQWQTLAQLSVEKGWLPLFDFAYQGFARGLEEDAEGLRAFAAMHKELIVASSYSANFGLYNERVGACTLVAADSETVDRAFSQMKAAIRANYSNPPAHGASVVATILSNDALRAIWEQELTDMRQRIQRMRQLFVNTLQEKGANRDFSFIIKQNGMFSFSGLTKEQVLRLREEFGVYAVASGRVNVAGMTPDNMAPLCEAIVAVL
CCCCCCCCCCCCCCCCCCCCCCCCCCCCECCCCCCCCCCCCCCCCCCCCHHHHCHHHHCCCCCCCCECCECHHHHHHHHHHHHCCCCCCCCCCCEEEEEECCCHHHHHHHHHHHHCCCCCCEEECCCCCCCCCCCCCCCCCCEEECCCCCCCCCCCCCHHHHHHHHHCCCCCCEECCCCCCCCCCCCCCCHHHHHHHHHHHHHHCCCCCCEEECCCCCCCCCCHHHHHHCCCCCCCCEEEEEECCCCCCCCCCCCEEEEEECCCHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCHHHHCCCCCEEECCCCCHHHHHHHCCCCECCCCCCEEEHHHCCHHHHHHHHHHCHHHC
CHHCCCCCCCCCHHHHHHHHHHCCCCCCEEEEECCCCCCCCCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCHHHHHHHHHHHCCCCCCCCCCCEEEEEEECCCCHHHHHHHHHHHHHCCCCEEEECCCCCCCCHHHHHHCCCCEEEEECCCCCCCCCCHHHHHHHHHHCCCCEEEEEECCCCCCCCCCCCHHHHHHHHHHHHHCCCEEEEEHHHHHHCCCCCCCCHHHHHHHCCCCCEEEEECCCHCCCCCCCCEEEEEEECCCHHHHHHHHHHHHHEEECCCCCCCHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCEEECCCCCCEEECCCCHHHHHHHHHHCEEEEECCCCEEEECCCCCCHHHHHHHHHHHC
> 1REQC_55-560
YKDMDWLDTYAGIPPFVHGPYATMYAFRPWTIRQYAGFSTAKESNAFYRRNLAAGQKGLSVAFDLPTHRGYDSDNPRVAGDVGMAGVAIDSIYDMRELFAGIPLDQMSVSMTMNGAVLPILALYVVTAEEQGVKPEQLAGTIQNDILKEFMVRNTYIYPPQPSMRIISEIFAYTSANMPKWNSISISGYHMQEAGATADIEMAYTLADGVDYIRAGESVGLNVDQFAPRLSFFWGIGMNFFMEVAKLRAARMLWAKLVHQFGPKNPKSMSLRTHSQTSGWSLTAQDVYNNVVRTCIEAMAATQGHTQSLHTNSLDEAIALPTDFSARIARNTQLFLQQESGTTRVIDPWSGSAYVEELTWDLARKAWGHIQEVEKVGGMAKAIEKGIPKMRIEEAAARTQARIDSGRQPLIGVNKYRLEHEPPLDVLKVDNSTVLAEQKAKLVKLRAERDPEKVKAALDKITWAAGNPDDKDPDRNLLKLCIDAGRAMATVGEMSDALEKVFGRYT
HCCCCCCCCCCCCCCCCCCCCCCHHHCCCCEECCEECCCCHHHHHHHHHHHHHCCCCCEEEEECHHHHCCCCCCCHHHHHHCCCCCECCCCHHHHHHHCCCCCCCCCCEEEECCCCHHHHHHHHHHHHHHCCCCHHHCCEEECCCHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHCCCEECEEEECHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHCCCCHHHCHHHEEEEEEECCCHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHCCCEEEEECCCCCCCCCCCHHHHHHHHHHHHHHHCCCCEEECCCCCCCCCCCCHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHCHHHHHHHHHHHHHHHHHHHCCCCCECCCECCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHHHCCCCCCHHHCHHHHHHHHHHCCCCHHHHHHHHHHHHCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEEEECCCCCCHHHHHHHHHHHHCCCCCEEEEEHCCCCCCCCCCCCCCCCCCCCCEEEECHHHHHHHHHHCCCCCCCEEEEECCCCHHHHHHHHHHHHHHCCCCCCHCCCCCCCCHHHHHHHCCCCCCCCCHHHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHCCCCHHHHCCCHEEEEECCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHEEEEECCCCCCCCCCCHHEEHHHHHHHHHHHHCCCCECECCCHHHCCCCCCHHHHHHHHHHHHHHHHHCCCHHCCCCCCCCEEHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHCCCCEEEEEEECCCCCCCCCCCEEECCCCCHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHCCCCHHHHHHHHHHHCCCCC
