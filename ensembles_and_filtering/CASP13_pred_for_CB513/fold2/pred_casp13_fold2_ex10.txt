> 6gnx_2
GYISIDAXKKFLGELHDFIPGTSGYLAYHVQN
CCEEHHHCCCCCCCEEECCCCCCCEEEEECCC
CCECHHHHHHHHCCHCCCCCCCCCCHEEEECC
> 6fxa_1
ENIEETITVMKKLEEPRQKVVLDTAKIQLKEQDEQ
CHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHCC
CCHHHHHHHHHHHHHHHHHHEHHHCCCCHHHHHCC
> 5w9f_1
SQETRKKATEMKKKFKNBEVRADESNHCVEVRBSDTKYTLC
CCHHHHHHHHHHHHCCCCEEECCCCCCCEEEECCCCEEEEC
CCEEECCCCEEECCCEEEHHHHHHHHHHHCCCHCCCCCCCC
> 6f45_2
SASIAIGDNDTGLRWGGDGIVQIVANNAIVGGWNSTDIFTEAGKHITSNGNLNQWGGGAIYCRDLNVS
CCEEECCCCCCEEEECCCCCEEEEECCEEEEEECCCEEEECCCCEEEECCCEEECCCCCEEECCEEEC
CCEEEECCCCCCEEECCCCEEEEEHCCHEEEEECCCCEEEEECCEEECCCCEECCCCCCEEEEEECCC
> 6qek_1
DIYGDEITAVVSKIENVKGISQLKTRHIGQKIWAELNILVDPDSTIVQGETIASRVKKALTEQIRDIERVVVHFEPAR
CHHHHHHHHHHCCCCCCCEEEEEEEEEECCEEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCCCEEEEEEEEEECC
CCCHHHHHHHHHCCCCCECCHHEEEHHCCCEEEEEEEEECCCCCEEHHHHHHHHHHHHHHHHHHCCHCEEEEEECCCC
> 6qfj_1
SHMEDYIEAIANVLEKTPSISDVKDIIARELGQVLEFEIDLYVPPDITVTTGERIKKEVNQIIKEIVDRKSTVKVRLFAAQEEL
CHHHHHHHHHHHHHHCCCCCCEEEEEEEEEECCEEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEEEEECCCEC
CCHHHHHHHHHHHHHCCCCHEECHHEEHHHCCCEEEEEEEEECCCCCEHHHHHHHHHHHHHHHHHHCCCHCEEEEEECCCHHCC
> 6btc_1
XNKKSKQQEKLYNFIIAKSFQQPVGSTFTYGELRKKYNVVCSTNDQREVGRRFAYWIKYTPGLPFKIVGTKNGSLLYQKIGINPC
CCCCCHHHHHHHHHHHHHHHHCCCCCEECHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHCCCCCEEEEEEECCEEEEEECCCCCC
CCCCHHHHHHHHHHHHHHEECCCCCCEEEEEEHHHHCCCCCCCHHHHHHHHHHHHHHHCCCCCCEEEECCCCCCEEEEECCCCCC
> 6d7y_1
LDTAQAPYKGSTVIGHALSKHAGRHPEIWGKVKGSXSGWNEQAXKHFKEIVRAPGEFRPTXNEKGITFLEKRLIDGRGVRLNLDGTFKGFID
CCCCCCEECCEEHHHHHHHHHHHHCHHHHCCCCCCHHHHHHHHHHHHHHHHHCCCCCEEEECCCCCEEEEEECCCCCEEEEECCCCEEEEEC
CCHCCCCCCCCCHHHHHHHHHCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCEEEEEECCCCCEEEECCCCCEEEEEC
> 6g57_1
KRSGFLTLGYRGSYVARIMVCGRIALAKEVFGDTLNESRDEKYTSRFYLKFTYLEQAFDRLSEAGFHMVACNSSGTAAFYRDDKIWSSYTEYIFFRP
CCCEEEEEEEEEECCCEEEEEEEHHHHHHHHHHHCCECCCCCCEEEEECCCCCHHHHHHHHHHCCCEEEEEEEEEEEECCCCCCEEEEEEEEEEEEC
CCCCEEEEEECCCEEEEEEECCCHEEHHHHCCCHHCCCCCCCCCCEEEHCCCHHHHHHHHHHHHCCEEEEECCCCCCCCCCHHCHCCCEEEEEEECC
> 6msp_1
MGHHHHHHENLYFQSHMTDELLERLRQLFEELHERGTEIVVEVHINGERDEIRVRNISKEELKKLLERIREKIEREGSSEVEVNVHSGGQTWTFNEK
CCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHCCCCCEEEEEEEECCEEEEEEEECCCHHHHHHHHHHHHHHHHHCCCCCEEEEEEECCEEEEEECC
CCHHHHHHHHHHHCHHHHHHHHHEHHCHHHHHHHCCCCCCCCCCCCCHHHCCCCCCCCCEECCCCCCCCCCCCHHCCCCCCCCHCCCCCCHHHEECC
> 6gnx_1
SLKPFTYPFPETRFLHAGPNVYKFKIRYGNKEVITQELEDSVRVVLGNLDNLQPFATEHFIVFPYKSKWERVSHLKFKHGEIILIPYPFVFTLYVEXKW
CCCCCCCCCCEEEEEEECCEEEEEEEEECCHHHHHHHHHHHHHHHHHCCCCCCCEECCCEEEEEEEEECCCCCCEEEEECCEEEEEEEEEEEEEEEECC
CCCCCCCCCCCCEEEECCCEEEEEEEECCCCCEEHHHHHHHHEEEECCCCCCCCCCCCCEEECCCHCHHHHCCCCEEECCCCECCCCCEEEEEEEECCC
> 6cp9_2
XFIENKPGEIELLSFFESEPVSFERDNISFLYTAKNKAGLSVDFSFSVVEGWIQYTVRLHENEILHNSIDGVSSFSIRNDNLGDYIYAEIITKELINKIEIRIRPDIKIKSSSVI
CCCCECCCHHHHHHHHCCCCCEEECCCCEEEEEEECCCCEEEEEEEECCCCEEEEEEEECCEEEEEEEEECCCECEEEEECCEEEEEEEEECCCCEEEEEEECCCCCEEEEEEEC
CCEECCCCHHHHHHHCCCCCEEECCCCCEEEEEECCCCCEEEEEEEECHCCCEEEEEEECCCEEEEEEHCCCCEEEEECCCCCCEEEEEEECCCCEEEEEEEECCEEEEEEEEEC
> 6cp9_1
TAQTIANSVVDAKKFDYLFGKATGNSHTLDRTNQLALEXKRLGVADDINGHAVLAEHFTQATKDSNNIVKKYTDQYGSFEIRESFFIGPSGKATVFESTFEVXKDGSHRFITTIPKNG
CHHHHHCCCCCHHHHHHHCCCCCCECCECCCHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHCCCHHHEEEEEECCCCEEEEEEEEEECCCCCEEEEEEEEEECCCCCEEEEEEEEECC
CHHHCCEEEECCCCCEHHECCCCCCCCCHHHHHHHHHHHHEECCECCHCCHHHHHHHHHHHHCCCCCEEEEECCCCCCEEEEEEEECCCCCCEEEEEEEEECCCCCCCCEEEEECCCC
> 6d34_1
PASPNIEAILASYAGFRDRDIEGILSGMHPDVEWVHPEGMGKYGLGGTKLGHAGIKEFLAHVPTVLGGMRLAPREFIEQGDRVVVFGTREVTSLRGTTATLDFVHSWTMRDGKATRMEDIFDTVAFHELIES
CCCHHHHHHHHHHHHHHCCCHHHHHCCEEEEEEEEECHHHHHHCCCEEEEHHHHHHHHHHHHHHCEEEEEEEEEEEEEECCEEEEEEEEEEEECCCCEEEEEEEEEEEEECCEEEEEEEECCHHHHHHHHCC
CCCCCHHHHHHHHHHHHCCCHHHHHHHHCCCHEEEECCCCCCCCCCCEEECHHHHHHHHHHHHHHCCCEEECEEEEECCCCEEEEEEEEEEEECCCCEEHHHEEEEEEECCCCEEEEEHHHHHHHHHHHHCC
> 6rap_1
AITADDIAVQYPIPTYRFIVTLGDEQVPFTSASGLDINFDTIEYRDGTGNWFKMPGQRQAPNITLSKGVFPGKNAMYEWINAIQLNQVEKKDIMISLTNEAGTEVLVSWNVSNAFPTSLTSPSFDATSNEIAVQQITLMADRVTIQTA
CCCCCCHHHHCCCCCCCEEEEECCEECCCCEEECCCCCCCEEEEECCCCCEEEEECCCCCCEEEEEEECCCCCCCCHHHHCCCECCECCCEEEEEEEECCCCCCEEEEEEEEEEEEEEEECCCCCCCCCCCCEEEEEEECCCEEEEEC
CCCHHHHCHCCCCCCCEEEEEECCCEEEEEEECCCCEEEEEEEEECCCCCCEECCCCCCCCCEEEEEEEECCCHHHHHHHHHHHCCCECCCCEEEEEECHCCCCHEEEEEEECCCEEEECCCCCCCCCCHEEEHHHHHHHCCEEEECC
> 6d7y_2
XKELFEVIFEGVNTSRLFFLLKEIESKSDRIFDFNFSEDFFSSNVNVFSELLIDSFLGFNGDLYFGVSXEGFSVKDGLKLPVVLLRVLKYEGGVDVGLCFYXNDFNSAGKVXLEFQKYXNGISADFGFENFYGGLEPASDQETRFFTNNRLGPLL
CCEEEEEEEECCCCCCHHHHHHHHHCCCCEEEEEEECCCCCCCCCCCCCHHHHHHHHHCCCCEEEEEEEEEEEEECCEEEEEEEEEEEEECCEEEEEEEEEECCHHHHHHHHHHHHHHHHHHHHHHCCCEEEEEECCCCCHHHEEEECCEECCCC
CHHHHHEEHHCCCHHCHHHHHHHHCCCCCCCCCCCCCCCHHCCCCHHHHHHHHHHHHCCCCCEEEEEEECCCCECEEEECCEEEEEEEECCCCEEEEEECCCCCCCHHHHHHHHHHHHHHHHHHHCCCCEEECCCCCCCCCCHEEECCCCCCCCC
> 6cp8_3
VNSTAKDIEGLESYLANGYVEANSFNDPEDDALECLSNLLVKDSRGGLSFCKKILNSNNIDGVFIKGSALNFLLLSEQWSYAFEYLTSNADNITLAELEKALFYFYCAKNETDPYPVPEGLFKKLXKRYEELKNDPDAKFYHLHETYDDFSKAYPLNN
CCCHHHHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHCHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHCCCCCHHHHCHHHHHHHHHHHCCCCC
CCCHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHHCHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHCCCCCHHHHHHHHHHHHCCCHHHHHHHHHHHEECCCCCCCCCCCHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHCCCCC
> 6cp8_1
NSFEVSSLPDANGKNHITAVKGDAKIPVDKIELYXRGKASGDLDSLQAEYNSLKDARISSQKEFAKDPNNAKRXEVLEKQIHNIERSQDXARVLEQAGIVNTASNNSXIXDKLLDSAQGATSANRKTSVVVSGPNGNVRIYATWTILPDGTKRLSTVTGTFK
CCCEEEEEECCCCCEEEEEEECCEEEECHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHCCCCECCECEEEEEEEECCEEEEEEEEEEECCCCCEEEEEEECCCC
CCCCEEECCCCCCCCEEEEECCCCEECECEEEEEECCCCCCCHHHHHHHHHHHHHHHHHCHHHHHHCCCCHHHHHHHHCCCCCHHHHHHHHHHHHECCCCCCCCCHHHHHHHHHHHHCCCCCCCCEEEEEEECCCCCEEEEEEEEECCCCCCEEEEEECCCC
> 6e4b_1
AMRLWLIRHGETQANIDGLYSGHAPTPLTARGIEQAQNLHTLLHGVSFDLVLCSELERAQHTARLVLSDRQLPVQIIPELNEMFFGDWEMRHHRDLMQEDAENYSAWCNDWQHAIPTNGEGFQAFSQRVERFIARLSEFQHYQNILVVSHQGVLSLLIARLIGMPAEAMWHFRVDQGCWSAIDINQKFATLRVLNSRAIGVEN
CEEEEEEECCCEHHHHHCECCCCCCCCECHHHHHHHHHHHHHCCCCCCCEEEECCCHHHHHHHHHHCCCCCCCEEECHHHCCCCCHHHCCCEHHHHHHHCHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECHHHHHHHHHHHCCCCHHHHHHECCCCCCEEEEEEECCEEEEEEEEECCCCCCC
CCEEEEEECCCEHHHHCCCECCCCCCCCCHHHHHHHHHHHHHHHCCCCEEEECCCCHHHHHHHHHHHCCCCCCEEECHCHHHCCCCCECCCCHHHHHHHCHHHHHHHHCCCCCECCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCHHHHHHHHHHCCCHHHHHECCCCCCEEEEEEECCCCCEEEEECCCCEEECC
> 6rbk_1
SLLERGLSKLTLNAWKDREGKIPAGSMSAMYNPETIQLDYQTRFDTEDTINTASQSNRYVISEPVGLNLTLLFDSQMPGNTTPIETQLAMLKSLCAVDAATGSPYFLRITWGKMRWENKGWFAGRARDLSVTYTLFDRDATPLRATVQLSLVADESFVIQQSLKTQSAPDRALVSVPDLASLPLLALSAGGVLASSVDYLSLAWDNDLDNLDDFQTGDFLRAT
CCCCCCCCCCEEEEECCCCCCCEEEEEECCCCCCCEEEEEEEEEEECCCCCCCCCCEEEEEEEEEEEEEEECCECCCCCCCCCHHHHHHHHHHHCCCCCCCCCCCCCEEECCCCCCCCCCEECCCEEECCCCCCCECCCCCECECCEEEEECCCCCCCHHHHHCCCCCCCCCCEECCCCCCHHHHHHHHHHHCCCCCCHHHHHHHCCCCCCCCCCCCCEECCC
CHHHHHHHHHEEEECCCCCCCCCCCEEEEEECCCHEEEEEEEEECCCCCCCCCCCCCEECCCCCCEEEEEEEECCCCCCCCCCHHHHHHHHHHEECCCCCCCCCCEEEEEECCECCCCCCCEEEEEEEEEEEEEEECCCCCEEEEEEEEEEEECCCHHHHCCCCCCCCCCCEEEEEEECCCCHHHHHHHHHHHCCCHHHHHHHHHCCCCCCCCCCCCCEEEEC
> 6f45_1
AVQGPWVGSSYVAETGQNWASLAANELRVTERPFWISSFIGRSKEEIWEWTGENHSFNKDWLIGELRNRGGTPVVINIRAHQVSYTPGAPLFEFPGDLPNAYITLNIYADIYGRGGTGGVAYLGGNPGGDCIHNWIGNRLRINNQGWICGGGGGGGGFRVGHTEAGGGGGRPLGAGGVSSLNLNGDNATLGAPGRGYQLGNDYAGNGGDVGNPGSASSAEMGGGAAGRAVVGTSPQWINVGNIAGSWL
CCCCCCHHHHHHHHHCCCEHHHHHHHCCCCCCCEEHHHHCCCCCCEEEEECCCEEEECHHHHHHHHHHCCCCCEEEEECCCEEECCCCCCCEEECCCCCCCCEEEEECCEEECCCCCCCECCCCCCCCCCCEEECCHHHEEEEECCEEECCCCCCCCEEECCEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEECCEECCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCEEEECCEEECCCC
CECCCCCCCCCCCCCCCCHHHHHHCEEEEECCCCCHHHHCCCCCCCCCCEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
> 6mxv_1
IXQHSSGFLKLVDDAKSRIQECSVDDIQKXNETQTLDGLLIDTREESEVANGYIPNAIHLSKGIIESAIESAVPNKNQKXYFYCGGGFRSALVADKLREXGYKNVISVDGGWRAWNAKGYPTVSPNQFRPNEFLKLVNNAKTQIKECSTTELYNKINSQELDGIVFDVREDSEFNRFHIQGATHLSKGQIEVKIENLVPNKQQKIYLYCGSGFRSALAAESLQHXGYTNVVSIAGGIKDWLANNYPVSQN
CCCCCHHHHHHHHHHHCCCEEECHHHHHHHHHHCCCCCEEEECCCHHHHCCCECCCEEECCCCCHHHHHHHHCCCCCCCEEEECCCCCHHHHHHHHHHHCCCCCEEEECCHHHHHHHCCCCCECHHHCCCHHHHHHHHHHHHHCEEECHHHHHHHHHCCCCCCEEEECCCHHHHHHCECCCCEECCCCCHHHHHHHHCCCCCCCEEEECCCCHHHHHHHHHHHHCCCCCEEEECCHHHHHHHCCCCCECC
CCCCCCCHHHHHHHHHCCCCCCCHHHHHHHHHCCCCEEEEEECCCHHHHHCCCCCCEEECCCCCEEEEHHHHCCCCCCEEEEECCCCCHHHHHHHHHHHCCCCEEEEEHCCHHHHHHCCCCECCCCCCCCHHHHHHHHHHCCCCCECCHHHHHHHHHCCCCCEEEEECCCHHHHHHCCCCCCEECCCCHHHHHHHHCCCCCCCEEEEEECCCCCEHHHHHHHHHCCCCEEEEHHCCHHHHHHCCCCECCC
> 5z82_1
SIGLAHNVTILGSGETTVVLGHGYGTDQSVWKLLVPYLVDDYKVLLYDHMGAGTTNPDYFDFDRYSSLEGYSYDLIAILEEFQVSKCIYVGHSMSSMAAAVASIFRPDLFHKLVMISPTPRLINTEEYYGGFEQKVMDETLRSLDENFKSLSLGTAPLLLACDLESAAMQEYCRTLFNMRPDIACCITRMICGLDLRPYLGHVTVPCHIIQSSNDIMVPVAVGEYLRKNLGGPSVVEVMPTEGHLPHLSMPEVTIPVVLRHIRQDITD
CHHHHCCCEEEECCCCEEEEECCCCCCHHHHCCCHHHCCCCCEEEEECCCCCCCCCHHHCCCCHHHCCHHHHHHHHHHHHHCCCCCEEEEEECHHHHHHHHHHHHCHHHEEEEEEECCCCCCECCCCCCCCECHHHHHHHCCCHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHCCHHHHHHHHHHHHCCCCHHHHHHCCCCEEEEEEEECCCCCHHHHHHHHHHCCCCEEEEEEEEECCCHHHHCHHHHHHHHHHHHHCCCCC
CHHHHCCEEEECCCCCEEEEECCCCCCHHHHHEECHHHHHCCEEEEEEECCCCCCCCCCCCHHHHCCCHCCHHHHHHHHHHHCCCCEEEEEEEHHHHHHHHHHHHCHHHHHEEEEECCCCCEECCCCCCCCCCHHHHHHHHHHHHHHHHHHHHCCCHEECCCCCCHHHHHHHHHHHCCCCCHHHHHHHHEEECCCCHCCCCCCCCCEEEEECCCCEECCEHHHHHHHHHCCCCCEEEEECCCCCCCCCCCCHHHHHHHHHHHHHCHCC
> 6rap_5
SNYQTLVDVNNAMNKMLRAYVNEAVAIRFDLPDTQADAAISVFLYDIHEDLQLRTAESRGFNAGAGRLLPGWVNVKCNYLITYWESPDSQPDNQAIQVMSQVLAALINNRQLADIGAYTQVMPPKENLNSLGNFWQSLGNRPRLSLNYCVTVPISLSDKGEEMTPVKSLSTTVEPKAPLSPLVITDALREQLRVALDACLAMTHVNLDSSPVANSDGSAAEIRVSLRVYGMTPTEYLAPMNTVFNEWEKSEAAAVTPDGYRVYINAVDKTDLTGI
CCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCEEECCCCECHHHCCCCCCCCCCCCCCCCCCCEEEECCEEECCCCCCCCCCCCCCCCCHHHHHHHHHHCCCCCCCCCEEECCCCCCCHHHHHHHHHHCCCCCCCCEEEEEEEEECCCCCCCCCCECCCEEEEEEECCCCCHHHHHHHHHHHHHHHCCHHHHHHHECCEEEEECCCCCCCCCCEEEEECEECCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCCEECCCCCCECCCC
CCCHHHHHHHHHHHHHHHHCCCCCEEEEECCCCCCCCCEEEEEEEEHHCCCHHCCCCCCCCCCCCCCCCCCCHHECHEEEEEECCCCCCCCCHHHHHHHHHHHHHHHCCCCCCHCCCCCEECCCCCCCCCHHHHHHHCCCCCCEEEEEEEEEEECCCCCCCCCCCEEEEEEEECCCCCCCCHHHHHHHHHHHHHHHHHHHEECEECCCCCCCCCCCCCCCCCEEEEEECCCCCHHHHHHHHHHHHHHCCCCEEEEEECCCCEEEEEECCCCCCCC
> 6d2v_1
GLVPRGSHMEGKKILVTGGTGQVARPVAEALAERNEVWCLGRFGTPGVEKELNDRGITTFHWDMDDPGAAAYEGLPDDFTHVLHSAVRRGEDGDVNAAVEVNSVACGRLMTHCRGAEAFLFVSTGALYKRQTLDHAYTEDDPVDGVADWLPAYPVGKIAAEGAVRAFAQVLNLPTTIARLNIAYGPGGYGGVPMLYFKRMLAGEPIPVPKEGQNWCSLLHTDDLVAHVPRLWEAAATPATLVNWGGDEAVGITDCVRYLEELTGVRARLVPSEVTRETYRFDPTRRREITGPCRVPWREGVRRTLQALHPEHLPS
CCCCHHHHCCCCEEEEECCCCCCHHHHHHHHHCCCEEEEEECCCCCCHHHHHHHCCCEEEECCCCCCCHHHHCCCCCCCCEEEECCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEEEEEHHHECCCCCCCEECCCCCECCCCCCCCHHHHHHHHHHHHHHHHHHHHCCCEEEEEECCEECCCCCCEHHHHHHHHHHHCCCEEEECCCCCEECCEEHHHHHHHHHHHHHCCECCCEEEEECCCCCEEHHHHHHHHHHHHCCCCCEEEECCCCCCCEECCHHHHHHHCCCCCCHHHHHHHHHHHHCHHHCCC
CCCCCCCCCCCCEEEEECCCCCEHHHHHHHHHHCCCEEEEECCCCHHHHHHHHHCCCCEEEEEHCCHHHHHHHCCCCCCCEEEEEHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHEEEECCCCEECCCCCCCCCEECCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCEEEEEEEEECCCCCCCCCHHHHHHHHHCCCCEEECCCCCCCCCCEEEHHHHHHHHHHHHCCCCCCEEEEECCCCCEHHHHHHHHHHHHCCCECCECCCCCCCCCCCCHHHHHHHHCCCCCCCHHHHHHHHHHHHCCCCCCC
> 6q64_1
LIPLIEAQTEEDLTPTXREYFAQIREYRKTPHVKGFGWFGNWTGKGNNAQNYLKXLPDSVDFVSLWGTRGYLSDEQKADLKFFQEVKGGKALLCWIIQDLGDQLTPKGLNATQYWVEEKGQGNFIEGVKAYANAICDSIEKYNLDGFDIDYQPGYGHSGTLANYQTISPSGNNKXQVFIETLSARLRPAGRXLVXDGQPDLLSTETSKLVDHYIYQAYWESSTSSVIYKINKPNLDDWERKTIITVEFEQGWKTGGITYYTSVRPELNSXEGNQILDYATLDLPSGKRIGGIGTYHXEYDYPNDPPYKWLRKALYFGNQVYPGKFD
CCCHHHCCCHHHCCHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCCCHHHCHHHCCCCCCEEECCCCCCCCCHHHHHHHHHHHHCCCCEEEEEEEECCCCCCCCCCCCCHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCCCCCCCCCCCECCCCCHHHHHHHHHHHHHHCCCCCEEEEEECHHHECHHHHCCCCCEEEECCCCCEHHHHHHHHCCCCCCCHHHHEEEEEEHHHHCCCCCCCCCECCCHHHHHCCCHHHHHHHCCECCCCCECCEEEEECHHHHCCCCCCCHHHHHHHHHHHHHCCCCCC
CCCCCCCCCHHCCCHHHHHHHHHHHHHHCCCCCEEEEEECCCCCCCCCCCCCHCCCCCCCEEEEECCCCCCCCHHHHHHHHHHHHCCCCEEEEEEHHHHCCCCCCCCCCCCEHCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCCEEEEECCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCCCEEEECCCCCCCHHHHHHHHHHEEEEHCCCCCCCCCCCCCCCCCCCCCCCEEEEEECHHHHHCCCCCCCCCCCCCCCCCHCCHHHHHHHHHCCCCCCCCCCEEEECCCHCHCCCCCHHHHHHHHHHHCCCCCCCCC
> 6n91_1
MITSSLPLTDLHRHLDGNIRTQTILELGQKFGVKLPANTLQTLTPYVQIVEAEPSLVAFLSKLDWGVAVLGDLDACRRVAYENVEDALNARIDYAELRFSPYYMAMKHSLPVTGVVEAVVDGVRAGVRDFGIQANLIGIMSRTFGTDACQQELDAILSQKNHIVAVDLAGDELGQPGDRFIQHFKQVRDAGLHVTVHAGEAAGPESMWQAIRDLGATRIGHGVKAIHDPKLMDYLAQHRIGIESCLTSNLQTSTVDSLATHPLKRFLEHGILACINTDDPAVEGIELPYEYEVAAPQAGLSQEQIRQAQLNGLELAFLSDSEKKALLAKAALRG
CCCCCCCCEEEEEEHHHCCCHHHHHHHHHHHCCCCCCCCHHHHHHHHCCCCCCCCHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHCCCCEEEEEECHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHCCEEEEEEEEEHHHCHHHHHHHHHHHHCCHHHCCEEEEECCCCCCCHHHHHHHHHHHHHCCCEEEEEECCCCCHHHHHHHHHHCCCCEEEECHHHHHCHHHHHHHHHHCCEEEECHHHHHHCCCCCCHHHCCHHHHHHCCCCEEECCECHHHHCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHCCC
CHHCCCCHHHEEEECCCCCCCHHHHHHHHHCCCCCCCCCHHHHHHHEEECCCCCCHHHHHHHHCCCEEECCCHHHHHHHHHHHHHHHHHHCCEEEEEEECHHHHHHCCCCCHHHHHHHHHHHHHHHHHHCCCEEEEEEECCCCCCHHHHHHHHHHHHHCCCCEEEEECCCCCCCCCHHHHHHHHHHHHHCCCCEEEECCCCCCCHHHHHHHHHHCHHECCCCEEHHCCHHHHHHHHHCCCCEEECCCCCHHHCCCCCHHHCCHHHHHHCCCEEEEECCCCCCCCCCCHHHHHHHHHHCCCCHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHCCC
> 6cvz_1
KHKYHFQKTFTVSQAGNCRIMAYCDALSCLVISQPSPQASFLPGFGVKMLSTANMKSQYIPMHGKQIRGLAFSSYLRGLLLSASLDNTIKLTSLETNTVVQTYNAGRPVWSCCWCLDEANYIYAGLANGSILVYDVRNTSSHVQELVAQKARCPLVSLSYMPRAASAAFPYGGVLAGTLEDASFWEQKMDFSHWPHVLPLEPGGAIDFQTENSSRHCLVTYRPDKNHTTIRSVLMEMSYRLDDTGNPICSCQPVHTFFGGPTAKLTKNAIFQSPENDGNILVCTGANSALLWDAASGSLLQDLQTDQPVLDICPFEVNRNSYLATLTEKMVHIYKWE
CCCEEEEEEEECCCCCCCCCEEEECCCCEEEEEEECCCCCCCCCEEEEEEECCCCCCEEEEEECCCEEEEEECCCCCCEEEEEECCCEEEEEECCCCEEEEEEECCCCEEEEEECCCCCCEEEEEECCCCEEEEECCCCCCCCEEECCCCCCCCEEEEEEECCCCCCCCCCCEEEEEECCEEEEEEECCCCCEEEEECCCCEEEEEEEEEECCCCEEEEEEEECCCCCCCEEEEEEEEEEECCCCCEEEEEEEEEEEECCCCCCCCECEEEECCCCCCCEEEEECCCEEEEEECCCCCEEEEEECCCCCCEEEEEEECCEEEEEEECCCEEEEEEEC
CCEEEEEEEEEEECCCCCEEEEECCCCCEEEEECCCCCCCCCCCCCEEEECCCCCCEEEECCCCCCCEEEEECCCCCCCEEEEHCCCCEEEEECCCCCEEEEEECCCCCEEEECCCCCCCEEEEECCCCEEEEEECCCCCCCCHEECCCCCCCCEEEEEECCCCCCCCCCCCCEEEEECCCCEEEECCCCCCCCCCCCCCCCCCEEEEECCCCCCEEEEEECCCCCCCCCCEEECCCCCCCCCCCCCCECCCEEEEECCCCCCECCCCEEEECCCCCCEEEEECCCCEEEEEECCCCCEEEECCCCCCEEEEEEECCCCCCEEEEEHHHHEEEEECC
> 6ek4_1
VVYPEINVKTLSQAVKNIWRLSHQQKSGIEIIQEKTLRISLYSRDLDEAARASVPQLQTVLRQLPPQDYFLTLTEIDTELEDPELDDETRNTLLEARSEHIRNLKKDVKGVIRSLRKEANLMASRIADVSNVVILERLESSLKEEQERKAEIQADIAQQEKNKAKLVVDRNKIIESQDVIRQYNLADMFKDYIPNISDLDKLDLANPKKELIKQAIKQGVEIAKKILGNISKGLKYIELADARAKLDERINQINKDCDDLKIQLKGVEQRIAGIEDVHQIDKERTTLLLQAAKLEQAWNIFAKQLQNTIDGKIDQQDLTKIIHKQLDFLDDLALQYHSMLLS
CCCCCCCHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHCCCECCCCCHHHHCCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCCEHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHCCCCC
CCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCC
> 6rap_3
TTTYPGVYLSEDAVSSFSVNSAATAVPLFAYDSENTNTINKPIQVFRNWAEFTVEYPTPLEDAFYTSLSLWFMHGGGKCYLVNEANIADAVAQYDDITLIVAAGTDTTTYTAFTTVVGQGYRIFGLFDGPKEKIAGTAKPDEVMEEYPTSPFGAVFYPWGTLASGAAVPPSAIAAASITQTDRTRGVWKAPANQAVNGVTPAFAVSDDFQGKYNQGKALNMIRTFSGQGTVVWGARTLEDSDNWRYIPVRRLFNAVERDIQKSLNKLVFEPNSQPTWQRVKAAVDSYLHSLWQQGALAGNTPADAWFVQVGKDLTMTQEEINQGKMIIKIGLAAVRPAEFIILQFSQDI
CCCCCCCCCCCCCCCCCCCCCCCCCCCEEEECCCCCCCCCCCEEEECCHHHHHHHCCCCCCCCCHHHHHHHHCCCCCCEEEEECCCHHHHHHHHCCCCCEEECCCCCCHHHHHHHHHHCCCCCCEECCCCCCCCCCCCCHHHCCCCCCCCCCEECCCCCECCCCCCCECHHHHCCHHHHHHHCCCCCCCCCCCCECCCCCECCCCCHHHHCCCCCCCCCCEEECCCCCCCEEECCCCCCCCCCCCCHHHHHHHHHHHHHCCCCCHHHCCCECCHHHHHHHHHHHHHHHHHHHHCCCECCCCHHHHEEEECCCCCCCCHHHHCCCCEEEEEEECECCEECEEEEEEECCC
CCCCCCEEEEECCCCCCCHCCCCCCEEEEEECCCCCCCCCCCEEEECCHHHHHHHCCCCCCHHHHHHHHHHHHCCCCCCEEEECCCCHHHHHHHCCCEEEECCCCCHHHHHHHHHHHHHHCCCEEEECCCCCCCCCCCCHHHHCCCCCCCHHHHHHCHHHECCCCEECCCCHHHHCEEECCCCCCCCEECCCEEEHCCCCCCEEECCHHHHCCCCCCCEEEEECCCCCCEEEEEEEECCCCCCEEEEEHHHHHHHHHHHHHHHHCEEEECCCCCHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHEEEEECCCCCCCHHHHHCCCEEEEEEEECCCCCEEEEEEHHCCC
> 6cci_1
VELPPEEADLFTGEWVFDNETHPLYKEDQBEFLTAQVTCMRNGRRDSLYQNWRWQPRDASLPKFKAKLLLEKLRNKRMMFVGDSLNRNQWESMVBLVQSVVPPGRKSLNKTGSLSVFRVEDYNATVEFYWAPFLVESNSDDPNMHSILNRIIMPESIEKHGVNWKGVDFLVFNTYIWWMNTFAMKVLRGSFDKGDTEYEEIERPVAYRRVMRTWGDWVERNIDPLRTTVFFASMSPLHIKSLDWENPDGIKDALETTPILNMSMPFSVGTDYRLFSVAENVTHSLNVPVYFLNITKLSEYRKDAHTSVHTIRQGKMLTPEQQADPNTYADDIHWCLPGLPDTWNEFLYTRIISR
CCCCCCCCCCCCEEEEECCCCCCCCCHHHCCCCCCCCCCCCCCCCCCHHHHEEEEECCCCCCCCCHHHHHHHCCCCEEEEEECHHHHHHHHHHHHHHHCCCCCCCEEEEEECCEEEEEECCCCEEEEEEECCCCCCECCCCCCCCCCCCCEECCCCCHHHHHHHCCCCEEEECCCHHHCCCCEEEEECCCHHHCCCCEEEEEHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHCCCCEEEECCHHHHHCCCCCEEECCCEECCEECCHHHHCCHHHHCEEEEECCCCHHHHHHHHHHHHHHCC
CCCCCCCCCCCCCEEEECCCCCCCCCCCCCCCCCCHHHHHHCCCCCHHHHCEEECCCCCCCCCHCHHHHHHHHCCCEEEEECCCCCCCCEEEHHEHHHHCCCCCCCCCCCCCCEEEEEEHHCCCEEEEHECCEEEECCCCCCCCCCCCHHEEECCHHHHHHHCCCCCEEEEEEEHEHECCCCCEEEEECCCCCCCHEEEEECHHHHHHHHHHHHHHHHHHCCCCCCCEEEEEECCCCCCCHHCCCCCCCCCCECCCCCCCCCCCCCCCCCCHHHHHHHHHHHHCCCCCEEEEEEHHHHHHHHCCCCCHEHHCCCCCCCHHHHCCCCCHCCEHHEECCCCCCHHHHHHHHHHHCC
> 6cl6_1
ALAATDIPGLDASKLVSGVLAEQRLPVFARGLATAVSNSSDPNTATVPLMLTNHANGPVAGRYFYIQSMFYPDQNGNASQIATSYNATSEMYVRVSYAANPSIREWLPWQRCDIGGSFTKEADGELPGGVNLDSMVTSGWWSQSFTAQAASGANYPIVRAGLLHVYAASSNFIYQTYQAYDGESFYFRCRHSNTWFPWRRMWHGGDFNPSDYLLKSGFYWNALPGKPATFPPSAHNHDVGQLTSGILPLARGGVGSNTAAGARSTIGAGVPATASLGASGWWRDNDTGLIRQWGQVTCPADADASITFPIPFPTLCLGGYANQTSAFHPGTDASTGFRGATTTTAVIRNGYFAQAVLSWEAFGR
CCCHHHCCCEEHHHEEECCCCHHHCCHHHHCECCCCCCCCCHHHCCCCEEEECCCCCCCCCCCEEEEEEEECCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCECCCEECCHHHECECCCCEECCCCCEHHHCCCCEEEEECCHHHHHHCECCCCCCCEEEEEEEEECCEEEEEEEECCCCCEEEEEEECCEECCCEEECECCCCCHHHECEHHHCCHHHCCCCCCCECECCCCEEHHHEEECCCCHHHCCCCCCCHHHHHHHHCCCCCCEEECCCCEEEEECCCCEEEEEEEEEECCCEEEEEECCCCCCCCEEEEEEEECCCCCCCCCCCEEEECCCCCEEEEEECCCCCEEEEEEEEEC
CHHHCCCHHHHHHHHHCCCHHHCCCCHHHHHHHHHCCCCCCCCHHCCCCEEECCCCCCCCCCCCCHHHECCCCCCCCHHHEEEHHCCCHHHHHHHHHHCCCCCCCCCCHHHHCCCCCCCCCHHCCCCCCCCHCHHCCCCEEECCHHHHHCCCCCCCHHHHHHEEEECCCCCCEEEEEEECCCCCEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCEEEEEEEEEECCCCCCEEEECECCCCCEEEEEEEECCCCCCCCCHCEEEECCCHCEEEEECCCCCCCCEEEEEECC
> 6cl5_1
ALAATDIPGLDASKLVSGVLAEQRLPVFARGLATAVSNSSDPNTATVPLMLTNHANGPVAGRYFYIQSMFYPDQNGNASQIATSYNATSEMYVRVSYAANPSIREWLPWQRCDIGGSFTKTTDGSIGNGVNINSFVNSGWWLQSTSEWAAGGANYPVGLAGLLIVYRAHADHIYQTYVTLNGSTYSRCCYAGSWRPWRQNWDDGNFDPASYLPKAGFTWAALPGKPATFPPSGHNHDTSQITSGILPLARGGLGANTAAGARNNIGAGVPATASRALNGWWKDNDTGLIVQWMQVNVGDHPGGIIDRTLTFPIAFPSACLHVVPTVKEVGRPATSASTVTVADVSVSNTGCVIVSSEYYGLAQNYGIRVMAIGY
CCCHHHCCCEEHHHEEECCCCHHHCCHHHHCECCCCCCCCCHHHCCCCEEEECCCCCCCCCCCEEEEEEEECCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCECCCEECCHHHECECCCCEECCCCCEHHHCCCCEEEEECCHHHHHCCECCCCCCCEEEEEEEEECCEEEEEEEECCCCEEEEEEECCEECCCEEECECCCCCHHHECEHHHCCCCCCCCCCCCECECCCCEEHHHEEECCCCHHHCCCCCCCHHHHHHHHCCCCCCEEECCCCEEEEECCCCEEEEEEEEEEECCCCCCEEEEEECCCCCCCCEEEEEEEEEEEECCCCCHHHEEEEEEEECCCEEEEEEEECCCCCCEEEEEEEEEEC
CHHCCCCHHHHHHHHHCCHHHHHCCCHHHHHHHHHCCCCCCHHHHCCCHEEECCCCCCCCCCCCEHHEEEECCCCCCHHEEEEECCCCCHHHHHHHHCCCCCCCCCCCHHHHCCCCCCCCCCCCCCCCCCCCHHHHCCCEEECCHHHHHCCCCCCCHHHCCEEEEECCCCCEEEEEEECCCCCEEEEEECCCCCCCCEECCCCCCCCCCCCCCCCCCCCCCHCCCCHCCCCCCCCCCCHHCCCCCCCHCCCCCCCHHCCCCCCCCCCCCCCCCCHCCCCCCECCCCCEEEEEEEEEECCCCCCCCCCEEEEEECCCCCCEEEEEEECCCCCCCCCCCHEEEECCCCCCCEEEEECCCCCCCCCCCCEEEEEECC
> 6hrh_1
LYFQSMFSYDQFFRDKIMEKKQDHTYRVFKTVNRWADAYPFAQHFSSKDVSVWCSNDYLGMSRHPQVLQATQETLQRHGVGAGGTRNISGTSKFHVELEQELAELHQKDSALLFSSCFVANDSTLFTLAKILPGCEIYSDAGNHASMIQGIRNSGAAKFVFRHNDPDHLKKLLEKSNPKIPKIVAFETVHSMDGAICPLEELCDVSHQYGALTFVDEVHAVGLYGSRGAGIGERDGIMHKIDIISGTLGKAFGCVGGYIASTRDLVDMVRSYAAGFIFTTSLPPMVLSGALESVRLLKGEEGQALRRAHQRNVKHMRQLLMDRGLPVIPCPSHIIPIRVGNAALNSKLCDLLLSKHGIYVQAINYPTVPRGEELLRLAPSPHHSPQMMEDFVEKLLLAWTAVGLPLQCRRPVHFELMSEWERSYFGNM
CCCCCCCCHHHHHHHHHHHHHHHCCCCCCCEEEECCCCCCEEEEECCEEEEECCCCCCCCHHHCHHHHHHHHHHHHHHCCCCCCCCCCCCECHHHHHHHHHHHHHCCCCEEEEECCHHHHHHHHHHHHHHHCCCCEEEEECCCCHHHHHHHHHHCCEEEEECCCCHHHHHHHHHCCCCCCCEEEEEECECCCCCCECCHHHHHHHHHHCCEEEEEECCCCCCCCCCCCCCHHHHCCCHHHCCEEEEECCCCCCCCCEEEEECHHHHHHHHHHCHHHHCCCCCCHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHCCCCECCCCCCCEEEECCCHHHHHHHHHHHHHCCCEECCEECCCCCCCCCCEEEECCCCCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHCCCC
CCCCCCCCHHHHHHHHHHHHHCCCCEEEHHHHHHHCCCCCCEECCCCCCEEEEECCCECCCCCCCHHHHHHHHHHHCCCCCCCCCEEECCCCEEHHHHHHHHHHHHHHHHHHHHHCHHEHCHHHHHHHHHHCCCEEEEECCCCCHHHHHHHECCCCCEEEEEECCHHHHHHHHHHCCCCCCEEEEEEEEECCCCCCCCHHHHHHHHHHHCCEEEEHHHHHCEEECCCCCCEECCCCCCCCEEEHHCCCHHHHCCCCCEHHCCHHHHHHHHCCCCCEEEEECCCHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHHCCCCECCCCCCEEEEEECCCHHHHHHHHHHHHHCCCEEECECCCCCCCCHEEEEECCCCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCCHHHHHHHHHHHCCCCCC
> 6m9t_1
DCGSVSVAFPITMLLTGFVGNALAMLLVSRSYRRRESKRKKSFLLCIGWLALTDLVGQLLTTPVVIVVYLSKQRWEHIDPSGRLATFFGLTMTVFGLSSLFIASAMAVERALAIRAPHWYASHMKTRATRAVLLGVWLAVLAFALLPVLGVGQYTVQWPGTWAFISTNWGNLFFASAFAFLGLLALTVTFSCNLATIKALVSRGSNIFEMLRIDEGLRLKIYKDTEGYYTIGIGHLLTKSPSLNAAKSELDKAIGRNTNGVITKDEAEKLFNQDVDATVRGILRNAKLKPVYDSLDAVRRAALINMVFQMGETGVAGFTNSLRMLQQKRWDEAAVNLAKSRWYNQTPNRAKRVITTFRTGTWDAYGSWGRITTETAIQLMAIMCVLSVCWSPLLIMMLKMIFNEKQKECNFFLIAVRLASLNQILDPWVYLLLRKILGRPLEVL
CCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCHHHHHHCCCCHHHHHHHHHHHHHHHHHHCHHHHCCCCEEEECCCCEEEECCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHCCEEEEEECCCCCEEEECCEEEECCCCHHHHHHHHHHHHCCCCCCECCHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHCCHHHHHHHHHHHHHHCHHHHHCCHHHHHHHHCCCHHHHHHHHHCCHHHHHCHHHHHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHC
CCCCCCCEEEHHEEHHHHHHHHHHHHHHHHHHHCCCCCCHCEEEEEHHHHHHHHHHHHECCCCEEEEEHHHCCCCECCCCCCCHHHHCCEEEEHHHHCHHHHHHHHHHHHHHHCCCCHHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHCCCCCEEEECCCEEEEEECCCCHHHHHHHHHHHHHHHHHHEHHEEHEEEHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHEEEEEEEEEEEEEEECHHEEEHCCCCCCCCCCHHHHHHHHHHHHHHHCCHHHHHHCHHHHHHHHHHHCCCCCCCCCCCEEEECCCCHHHHHHHHHHHHHCCCCCCCCCCCEEEECCCCCCCCHHEEEHHCCCCCCCCCCCCCEEHHHHHHHHHEEEEEEECCCHHHHHHEEEECCCCCCCHHHHHHEHHHHCHCHHHCCHHHEHHHHHHHHHHHCC
> 5w6l_1
NAQELKERAKVFAKPIGASYQGILDQLDLVHQAKGRDQIAASFELNKKINDYIAEHPTSGRNQALTQLKEQVTSALFIGKXQVAQAGIDAIAQTRPELAARIFXVAIEEANGKHVGLTDXXVRWANEDPYLAPKHGYKGETPSDLGFDAKYHVDLGEHYADFKQWLETSQSNGLLSKATLDESTKTVHLGYSYQELQDLTGAESVQXAFYFLKEAAKKADPISGDSAEXILLKKFADQSYLSQLDSDRXDQIEGIYRSSHETDIDAWDRRYSGTGYDELTNKLASATGVDEQLAVLLDDRKGLLIGEVHGSDVNGLRFVNEQXDALKKQGVTVIGLEHLRSDLAQPLIDRYLATGVXSSELSAXLKTKHLDVTLFENARANGXRIVALDANSSARPNVQGTEHGLXYRAGAANNIAVEVLQNLPDGEKFVAIYGKAHLQSHKGIEGFVPGITHRLDLPALKVSDSNQFTVEQDDVSLRVVYDDVANKPKITFKG
CCCCHHHHHHCCCCCCCHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHCHHHCCCCCCCCCCCCCCCCCCCEEEECCCCHHHHHHHHHHHHHCCCCCCEEEECCCCEEEECCCHHHHHHCCCCHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHECHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHHHCCCCCCCHHHHHHHCCCHHHHHHHHHCCCCEEEECCCCCCCCHHHHHHHHCHHHHHHHCEEEEEECCCCCCCHHHHHHHHHHHCCCCHHHHHHHHHCCCCHHHHHHHHHCCCEEEECCCHHHCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCHHHHCCEEECCCEECCHHHHHCCCEEEECCCCCEEECCCCHHHCCECCCCCCCCCCCCCC
CCHHHHHHHCECCCCCCCHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHCCCCHCCCCCCCCCCCCCCCCCCEEEEEHCCCHHHHHHHHHHHHHHHCCCCEEECCCCCEEEECCCHHHHHHCCCCHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHCCCCCHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCEEEECCCCCHHCHHHHHHHHHHHHHHCCCCEEEEHHHHHHHHHHHHHHHHCCCCCCHHHHHHHCCCCCHHHHHHHHHHCCCEEEEECCCHHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEEEECCCCHCCCCCCCCCCCCHHHHCCCEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCEEECC
> 6rbk_3
HITLDIAGQRSTLGIRRLRVQQLINEIPLAQLELHIPTDNHGAADNAVQHEVSRFTLGVRVGIAQDNKPLFDGYLVQKKMQLKGKEWSVRLEARHALQKLTFLPHSRVFRQQDDSTVMKGLLQSAGVKLTQSKHDQLLQFRLSDWQFIRSRLLSTNCWLLPDAASDTVVIRPLSSRTLARDSHDYTLYEINLNFDNRFTPDSLSLQGWDIAAQRLTAAQKSPAGAFRPWKPAGQDYALAFSMLPEATLQTLSNSWLNYQQMTGVQGHIVLAGTRDFAPGESITLSGFGAGLDGTAMLSGVNQQFDTQYGWRSELVIGLPASMLEPAPPVRSLHIGTVAGFTADPQHLDRIAIHLPALNLPDSLIFARLSKPWASHASGFCFYPEPGDEVVVGFIDSDPRYPMILGALHNPKNTAPFPPDEKNNRKGLIVSQADQTQALMIDTEEKTLRLMAGDNTLTLTGEGNLTMSTPNALQLQADTLGLQADSNLSIAGKQQVEITSAKINM
CEEEEECCECCCCCEEEEEEEECCCCCCEEEEEEECCCCCECHHHHCCHHHHCCCCCCCEEEEEECCEECCCEEEEEEEECCCCCCCEEEEEEECHHHHHCCCCECCCCCCCCHHHHHCCCHHHCCCEEECCCCCCCCCCCECCCCCCCCCCCCCCEECCECCCCCEEECEECCCCCECCCCCCCCEEEEEECCCCCCCCCCCEEEECCCCCCCCCCCECCCCCCCCCEEECCCCCEEEECCCCCCCCHHHHHHHHHHHHCCCCCEEEEECCCCCCCCCCCEEECCCCCCCCEEECEEEEECCEECCCCECCEEEECCCCCCCCCCCCCCCCEEECECCCCCCCCCCCCEEECCCCCCCCCCCEEEEECCCCCCCCCCCCCCCCCCCCEEEEEHHHCCCCEEEEECCCCCCCCCCCCCCCCCCECCCCCECCCCECCEECECCCCCCEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CEEECCCCCCCCEEEEEEEEEHHHCCCCEEEEEEECCCCCCCCCCCCHHCCCCCCEEEEEECCCCCCEEEEEEEEEEEEEEECCCCEEEEEEECCHHHHHHCCCCEEEEHCCCHHHHHHHHHHHCCCECCCCCEHEEEECCCCHHHHHHHHHHHCCEEEEECCCCEEEECCCCCCCCCCEHCCCCCHEEEEHHHCHHCCCHHEEEEECCCCHHHHEEEECCCCCCCCCCCCCHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHEEEEEEEECCCECCCCCEEEECCCCCCCCCCEEEEEEEEEEECCCCEEEEEEEECCCCCCCCCCCCCCCEEEEEECCCCCCCCCCEEEEEEEEECCCCCEEEEEEEECCCCCCCCEEEECCCCCEEEEEECCCCCCCCEEEECCCCCCCCCCCCCCCCCCEEEEEEECCCCEEEEEECCCCCEEEEECCCCEEEEECCCCEEEECCCCEEEECCCEEEEECCCEEEEECCEEEEEECEEEC
> 6n9y_1
MERFLRKYNISGDYANATRTFLAISPQWTCSHLKRNCLFNGMCAKQNFERAMIAATDAEEPAKAYRLVELAKEAMYDRETVWLQCFKSFSQPYEEDIEGKMKRCGAQLLEDYRKNGMMDEAVKQSALVNSERVRLDDSLSAMPYIYVPIKEGQIVNPTFISRYRQIAYYFYSPNLADDWIDPNLFGIRGQHNQIKREIERQVNTCPYTGYKGRVLQVMFLPIQLINFLRMDDFAKHFNRYASMAIQQYLRVGYAEEVRYVQQLFGKIPTGEFPLHHMMLMRRDFPTRDRSIVEARVRRSGDENWQSWLLPMIIVREGLDHQDRWEWLIDYMDRKHTCQLCYLKHSKQIPTCGVIDVRASELTGCSPFKTVKIEEHVGNNSVFETKLVRDEQIGRIGDHYYTTNCYTGAEALITTAIHIHRWIRGCGIWNDEGWREGIFMLGRVLLRWELTKAQRSALLRLFCFVCYGYAPRADGTIPDWNNLGSFLDTILKGPELSEDEDERAYATMFEMVRCIITLCYAEKVHFAGFAAPACESGEVINLAARMSQMRMEY
CHHHHHHHCCCHHHHHHHHHHHHHCCCCCCCCCCHHHECCCCECCCCHHHHHHHCCCCCCHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCHHHHHHCCCCCCCCCCCCCCCCEECHHHHCEEECCCCECCCCEEEECCCCEEEEECCCCCCEEECCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCEEEECCCHHHHCCHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHCCCCCCCCCCHHHHCCCCCCCCCCCCHHHHHHCCCCCCCHHHCCEECHHHHHHCCCCCCHHHHHHHHHHCCCCCHHHHHCCCCCCEEEEECHHHHHHCCCCCEEEEEEEEECCCCCCCCCCCCCCEEEEEECCEEEEEECCCCCHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHCCC
HHHHHHHCCCCCHHHHHHHHHHHHCCCCECCCCCCCEECCCEEHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHCCHHHHHHHCCCCCCCCEEEECCHCCCHEEEEEECCCCCCCCCCHEECCCEEEEEECCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCEEEEEEEHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHEECCCCCCHHHHHHHHCCCCCCCCHEHHHHHHCCCCCCCHHHHHHHHHHHHHHHCCCCCHHHHHHHHCCCCCHHHHHHHHHCCCCEEEEEEEEHHHHCCCCHHHEEEEEECCCCCCCEEEEECCCCEEEHCCCEEEEEECCCCHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHECCCCCHHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHECCCCCCHHHHHHHHHHHHHHHHHHHEHHCCCCEEEECCCCCCCCCCHHHHHHHHHHHHHHHC
> 6nq1_1
AARWDLCIDQAVVFIEDAIQYRSINHRVDASSMWLYRRYYSNVCQRTLSFTIFLILFLAFIETPSSLTSTADVRYRAAPWEPPAGLTESVEVLCLLVFAADLSVKGYLFGWAHFQKNLWLLGYLVVLVVSLVDWTVSLSLVAHEPLRIRRLLRPFFLLQNSSMMKKTLKCIRWSLPEMASVGLLLAIHLCLFTMFGMLLFAGLTYFQNLPESLTSLLVLLTTANNPDVMIPAYSKNRAYAIFFIVFTVIGSLFLMNLLTAIIYSQFRGYLMKSLQTSLFRRRLGTRAAFEVLSSMVGAVGVKPQNLLQVLQKVQLDSSHKQAMMEKVRSYGSVLLSAEEFQKLFNELDRSVVKEHPPRPEYQSPFLQSAQFLFGHYYFDYLGNLIALANLVSICVFLVLDADVLPAERDDFILGILNCVFIVYYLLEMLLKVFALGLRGYLSYPSNVFDGLLTVVLLVLEISTLAVYRLSLWDMTRMLNMLIVFRFLRIIPSMKPMAVVASTVLGLVQNMRAFGGILVVVYYVFAIIGINLFRGVIVALSAPBGSFEQLEYWANNFDDFAAALVTLWNLMVVNNWQVFLDAYRRYSGPWSKIYFVLWWLVSSVIWVNLFLALILENFLHKW
CHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHCCHHHHHHHHHHHHHHCCHHHHCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHCCCHHHHHHCCCCHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHCCCCCCCCCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCECHHHHHHHHHHCCCCHHHHHHHHHHHHHCCCCCECHHHHHHHHHHHHCCCCCCCCCCCCCCHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCHHHCCCCCHHHHHHHHHHHHCCCCCHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCC
CCCHHHHHHHHHEEHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHEEHHCHHHHHHCHHHEEEEEEEEEHHHHHHEEEHHCCCCCEEEHHHHCHEEEHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHEEECCCCCCCECHCHHHCHHHHEEEEEEEEEEEEHHHHHHHHEEHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHEECCCCCCCEHHHHHHHHHCCCCCCHHHHHHHHHHCCCCCCCECHHHHHHHHHHHHCCEEECCCCCCCCCCCHHHHHHHHEECHHHHEEEEEEEHHHHEEEEEEEHHCCCCCCCCCCCCEEHHHHHEEEHHHHHHHHHHHHHHCHHHHHCCCCCCCCEEEEEEEHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHHHEEEEHHHHHHHHHHHHCCECCCCCCCCCCHHHHHHHHHCHHHHHHHHHHHHHHEEECCCEEEHHHHHHHCCCHHHHEEEEEEEEEEEEEEEEEEEEHHHHHHECC
> 6dru_1
TYFAPNSTGLRIQHGFETILIQPFGYDGFRVRAWPFRPPSGNEISFIYDPPIEGYEDTAHGMSYDTATTGTEPRTLRNGNIILRTTGWGGTTAGYRLSFYRVNDDGSETLLTNEYAPLKSLNPRYYYWPGPGAEFSAEFSFSATPDEQIYGTGTQQDHMINKKGSVIDMVNFNSYIPTPVFMSNKGYAFIWNMPAEGRMEFGTLRTRFTAASTTLVDYVIVAAQPGDYDTLQQRISALTGRAPAPPDFSLGYIQSKLRYENQTEVELLAQNFHDRNIPVSMIVIDYQSWAHQGDWALDPRLWPNVAQMSARVKNLTGAEMMASLWPSVADDSVNYAALQANGLLSATRDGPGTTDSWNGSYIRNYDSTNPSARKFLWSMLKKNYYDKGIKNFWIDQADGGALGEAYENNGQSTYIESIPFTLPNVNYAAGTQLSVGKLYPWAHQQAIEEGFRNATDTKEGSAADHVSLSRSGYIGSQRFASMIWSGDTTSVWDTLAVQVASGLSAAATGWGWWTVDAGGFEVDSTVWWSGNIDTPEYRELYVRWLAWTTFLPFMRTHGSRTBYFQDAYTBANEPWSYGASNTPIIVSYIHLRYQLGAYLKSIFNQFHLTGRSIMRPLYMDFEKTDPKISQLVSSNSNYTTQQYMFGPRLLVSPVTLPNVTEWPVYLPQTGQNNTKPWTYWWTNETYAGGQVVKVPAPLQHIPVFHLGSREELLSGNVF
CCCCCCCCCEEEEECCEEEEEEECCCCEEEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCCCEEEEECCEEEEEECCCCCCCCCECEEEEECCCCCEEEEEEECCCCCCCCCEEEECCCCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCCCEEECCCECCEEEEEEEEECCCEEEEECCCCCEEEEECCCEEEEEEEEECCEEEEEEECCCCCHHHHHHHHHHHHCCCCCCCHHHHCEEECCCCCCCHHHHHHHHHHHHHCCCCCCEEEECCCCECCCCCCCECCCCCCCHHHHHHHHHHHHCCEEEEEECCEECCCCCCHHHHHHCCCECEECCCCCCCEEECCEEEEEECCCCHHHHHHHHHHHHHHCHHHCCCEEEECCCCCCCCCCCCCCCCCCHHHHHCCCCCCCEECCCCEHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCEEECCCCCCHHHCCEEEECCCCECCHHHHHHHHHHHHHHHHHCCCCEECCECCCECCCCCCCCCECCCHHHHHHHHHHHHHHCCCCCEEECCCCECCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHCHHHHHHHHHHHHHHCCCCEECCHHHHCCCCCCHHHHHHCCCHHHHCCEEECCCEEECCCCCCCCCEEEEEECCCCCCCCCCEEECCCCCEECCCEEEEEECCCCCCCEEECCCHHHHCCCCCC
CCEECCCCEEEEECCCCEEEEECCCCCCEEEEHHHCCCCCCCCHHCCCCCCCCCCCCCCCCCCCEEEECCCCEEEEECCCEEEEECCCCCCCCCCEEEEEECCCCCCHEEEHHHCCCCCCCCCHHECCCCCCCCEEEEEEECCCCCHCHECCCCCCCCCECCCCCEEEHHCCCCCEEEEEEECCCCCEEEEECCCCCEEEECCCCCEEEEHHHHHEEEEEECCCCCCHHHHHHHHHHHCCCCCCCCHHHCCCEEHHHHHCCHHHHHHHHHHHHHCCCCCEEEEEEHHCCCCCCCCCCCCCCCCCCHHHHHHHHHHCCCEEEEEECCCCCCCCHCHHHHHHCCCEEECCCCCEEECCCCCCCCEEECCCCCHHHHHHHHHHHHCHHHHCCEEEEECCCCCCCCCCCCCCCCCCCCHECCCCCCCCHEEECCCHHHHHCHHHHHHHHHHHHHHHCCCCCCCCCCCCEEEEEEHHCCCCCCCEEEEECCCCCCCHHHHHHHHHHCHHHHHCCCCCCCECCCCEECCCCCCCCCCCCCCCHHHHHHHHHHHHHHCEHEECCCCCCCCCCCCCCCCCCEECCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCEECCHHCCCCCCCCCHCHHCCCCCCECCCHHECCCHHEECCEECCCCCEEEEEECCCCCCCCCEEEEECCCCEECCCEEEEEECCCCCCEEEECCCCCCCCCCCCC
