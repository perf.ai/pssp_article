import sys
# Execute: python calc_Q3.py <pred_file>
import string 
lines = None
labels = ['H', 'E', 'C']
with open(sys.argv[1]) as file:
	lines = file.readlines()
if lines is None: exit(0)
countCor = [0, 0, 0]
countAll = [0, 0, 0]
for l in range(0, len(lines)//4):
	protein_name = lines[4*l]
	primary = lines[4*l + 1]
	secondary = lines[4*l + 2]
	prediction = lines[4*l + 3]
	for s, p in zip(secondary, prediction):
		if s == '\n': continue
		if s == p:
			countCor[labels.index(s)] += 1
		countAll[labels.index(s)] += 1
total = countAll[0] + countAll[1] + countAll[2]
correct = countCor[0] + countCor[1] + countCor[2]
headers = ['Q3_All', 'Q3_C', 'Q3_E', 'Q3_H']
q3 = [(100*correct/total), 
	  (100*countCor[0]/countAll[0]), 
	  (100*countCor[1]/countAll[1]), 
	  (100*countCor[2]/countAll[2])]
print("\n   {0:11}{1:11}{2:11}{3:11}".format(' Q3_ALL', ' Q3_H', ' Q3_E', ' Q3_C'))
print('{0:11.4f}{1:11.4f}{2:11.4f}{3:11.4f}\n'.format(q3[0], q3[1], q3[2], q3[3]))

