"""
	Author: Panayiotis Leontiou
	Since: Jul 4, 2020
	Version: 1.0
	Bugs: No known bugs
	Execute: python sort_3class.py
	About: This program replaces the 8-class secondary structure with the 3-class structure
		   and creates two output files, one with the same order and one sorted based on
		   the length of each primary structure. It also prints the names of the proteins
		   that include unknown characters along with those characters.
"""

import string
import io
from collections import OrderedDict

with open('corrected_datasets_CASP13.txt', 'r') as casp13_f:
	casp13_data = casp13_f.readlines()

def replace_all(text, dic):
	for i, j in dic.items():
		text = text.replace(i, j)
	return text

def find_offsets(haystack, needle):
	"""
	Find the start of all (possibly-overlapping) instances of needle in haystack
	"""
	offs = -1
	while True:
		offs = haystack.find(needle, offs+1)
		if offs == -1:
			break
		else:
			yield offs

def replace_str_index(text, index=0, replacement=''):
	return '%s%s%s'%(text[:index],replacement,text[index+1:])

mappings = OrderedDict([("G", "H"), ("I", "H"), ("B", "E"), ("T", "C"), 
						("S", "C"), (" ", "C"), ("!", "")]) 
# {"G": "H", "I": "H", "B": "E", "T": "C", "S": "C", " ": "C"}

arr = []
with open('casp13_3class.txt', 'w') as out_f:
	for i in range(0, len(casp13_data), 3):
		protein_name = casp13_data[i].rstrip('\n')
		primary_structure = casp13_data[i + 1].rstrip('\n')
		secondary_structure = casp13_data[i + 2].rstrip('\n')

		# temp = protein_name.split(',')
		# new_name = '> ' + temp[-1] + '_' + temp[1]
		new_name = protein_name
		print(new_name.split()[-1] + '.fasta.hssp')
		out_f.write(new_name + '\n')

		# for offs in find_offsets(primary_structure, '!'):
		# 	# print(secondary_structure[offs])
		# 	secondary_structure = replace_str_index(secondary_structure, offs, '!')

		# new_primary = primary_structure.replace('!', '').upper()
		# print(new_primary)
		new_primary = primary_structure
		out_f.write(new_primary + '\n')
		
		# new_secondary = replace_all(secondary_structure, mappings)
		# print(new_secondary)
		new_secondary = secondary_structure
		out_f.write(new_secondary + '\n')

		arr.append([new_name, new_primary, new_secondary])

# Sort proteins based on primary structure length
arr.sort(key = lambda s: len(s[1]))

with open('casp13_sorted.txt', 'w') as sorted_out_f:
	for p in arr:
		sorted_out_f.write(p[0] + '\n')
		sorted_out_f.write(p[1] + '\n')
		sorted_out_f.write(p[2] + '\n')

for p in arr:
	temp = replace_all(p[2], OrderedDict([("H", ""), ("E", ""), ("C", "")]))
	if len(temp) > 0:
		print(p[0] + '-->' + temp)
