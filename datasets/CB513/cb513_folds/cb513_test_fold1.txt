> 1CDLG_796-815
ARRKWQKTGHAVRAIGRLSS
CHHHHHHHHHHHHHHHHCCC
> 1BPHA_1-21
GIVEQCCASVCSLYQLENYCN
CHHHHHCCCCCCHHHHHCCEC
> 1COIA_1-29
EVEALEKKVAALESKVQALEKKVEALEHG
CHHHHHHHHHHHHHHHHHHHHHHHHHCCC
> 1DARA_433-481
EEDPTFRVSTQTIISGMGELKREFKVDANVGKPQV
CCCCCCEEECCEEEEECCCCCCCEEEECCCECCCE
> 1TIIC_195-230
TTCASLTNKLSQHDLADFKKYIKRKFTLMTLLSINN
CCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCC
> 1ILKA_116-160
NKSKAVEQVKNAFNKLQEKGIYKAMSEFDIFINYIEAYMTMKIRN
CCCHHHHHHHHHHHHHCHHHHHHHHHCHHHHHHHHHHHHHHHHCC
> 1PTRA_231-280
HRFKVYNYMSPTFCDHCGSLLWGLVKQGLKCEDCGMNVHHKCREKVANLC
CCEEEECCCCCCECCCCCCECCCCCCCEEEECCCCCEECHHHHCCCCCCC
> 1CEOA_202-254
PFFFTHQKAHWSESAMAYNRTVKYPGQYEGIEEFVKNNPKYSFMMELNNLKLN
CHHHHCCCCCCCHHHHHHCCCCCCCEECCCHHHHHHHCHHHHHHHHHCCCEEC
> 1YRNB_128-190
TKPYRGHRFTKENVRILESWFAKNIENPYLDTKGLENLMKNTSLSRIQIKNWVSNRRRKEKTI
CCCCCCCCCCHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHC
> 1AHBA_180-246
VPSLATISLENSWSGLSKQIQLAQGNNGIFRTPIVLVDNKGNRVQITNVTSKVVTSNIQLLLNTRNI
CCCHHHHHHHHHHHHHHHHHHHHCCCCCEEEEEEEECCCCCCCEEEEECCCHHHHHCECCECCHHHC
> 1QBBA_819-885
LPVPGARVAGGKLEANIALPGLGIEYSTDGGKQWQRYDAKAKPAVSGEVQVRSVSPDGKRYSRAEKV
CCCCEEEEECCEEEEECCCCCCEEEEECCCCCCCEECCCCCCCECCCCCEEEEECCCCCCECCCEEC
> 1SPBP_7-77
EKKYIVGFKQTMSTMSAAKKKDVISEKGGKVQKQFKYVDAASATLNEKAVKELKKDPSVAYVEEDHVAHAY
CCEEEEEECCCCCCCCHHHHHHHHHHCCCEEEEECCCCCEEEEECCHHHHHHHHHCCCEEEEEECCEEEEC
> 1CTNA_444-516
YGRGWTGVNGYQNNIPFTGTATGPVKGTWENGIVDYRQIAGQFMSGEWQYTYDATAEAPYVFKPSTGDLITFD
EEEEEECCECCECCEHHHCECCEECCCCCCCCEEEHHHHHHHCCCCCCEEEEECCCCEEEEEECCCCEEEECC
> 3ICBA_1-75
KSPEELKGIFEKYAAKEGDPNQLSKEELKLLLQTEFPSLLKGPSTLDELFEELDKNGDGEVSFEEFQVLVKKISQ
CCHHHHHHHHHHHHCCCCCCCCECHHHHHHHHHHHCCCCCCCCCCHHHHHHHHCCCCCCCECHHHHHHHHHHHHC
> 2CTHB_29-107
KCGDCHHPVNGKEDYRKCGTAGCHDSMDKKDKSAKGYYHVMHDKNTKFKSCVGCHVEVAGADAAKKKDLTGCKKSKCHE
CHHHCCCECCCCECCCCCCCCCCCCECCCCCCCCCEHHHHHHCCCCCCCCHHHHHHHHHCCCHHHHHHHHCCCCCCCCC
> 1SCUE_21-101
VGYACTTPREAEEAASKIGAGPWVVKCQVHAGGRGKAGGVKVVNSKEDIRAFAENWLGKRLVTYQTDANGQPVNQILVEAA
CEEEECCHHHHHHHHHHHCCCCEEEEECCCCCCCCCCCCEEEECCHHHHHHHHHHHCCCEECCCCCCCCCEECCCEEEEEC
> 1TCRA_117-213
YIQNPEPAVYALKDPRSQDSTLCLFTDFDSQINVPKTMESGTFITDATVLDMKAMDSKSNGAIAWSNQTSFTCQDIFKETNATYPSSDVPC
CCCCCCCEEEEEECCCCCCCEEEEEECCCCCCCCCCCCCCCEEECCCEEEECCCCCCEEEEEEEEECCCCCCHHHHCCCCCCCCCCCCCCC
> 1EFTA_311-405
TPHTKFEASVYVLKKEEGGRHTGFFSGYRPQFYFRTTDVTGVVRLPQGVEMVMPGDNVTFTVELIKPVALEEGLRFAIREGGRTVGAGVVTKILE
CCEEEEEEEEEECCHHHCCCCCCECCCCCCEEEECCEEEEEEEECCCCCCCECCCCEEEEEEEEEEEECCCCCCEEEEEECCEEEEEEEEEEECC
> 1IRKA_981-1079
SSVFVPDEWEVSREKITLLRELGQGSFGMVYEGNARDIIKGEAETRVAVKTVNESASLRERIEFLNEASVMKGFTCHHVVRLLGVVSKGQPTLVVMELM
CCCCCCHHHECCHHHEEEEEEEEECCCCEEEEEEEEEEECCEEEEEEEEECCCCCCCHHHHHHHHHHHHHHCCCCCCCECCEEEEECCCCCCEEEEECC
> 2CHBE_1-103
TPQNITDLCAEYHNTQIHTLNDKIFSYTESLAGKREMAIITFKNGATFQVEVPGSQHIDSQKKAIERMKDTLRIAYLTEAKVEKLCVWNNKTPHAIAAISMAN
CCCCHHHHHCCCCCEEEEEEEEECCEEEEECCCCCCCEEEECCCCCEEEECCCCCCCCHHHHHHHHHHHHHHHHHHHHCCEEEEEEEECCCCCEEEEEEEEEC
> 1KTEA_1-105
AQAFVNSKIQPGKVVVFIKPTCPFCRKTQELLSQLPFKEGLLEFVDITATSDTNEIQDYLQQLTGARTVPRVFIGKECIGGCTDLESMHKRGELLTRLQQVGAVK
CHHHHHHHCCCCCEEEEECCCCHHHHHHHHHHHHCCECCCCEEEEEHHHCCCHHHHHHHHHHHHCCCCCCEEEECCEEEECHHHHHHHHHHCHHHHHHHHHCCEC
> 1UMUB_32-137
DYVEQRIDLNQLLIQHPSATYFVKASGDSXIDGGISDGDLLIVDSAITASHGDIVIAAVDGEFTVKKLQLRPTVQLIPXNSAYSPITISSEDTLDVFGVVIHVVKA
CCCCCCCCHHHHHCCCHHHEEEEECCCCCCHHHCCCCCCEEEEECCCCCCCCCEEEEEECCEEEEEEEECCCCCEEECCCCCCCCEECCCCCCEEEEEEEEEEECC
> 2TGIA_1-112
ALDAAYCFRNVQDNCCLRPLYIDFKRDLGWKWIHEPKGYNANFCAGACPYLWSSDTQHSRVLSLYNTINPEASASPCCVSQDLEPLTILYYIGKTPKIEQLSNMIVKSCKCS
CCCHHHHCCCCCCECEEECCEEEHHHHHCCCCEEECCEEECCEEECECCCCCCECCHHHHHHHHHHHHCHHHCCCCCEEECCEEEEEEEEEECCEEEEEEEEEEEECCEEEC
> 3ECAB_213-326
KVGIVYNYANASDLPAKALVDAGYDGIVSAGVGNGNLYKSVFDTLATAAKTGTAVVRSSRVPTGATTQDAEVDDAKYGFVASGTLNPQKARVLLQLALTQTKDPQQIQQIFNQY
CEEEEECCCCCCCHHHHHHHHCCCCEEEEEEECCCECCHHHHHHHHHHHHCCCEEEEEECCCECCECCCCCCCHHHHCCEECECCCHHHHHHHHHHHCCCCCCHHHHHHHHHHC
> 1REGY_1-120
MIEITLKKPEDFLKVKETLTRMGIANNKDKVLYQSCHILQKKGLYYIVHFKEMLRMDGRQVEMTEEDEVRRDSIAWLLEDWGLIEIVPGQRTFMKDLTNNFRVISFKQKHEWKLVPKYTI
CEEEECCCCCHHHHHHHHHCCEEEEECCCCEEEECEEEEEECCEEEEEEHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHCCCCEECCCCCEECCCCCCCCEECCCCCHHHCEEEECCCC
> 1FIND_311-432
VNQFLTQYFLHQQPANCKVESLAMFLGELSLIDADPYLKYLPSVIAGAAFHLALYTVTGQSWPESLIRKTGYTLESLKPCLMDLHQTYLKAPQHAQQSIREKYKNSKYHGVSLLNPPETLNL
HHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHCHHHHCCCCHHHHHHHHHHHHHHHHCCCCCCHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHCCCCHHHHHCCCHHHCCHHHCCCCCCCCC
> 1NDHA_3-125
PAITLENPDIKYPLRLIDKEVVNHDTRRFRFALPSPEHILGLPVGQHIYLSARIDGNLVIRPYTPVSSDDDKGFVDLVIKVYFKDTHPKFPAGGKMSQYLESMKIGDTIEFRGPNGLLVYQGK
CCECCCCCCCEEEEEEEEEEECCCCEEEEEECCCCCCCECCCCCCCEEEEEEEECCEEEEEEECCCCCCCCCCCCCEEEEECCCCCCCCCCCCCHHHHHHHHCCCCCEEEEEEEECCEEEEEC
> 2PHYA_1-125
MEHVAFGSEDIENTLAKMDDGQLDGLAFGAIQLDGDGNILQYNAAEGDITGRDPKQVIGKNFFKDVAPCTDSPEFYGKFKEGVASGNLNTMFEYTFDYQMTPTKVKVHMKKALSGDSYWVFVKRV
CCCCCCCCCCHHHHHCCCCHHHHCCCCCEEEEEECCCEEEEECHHHHHHHCCCHHHHCCCEHHHHHCHHHCCCCCHHHHHHHHHHCCCEEEEEEEECCCCCCEEEEEEEEECCCCCEEEEEEEEC
> 1AOZB_1-130
SQIRHYKWEVEYMFWAPNCNENIVMGINGQFPGPTIRANAGDSVVVELTNKLHTEGVVIHWHGILQRGTPWADGTASISQCAINPGETFFYNFTVDNPGTFFYHGHLGMQRSAGLYGSLIVDPPQGKKEP
CCEEEEEEEEEEEEECCCCCCEEEEEECCECCCCCEEEECCCEEEEEEEECCCCCCECEEEECCCCCCCHHHCCCECCCECCECCCCEEEEEEECCCCEEEEEEECCCCHHHHCCEEEEEEECCCCCCCC
> 1LISA_4-134
HYVEPKFLNKAFEVALKVQIIAGFDRGLVKWLRVHGRTLSTVQKKALYFVNRRYMQTHWANYMLWINKKIDALGRTPVVGDYTRLGAEIGRRIDMAYFYDFLKDKNMIPKYLPYMEEINRMRPADVPVKYM
CCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHCCCHHHHHHHHHCCCCCCCCHHHHHHHHCCHHHCCCCCC
> 3PMGB_420-561
GRNFFTRYDYEEVEAEGATKMMKDLEALMFDRSFVGKQFSANDKVYTVEKADNFEYHDPVDGSVSKNQGLRLIFADGSRIIFRLSGTGSAGATIRLYIDSYEKDNAKINQDPQVMLAPLISIALKVSQLQERTGRTAPTVIT
CEEEEEEEEEEEEEHHHHHHHHHHHHHHHCCCCCCCCEEECCCCEEEEEEEEECCEECCCCCCEECCCCEEEEECCCCEEEEEEEECCCCEEEEEEEEEEEECCCCCCCCCHHHHHHHHHHHHHHHHCHHHHHCCCCCCEEC
> 1NLKL_2-144
AIERTLSIIKPDGLEKGVIGKIISRFEEKGLKPVAIRLQHLSQAQAEGFYAVHKARPFFKDLVQFMISGPVVLMVLEGENAVLANRDIMGATNPAQAAEGTIRKDFATSIDKNTVHGSDSLENAKIEIAYFFRETEIHSYPYQ
CEEEEEEEECHHHHHCCCHHHHHHHHHHCCCEEEEEEEECCCHHHHHHHCHHHCCCCCHHHHHHHHCCCCEEEEEEEEECHHHHHHHHHCCCCHHHCCCCCHHHHHCCECCECCEEECCCHHHHHHHHHHHCCHHHCCCCCCC
> 2AFNC_9-157
IAALPRQKVELVDPPFVHAHSQVAEGGPKVVEFTMVIEEKKIVIDDAGTEVHAMAFNGTVPGPLMVVHQDDYLELTLINPETNTLMHNIDFHAATGALGGGGLTEINPGEKTILRFKATKPGVFVYHCAPPGMVPWHVVSGMNGAIMVL
CCCCCEEECCCCCCCCCCCCCCECCCCCCEEEEEEEEEEEEEECCCCCCEEEEEEECCECCCCCEEEECCCEEEEEEEECCCCCCCECCEECCCCCHHHHCCCCCECCCEEEEEEEECCCCEEEEEECCCCCCHHHHHCCCCEEEEEEE
> 1KTQA_293-445
ALEEAPWPPPEGAFVGFVLSRKEPMWADLLALAAARGGRVHRAPEPYKALRDLKEARGLLAKDLSVLALREGLGLPPGDDPMLLAYLLDPSNTTPEGVARRYGGEWTEEAGERAALSERLFANLWGRLEGEERLLWLYREVERPLSAVLAHME
CCCEECCCCCCCCEEEEEECCCCHHHCCEEEEEEEECCEEEECCCHHHHHCCCCCECCCCHHHHHHHHHHCCCCCCECCCHHHHHHHHCCCCCCHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHHHCCHHHHHHHHHCHHHHHHHHHHHH
> 1L58A_1-164
MNIFEMLRIDEGLRLKIYKDTEGYYTIGIGHLLTKSPSLNAAKSELDKAIGRNCNGVITKDEAEKLFNQDVDAAVRGILRNAKLKPVYDSLDAVRRCALINMVFQMGETGVAGFTNSLRMLQQKRWDEAAVNLAKSRWYNQTANRAKRVITTFRTGTWDAYKNL
CCHHHHHHHHHCCEEEEEECCCCCEEEECCEEEECCCCHHHHHHHHHHHHCCCCCCECCHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHCCHHHHHHHHHHHHHHHHHHHHCCHHHHHHHHCCCHHHHHHHHHCCHHHHHCHHHHHHHHHHHHHCCCHHHHCC
> 821PA_1-166
MTEYKLVVVGAPGVGKSALTIQLIQNHFVDEYDPTIEDSYRKQVVIDGETCLLDILDTAGQEEYSAMRDQYMRTGEGFLCVFAINNTKSFEDIHQYREQIKRVKDSDDVPMVLVGNKCDLAARTVESRQAQDLARSYGIPYIETSAKTRQGVEDAFYTLVREIRQH
CEEEEEEEEECCCCCHHHHHHHHHHCCCCCCCCCCCEEEEEEEEEECCEEEEEEEEEECCCCCCHHHHHHHHHHCCEEEEEEECCCHHHHHHHHHHHHHHHHHHCCCCCCEEEEEECCCCCCCCCCHHHHHHHHHHHCCCEEEECCCCCCCHHHHHHHHHHHHHCC
> 2PGDA_1-181
AQADIALIGLAVMGQNLILNMNDHGFVVCAFNRTVSKVDDFLANEAKGTKVLGAHSLEEMVSKLKKPRRIILLVKAGQAVDNFIEKLVPLLDIGDIIIDGGNSEYRDTMRRCRDLKDKGILFVGSGVSGGEDGARYGPSLMPGGNKEAWPHIKAIFQGIAAKVGTGEPCCDWVGDDGAGHF
CCECEEEECCCHHHHHHHHHHHHCCCCEEEECCCCHHHHHHHHCCCCCCCCEECCCHHHHHHHECCCCEEEECCCCCHHHHHHHHHHHHHCCCCCEEEECCCCCHHHHHHHHHHHHHCCCEEEEEEEECHHHHHHHCCEEEEEECCCCHHHHHHHHHHHCCECCCCCECCCCCEECCHHHH
> 3PMGB_1-188
VKIVTVKTKAYPDQKPGTSGLRKRVKVFQSSTNYAENFIQSIISTVEPAQRQEATLVVGGDGRFYMKEAIQLIVRIAAANGIGRLVIGQNGILSTPAVSCIIRKIKAIGGIILTAXHNPGGPNGDFGIKFNISNGGPAPEAITDKIFQISKTIEEYAICPDLKVDLGVLGKQQFDLENKFKPFTVEIV
CCCEEEECCCCCCCCCECCEEEEEHHHHHHCCCHHHHHHHHHHHCCCHHHCCCCEEEEEECCCCCHHHHHHHHHHHHHHCCCCEEEEEEEEECCHHHHHHHHHHHCCCEEEEECCCCCCCCCCCEEEEEEEECCCCECCHHHHHHHHHHHHHCCEEEECCCCCCCCCCCEEEEECCCCCCCCEEEEEE
> 1PNMB_260-450
GADRVTEIDRLLEQKPRLTADQAWDVIRQTSRQDLNLRLFLPTLQAATSGLTQSDPRRQLVETLTRWDGINLLNDDGKTWQQPGSAILNVWLTSMLKRTVVAAVPMPFDKWYSASGYETTQDGPTGSLNISVGAKILYEAVQGDKSPIPQAVDLFAGKPQQEVVLAALEDTWETLSKRYGNNVSNWKTPAM
CCCCCHHHHHHHHHCCCECHHHHHHHHHHHHHECCCHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHCCCECCECCCCCEECCCHHHHHHHHHHHHHHHCHHHHCCCCHHHHHCCCCCCCCCCCCCCCCCCCHHHHHHHHHCCHHHCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHCCCHHHCCEECC
> 1GPMD_3-208
ENIHKHRILILDFGSQYTQLVARRVRELGVYCELWAWDVTEAQIRDFNPSGIILSGGPESTTEENSPRAPQYVFEAGVPVFGVCYGMQTMAMQLGGHVEASNEREFGYAQVEVVNDSALVRGIEDALTADGKPLLDVWMSHGDKVTAIPSDFITVASTESCPFAIMANEEKRFYGVQFHPEVTHTRQGMRMLERFVRDICQCEALW
CCCCCCEEEEEECCCCCHHHHHHHHHHCCCEEEEEECCCCHHHHHHHCCCEEEECCCCCCCCCCCCCCCCHHHHHCCCCEEEECHHHHHHHHHHCCCEECCCCCEEEEEEEEECCCCCCCCCCCCEECCCCCEEEEEEEEECCEECCCCCCCEEEEEECCEEEEEEEECCCCEEEECECCCCCCCCCHHHHHHHHHHCCCCCCCCC
> 2REBA_27-269
MRLGEDRSMDVETISTGSLSLDIALGAGGLPMGRIVEIYGPESSGKTTLTLQVIAAAQREGKTCAFIDAEHALDPIYARKLGVDIDNLLCSQPDTGEQALEICDALARSGAVDVIVVDSVAALTPKAEIEGLAARMMSQAMRKLAGNLKQSNTLLIFINQTGGNALKFYASVRLDIRRIGAVKEGENVVGSETRVKVVKNKIAAPFKQAEFQILYGEGIN
CCCCCCCCCCCCEECCCCHHHHHHCCCCCEECCCEEEEECCCCCCHHHHHHHHHHHHHHCCCCEEEEECCCCCCHHHHHHCCCCHHHCEEECCCCHHHHHHHHHHHHHHCCCCEEEEECHHHCCCHHHHCCHHHHHHHHHHHHHHHHHHHHCCEEEEEECCCCHHHHHHCCEEEEEEEEEEEEECCEEEEEEEEEEEEEECCCCCCCEEEEEEECCCEEC
> 2CABA_5-260
WGYDDKNGPEQWSKLYPIANGNNQSPVDIKTSETKHDTSLKPISVSYNPATAKEIINVGHSFHVNFEDNQDRSVLKGGPFSDSYRLFQFHFHWGSTNEHGSEHTVDGVKYSAELHVAHWNSAKYSSLAEAASKADGLAVIGVLMKVGEANPKLQKVLDALQAIKTKGKRAPFTNFDPSTLLPSSLDFWTYPGSLTHPPLYESVTWIICKESISVSSEQLAQFRSLLSNVEGDNAVPMQHNNRPTQPLKGRTVRASF
CCCCCCCCHHHHHHCCHHHHCCCCCCCEECCCCCEECCCCEEEEEECCHHHEEEEEECCCCEEEEECCCCCCCEEEEECCCCCEEEEEEEEEECCCCCCCCCCEECCEECCEEEEEEEEECCCCCCHHHHCCCCCCEEEEEEEEEECCCCHHHHHHHHHHHHCCCCCCEEECCCCCHHHHCCCCCCEEEEEECCCCCCCCCCEEEEEECCCEEECHHHHHHHCCCECCCCCCCCCECCCCCCCCCCCCCCCCEECC
> 4RHV1_17-289
TVASISSGPKHTQKVPILTANETGATMPVLPSDSIETRTTYMHFNGSETDVECFLGRAACVHVTEIQNKDATGIDNHREAKLFNDWKINLSSLVQLRKKLELFTYVRFDSEYTILATASQPDSANYSSNLVVQAMYVPPGAPNPKEWDDYTWQSASNPSVFFKVGDTSRFSVPYVGLASAYNCFYDGYSHDDAETQYGITVLNHMGSMAFRIVNEHDEHKTLVKIRVYHRAKHVEAWIPRAPRALPYTSIGRTNYPKNTEPVIKKRKGDIKSY
CEEECCECCCCCCCCCCEECHHHCCCCCCCHHHCCCECCEECCCCCHHHEHHHHHCCCEEEEEEEEEECCCCCCCCCHHHCCEEEEECCCCCCHHHHHHHCCEEEEEEEEEEEEEEEEECCCCCCCCCCCEEEEEECCCCCCCCCCCCCHHHHCCCCCEEEEECCCEEEEEECCCCCCCCEECCCCECCCCCCCCCCCCCHHHCCCEEEEEECCCCCCCCEEEEEEEEEEEEEEEEEEECCCECCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
> 1NARA_1-289
PKPIFREYIGVKPNSTTLHDFPTEIINTETLEFHYILGFAIESYYESGKGTGTFEESWDVELFGPEKVKNLKRRHPEVKVVISIGGRGVNTPFDPAEENVWVSNAKESLKLIIQKYSDDSGNLIDGIDIHYEHIRSDEPFATLMGQLITELKKDDDLNINVVSIAPSENNSSHYQKLYNAKKDYINWVDYQFSNQQKPVSTDDAFVEIFKSLEKDYHPHKVLPGFSTDPLDTKHNKITRDIFIGGCTRLVQTFSLPGVFFWNANDSVIPKRDGDKPFIVELTLQQLLAA
CCCEEEEEECCCCCCCCCCCCCCCCCCCCCCEEEEEEEEEEEEECCCCCEEEEEEECCCHHHHCHHHHHHHHHHCCCCEEEEEEEECCCCCCECECCHHHHHHHHHHHHHHHHHHCEECCEECCCEEEEEECCECCCCCHHHHHHHHHHHHHHCCCCCCCEEEECCCCCCHHHHHHHHHHHCCCCCEEEEEHHHCCCCCCCHHHHHHHHHHHHHHCCCCCEEEEEECCHHHHHHCCCCHHHHHHHHHHHHHCCCCCEEEEECHHHHCCCCCCCCCCCHHHHHHHHHHHC
> 1MASB_2-315
AKKIILDCDPGLDDAVAILLAHGNPEIELLAITTVVGNQTLAKVTRNAQLVADIAGITGVPIAAGCDKPLVRKIMTKNKVDERHAVNLIIDLVMSHEPKTITLVPTGGLTNIAMAARLEPRIVDRVKEVVLMGGGYHEGNATSVAEFNIIIDPEAAHIVFNESWQVTMVGLDLTHQALATPPILQRVKEVDTNPARFMLEIMDYYTKIYQSNRYMAAAAVHDPCAVAYVIDPSVMTTERVPVDIELTGKLTLGMTVADFRNPRPEHCHTQVAVKLDFEKFWGLVLDALERIGDPQ
CCEEEEEECCCHHHHHHHHHHHCCCCCEEEEEEECCCCCCHHHHHHHHHHHHHHHCCCCCCEEECCCCCCCCCCCCCCCCCCCCHHHHHHHHHHCCCCCCEEEEECCCCHHHHHHHHHCCHHHHHCCEEEECCCCCCCCCCCCCCCHHHHHCHHHHHHHHHCCCCEEECCHHHHCCCEECHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHHCCCCCEECCHHHHHHHHHCHHHEEEECCCEEECCCCCCCCCCEEECCCCCCCCCCCEEEEEEECHHHHHHHHHHHHHHHCCCC
> 1GP2B_43-340
IQMRTRRTLRGHLAKIYAMHWGTDSRLLVSASQDGKLIIWDSYTTNKVHAIPLRSSWVMTCAYAPSGNYVACGGLDNICSIYNLKTREGNVRVSRELAGHTGYLSCCRFLDDNQIVTSSGDTTCALWDIETGQQTTTFTGHTGDVMSLSLAPDTRLFVSGACDASAKLWDVREGMCRQTFTGHESDINAICFFPNGNAFATGSDDATCRLFDLRADQELMTYSHDNIICGITSVSFSKSGRLLLAGYDDFNCNVWDALKADRAGVLAGHDNRVSCLGVTDDGMAVATGSWDSFLKIWN
CCCCEEEEECCCCCCEEEEEECCCCCEEEEEECCCEEEEEECCCCCEEEEEECCCCCEEEEEECCCCCEEEEEECCCEEEEEECCCCCCCCCCCEEEECCCCCEEEEEEEECCEEEEEECCCCEEEEEHHHCEEEEEECCCCCCEEEEEECCCCCEEEEEECCCCEEEEECCCCEEEEEECCCCCCEEEEEECCCCCEEEEEECCCCEEEEECCCCEEEEEECCCCCCCCEEEEEECCCCCEEEEEECCCEEEEEECCCCCEEEEEECCCCCEEEEEECCCCCCEEEEECCCCEEEEC
> 1KIMB_46-374
MPTLLRVYIDGPHGMGKTTTTQLLVALDIVYVPEPMTYWRVLGASETIANIYTTQHRLDQGEISAGDAAVVMTSAQITMGMPYAVTDAVLAPHIGGEAPPALTLIFDRHPIAALLCYPAARYLMGSMTPQAVLAFVALIPPTLPGTNIVLGALPEDRHIDRLAKRQRPGERLDLAMLAAIRRVYGLLANTVRYLQCGGSWREDWGQLAGPRPHIGDTLFTLFRAPELLAPNGDLYNVFAWALDVLAKRLRSMHVFILDYDQSPAGCRDALLQLTSGMVQTHVTTPGSIPTICDLARTFAREMGE
CCEEEEEEEECCCCCCHHHHHHHHHCCCEEEECCCHHHHHCCCCCCHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHEEEECCCCEEEEEECCHHHHHCHHHHHHHHCCCCCHHHHHHHHHCCCCCCCCCEEEEEECCHHHHHHHHHHCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHCCCCCCCHHHCHHHHHCCHHHECCCCCECHHHHHHHHHHHHHHCCCEEEEEECCCCHHHHHHHHHHHHHHCCEEEECCCCHHHHHHHHHHHHHHHHCC
> 1ONRB_2-317
TDKLTSLRQYTTVVADTGDIAAMKLYQPQDATTNPSLILNAAQIPEYRKLIDDAVAWAKQQSNDRAQQIVDATDKLAVNIGLEILKLVPGRISTEVDARLSYDTEASIAKAKRLIKLYNDAGISNDRILIKLASTWQGIRAAEQLEKEGINCNLTLLFSFAQARACAEAGVFLISPFVGRILDWYKANTDKKEYAPAEDPGVVSVSEIYQYYKEHGYETVVMGASFRNIGEILELAGCDRLTIAPALLKELAESEGAIERKLSYTGEVKARPARITESEFLWQHNQDPMAVDKLAEGIRKFAIDQEKLEKMIGDLL
CEHHHHHCCCCEEEEECCCHHHHHHHCCCEEECCHHHHHHHCCCHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCEEEECCHHHCCCHHHHHHHHHHHHHHHHHCCCCHHHEEEEEECCHHHHHHHHHHHHCCCCEEEEEECCHHHHHHHHHCCCCEEEEECHHHHHHHHHCCCCCCCCHHHCHHHHHHHHHHHHHHHCCCCCEEEEECCCCHHHHHHCCCCCEEEECHHHHHHHHHCECCCCCCCCCCCCCCCCCCCCCHHHHHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHHHHCCC
> 1BMV2_3001-2192
METNLFKLSLDDVETPKGSMLDLKISQSKIALPKNTVGGTILRSDLLANFLTEGNFRASVDLQRTHRIKGMIKMVATVGIPENTGIALACAMNSSIRGRASSDIYTICSQDCELWNPACTKAMTMSFNPNPCSDAWSLEFLKRTGFHCDIICVTGWTATPMQDVQVTIDWFISSQECVPRTYCVLNPQNPFVLNRWMGKLTFPQGTSRSVKRMPLSIGGGAGAKSAILMNMPNAVLSMWRYFVGDLVFEVSKMTSPYIKCTVSFFIAFGNLADDTINFEAFPHKLVQFGEIQEKVVLKFSQEEFLTAWSTQVRPATTLLADGCPYLYAMVHDSSVSTIPGDFVIGVKLTIIENMCAYGLNPGISGSRLLGTIPQ
CCCCCCCCCCCCCCCCCCCCCCCEEEEEEEEECCCCCCCEEEEEEEHHHHHCCCCCCCHHHHHCCCCECCCEEEEEECCCCCCCCCEEEEEEECEECCCCCCCCCCCCCCEEEEECCCCCCCEEEEECCCCCCCCECHHHHHCCCCEEEEEEEECCCCCCCCCEEEEEEEEECCCCCCCCCEECCCCCCCEEEEEEEEEEEECCECCCCCEEEECCCCCCEEECCEEECCHHHHCHHHCCEEEEEEEEEEEECCCCCCECCEEEEEECCCCCCCCHHHHHCCEEEECCCCCCCEEEEEECHHHCCCCEECCCCCCCCCCCCCCCEEEEEECCCEECCCCCCEEEEEEEEEEEEEECCCCCCCCCCCCCCCCCCC
> 1QBBA_336-818
PRFPYRGIFLDVARNFHKKDAVLRLLDQMAAYKLNKFHFHLSDDEGWRIEIPGLPELTEVGGQRCHDLSETTCLLPQYGQGPDVYGGFFSRQDYIDIIKYAQARQIEVIPEIDMPAHARAAVVSMEARYKKLHAAGKEQEANEFRLVDPTDTSNTTSVQFFNRQSYLNPCLDSSQRFVDKVIGEIAQMHKEAGQPIKTWHFGGDEAKNIRLGAGYTDKAKPEPGKGIIDQGNEDKPWAKSQVCQTMIKEGKVADMEHLPSYFGQEVSKLVKAHGIDRMQAWQDGLKDAESSKAFATSRVGVNFWDTLYWGGFDSVNDWANKGYEVVVSNPDYVYMDFPYEVNPDERGYYWGTRFSDERKVFSFAPDNMPQNAETSVDRDGNHFNAKSDKPWPGAYGLSAQLWSETQRTDPQMEYMIFPRALSVAERSWHRAGWEQDYRAGREYKGGETHFVDTQALEKDWLRFANILGQRELAKLDKGGVAYR
CCCCEEEEEEECCCCCCCHHHHHHHHHHHHHCCCCEEEEEEEECCEECECCCCCCHHHHCCCEECCCCCCCCCECCCCCCCCCCEECCECHHHHHHHHHHHHHCCCEEEEEEEECCCCHHHHHHHHHHHHHHHHCCCHHHHHCCCCCCCCCCCCCCCCCCCCHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCEEEECCCCCCHHHCCCEEECCCCCCCCEEECCCCCCCCCCCCHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEHHHHCCCCCHHHCCCCEEEEEECCCCCCCHHHHHHHHHHCCCEEEECCHHHHECCCCCCCCCCCCCCCCCCCCCCHHHHHHCCCCCHHHHHHCCCCCCCCCCEEECCCCCCCCCEEEEEECCCCCCCHHHHHHHHCCHHHHHHHHHHCCCCCCCCCCCCCEEECCCCCCCCHHHHHHHHHHHHHHHHHCHHHHHHHCCCCCC
> 1CBGA_1-490
FKPLPISFDDFSDLNRSCFAPGFVFGTASSAFQYEGAAFEDGKGPSIWDTFTHKYPEKIKDRTNGDVAIDEYHRYKEDIGIMKDMNLDAYRFSISWPRVLPKGKLSGGVNREGINYYNNLINEVLANGMQPYVTLFHWDVPQALEDEYRGFLGRNIVDDFRDYAELCFKEFGDRVKHWITLNEPWGVSMNAYAYGTFAPGRCSDWLKLNCTGGDSGREPYLAAHYQLLAHAAAARLYKTKYQASQNGIIGITLVSHWFEPASKEKADVDAAKRGLDFMLGWFMHPLTKGRYPESMRYLVRKRLPKFSTEESKELTGSFDFLGLNYYSSYYAAKAPRIPNARPAIQTDSLINATFEHNGKPLGPMAASSWLCIYPQGIRKLLLYVKNHYNNPVIYITENGRNEFNDPTLSLQESLLDTPRIDYYYRHLYYVLTAIGDGVNVKGYFAWSLFDNMEWDSGYTVRFGLVFVDFKNNLKRHPKLSAHWFKSFLKK
CCCCCCCCCCHHHCCHHHCCCCCEEEEECCHHHHCCCCCCCCCCCEHHHHHHHHCHHHCCCCCCCCCCCCHHHHHHHHHHHHHHCCCCEEEEECCHHHHCCCCCHHHCCCHHHHHHHHHHHHHHHHCCCEEEEEEECCCCEHHHHHHHCHHHCCCHHHHHHHHHHHHHHHHCCCCCEEEEEECHHHHHHHHHCCCCCCCCCCCHHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHCHHHHCCEEEEEEECCEEEECCCCHHHHHHHHHHHHHHCHHHHHHHHHCCCCHHHHHHHHHHCCCCCHHHHHHHCCCCCEEEEECCCEEEEEECCCCCCCCCCHHHHCCEEEECEECCEECCCECCCCCCECCCHHHHHHHHHHHHHCCCCCEEEEECCCCEECCCCCCHHHHHCCHHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCECCCCHHHCCCEECCCEEEECCCCCEEEECHHHHHHHHHCCC
> 2TSCA_1-264
MKQYLELMQKVLDEGTQKNDRTGTGTLSIFGHQMRFNLQDGFPLVTTKRCHLRSIIHELLWFLQGDTNIAYLHENNVTIWDEWADENGDLGPVYGKQWRAWPTPDGRHIDQITTVLNQLKNDPDSRRIIVSAWNVGELDKMALAPCHAFFQFYVADGKLSCQLYQRSCDVFLGLPFNIASYALLVHMMAQQCDLEVGDFVWTGGDTHLYSNHMDQTHLQLSREPRPLPKLIIKRAPESIFDYRFEDFEIEGYDPHPGIKAPVAIMKQYLELMQKVLDEGTQKNDRTGTGTLSIFGHQMRFNLQDGFPLVTTKRCHLRSIIHELLWFLQGDTNIAYLHENNVTIWDEWADENGDLGPVYGKQWRAWPTPDGRHIDQITTVLNQLKNDPDSRRIIVSAWNVGELDKMALAPCHAFFQFYVADGKLSCQLYQRSCDVFLGLPFNIASYALLVHMMAQQCDLEVGDFVWTGGDTHLYSNHMDQTHLQLSREPRPLPKLIIKRAPESIFDYRFEDFEIEGYDPHPGIKAPVAI
CHHHHHHHHHHHHHCEEECCCCCCCEEEEEEEEEEEECCCCCCCCCCCCCCHHHHHHHHHHHHCCCCECHHHHHCCCCCCHHHCCCCCECCCCHHHHHHCEECCCCCEECHHHHHHHHHHHCCCCCCCEEECCCHHHHHHCCCCCCEEEEEEEECCCEEEEEEEEEEEECCCCHHHHHHHHHHHHHHHHHHHCCEEEEEEEEEEEEEEEHHHHHHHHHHHCCCCCCCCEEEECCCCCCHHHCCHHHEEEECCCCCCCCCCCCCCCHHHHHHHHHHHHHCEEECCCCCCCEEEEEEEEEEEEHHHCCCCCCCCCCCHHHHHHHHHHHHHCCCECHHHHCCCCCCCHHHCCCCCECCCCHHHHHHCEECCCCCEECHHHHHHHHHHHCCCCCCCEEECCCHHHCCCCCCCCCEEEEEEEECCCEEEEEEEEEEEECCCCHHHHHHHHHHHHHHHHHHHCCEEEEEEEEEEEEEEEHHHHHHHHHHHCCCCCCCCEEEECCCCCCHHHCCHHHEEEECCCCCCCCCCCCCC
