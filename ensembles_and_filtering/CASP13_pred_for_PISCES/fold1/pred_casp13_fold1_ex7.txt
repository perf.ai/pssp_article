> 6gnx_2
GYISIDAXKKFLGELHDFIPGTSGYLAYHVQN
CCEEHHHCCCCCCCEEECCCCCCCEEEEECCC
CCCCHHHHHHECEHEEEECCCCCCEEEEEECC
> 6fxa_1
ENIEETITVMKKLEEPRQKVVLDTAKIQLKEQDEQ
CHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHCC
CCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCC
> 5w9f_1
SQETRKKATEMKKKFKNBEVRADESNHCVEVRBSDTKYTLC
CCHHHHHHHHHHHHCCCCEEECCCCCCCEEEECCCCEEEEC
CCHHHHHCCCHCCCEHHHHCHHHHHECCCCCHHHHHHHCCC
> 6f45_2
SASIAIGDNDTGLRWGGDGIVQIVANNAIVGGWNSTDIFTEAGKHITSNGNLNQWGGGAIYCRDLNVS
CCEEECCCCCCEEEECCCCCEEEEECCEEEEEECCCEEEECCCCEEEECCCEEECCCCCEEECCEEEC
CCCEECCCCCCCCCCCCCCEEEEEECCEEEEEECCCCEEEECCCEECCCCCCCCCCCCCEEEEEECCC
> 6qek_1
DIYGDEITAVVSKIENVKGISQLKTRHIGQKIWAELNILVDPDSTIVQGETIASRVKKALTEQIRDIERVVVHFEPAR
CHHHHHHHHHHCCCCCCCEEEEEEEEEECCEEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCCCEEEEEEEEEECC
CCCHHHHHHHHHCCCCCEEHHHEEECCCCCEEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCHHHCEEEEEECCCC
> 6qfj_1
SHMEDYIEAIANVLEKTPSISDVKDIIARELGQVLEFEIDLYVPPDITVTTGERIKKEVNQIIKEIVDRKSTVKVRLFAAQEEL
CHHHHHHHHHHHHHHCCCCCCEEEEEEEEEECCEEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEEEEECCCEC
CCCHHHHHHHHHHHHCCCCEEEEEEEEEEHHCCEEEEEEEEEECCCCCEHHHHHHHHHHHHHHHHHCCCCCEEEEEECCCCCCC
> 6btc_1
XNKKSKQQEKLYNFIIAKSFQQPVGSTFTYGELRKKYNVVCSTNDQREVGRRFAYWIKYTPGLPFKIVGTKNGSLLYQKIGINPC
CCCCCHHHHHHHHHHHHHHHHCCCCCEECHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHCCCCCEEEEEEECCEEEEEECCCCCC
CCCCCHHHHHHHHHHHHHHECCCCCCEEEEHHHHHHCCCCCCHHHHHHHHHHHHHHHHCCCCCCEEEECCCCCCEEEEECCCCCC
> 6d7y_1
LDTAQAPYKGSTVIGHALSKHAGRHPEIWGKVKGSXSGWNEQAXKHFKEIVRAPGEFRPTXNEKGITFLEKRLIDGRGVRLNLDGTFKGFID
CCCCCCEECCEEHHHHHHHHHHHHCHHHHCCCCCCHHHHHHHHHHHHHHHHHCCCCCEEEECCCCCEEEEEECCCCCEEEEECCCCEEEEEC
CCCCCCCCCCCCHHHHHHHHHCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHCCCEEEEEECCCCCEEEEEECCCCCEEEECCCCCEEEECC
> 6g57_1
KRSGFLTLGYRGSYVARIMVCGRIALAKEVFGDTLNESRDEKYTSRFYLKFTYLEQAFDRLSEAGFHMVACNSSGTAAFYRDDKIWSSYTEYIFFRP
CCCEEEEEEEEEECCCEEEEEEEHHHHHHHHHHHCCECCCCCCEEEEECCCCCHHHHHHHHHHCCCEEEEEEEEEEEECCCCCCEEEEEEEEEEEEC
CCCCCEEEEECCCHHHHEEEECCHHHHHHHHCCCHHHCCCCCCCEEEECCHCHHHHHHHHHHHCCCEEEEECCCCCCCHCHHHHCCCCCCEEEEECC
> 6msp_1
MGHHHHHHENLYFQSHMTDELLERLRQLFEELHERGTEIVVEVHINGERDEIRVRNISKEELKKLLERIREKIEREGSSEVEVNVHSGGQTWTFNEK
CCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHCCCCCEEEEEEEECCEEEEEEEECCCHHHHHHHHHHHHHHHHHCCCCCEEEEEEECCEEEEEECC
CCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCHHHHHHHHCHHHHHHHHHHHHHHHHHHHCCCCCCCCECCCCCCCCEEHCC
> 6gnx_1
SLKPFTYPFPETRFLHAGPNVYKFKIRYGNKEVITQELEDSVRVVLGNLDNLQPFATEHFIVFPYKSKWERVSHLKFKHGEIILIPYPFVFTLYVEXKW
CCCCCCCCCCEEEEEEECCEEEEEEEEECCHHHHHHHHHHHHHHHHHCCCCCCCEECCCEEEEEEEEECCCCCCEEEEECCEEEEEEEEEEEEEEEECC
CCCCCCCCCCCEEEEEECCEEEEEEEECCCCCEEHHHHHHHEEEEECCCCCCCCCCCCCEEECCCCCHHCCCCCCEEEECCCECCCCCEEEEEEEECCC
> 6cp9_2
XFIENKPGEIELLSFFESEPVSFERDNISFLYTAKNKAGLSVDFSFSVVEGWIQYTVRLHENEILHNSIDGVSSFSIRNDNLGDYIYAEIITKELINKIEIRIRPDIKIKSSSVI
CCCCECCCHHHHHHHHCCCCCEEECCCCEEEEEEECCCCEEEEEEEECCCCEEEEEEEECCEEEEEEEEECCCECEEEEECCEEEEEEEEECCCCEEEEEEECCCCCEEEEEEEC
CHHHHCCCHHHHHHHHCCCCEEECCCCCEEEEEEECCCCEEEEEEECHHHCCEEEEEEECCEEEEEECHCCCEEEEEEECCCCCEEEEEEECCCHEEEEEEEEECEEEEEEEEEC
> 6cp9_1
TAQTIANSVVDAKKFDYLFGKATGNSHTLDRTNQLALEXKRLGVADDINGHAVLAEHFTQATKDSNNIVKKYTDQYGSFEIRESFFIGPSGKATVFESTFEVXKDGSHRFITTIPKNG
CHHHHHCCCCCHHHHHHHCCCCCCECCECCCHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHCCCHHHEEEEEECCCCEEEEEEEEEECCCCCEEEEEEEEEECCCCCEEEEEEEEECC
CHHHCCCEEECCCCHHHHHCCCCCCCCHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHCCCCEEEEECCCCCCEEEEEEEEECCCCCEEEEEEEEEECCCCCCCEEEEEECCC
> 6d34_1
PASPNIEAILASYAGFRDRDIEGILSGMHPDVEWVHPEGMGKYGLGGTKLGHAGIKEFLAHVPTVLGGMRLAPREFIEQGDRVVVFGTREVTSLRGTTATLDFVHSWTMRDGKATRMEDIFDTVAFHELIES
CCCHHHHHHHHHHHHHHCCCHHHHHCCEEEEEEEEECHHHHHHCCCEEEEHHHHHHHHHHHHHHCEEEEEEEEEEEEEECCEEEEEEEEEEEECCCCEEEEEEEEEEEEECCEEEEEEEECCHHHHHHHHCC
CCCHHHHHHHHHHHHHHCCCHHHHHHHCCCCEEEEECCCCCCCCCCCEEECHHHHHHHHHHHHHHCCEEEEEEHEEEECCCEEEEEEEEEEEECCCCEEECCEEEEEEEECCEEEEEEEECCHHHHHHHHHC
> 6rap_1
AITADDIAVQYPIPTYRFIVTLGDEQVPFTSASGLDINFDTIEYRDGTGNWFKMPGQRQAPNITLSKGVFPGKNAMYEWINAIQLNQVEKKDIMISLTNEAGTEVLVSWNVSNAFPTSLTSPSFDATSNEIAVQQITLMADRVTIQTA
CCCCCCHHHHCCCCCCCEEEEECCEECCCCEEECCCCCCCEEEEECCCCCEEEEECCCCCCEEEEEEECCCCCCCCHHHHCCCECCECCCEEEEEEEECCCCCCEEEEEEEEEEEEEEEECCCCCCCCCCCCEEEEEEECCCEEEEEC
CCCHHHCCCCCCCCEEEEEEEECCCEEEEEEECCCCEEEEEEEEHCCCCCCEECCCCCCCCCEEEEECEECCCHHHHHHHHHHHCCCCECCEEEEEEECCCCCCEEEEEEEEEECCEECCCCCCCCCCCCEEEEEEEEEECCEEEECC
> 6d7y_2
XKELFEVIFEGVNTSRLFFLLKEIESKSDRIFDFNFSEDFFSSNVNVFSELLIDSFLGFNGDLYFGVSXEGFSVKDGLKLPVVLLRVLKYEGGVDVGLCFYXNDFNSAGKVXLEFQKYXNGISADFGFENFYGGLEPASDQETRFFTNNRLGPLL
CCEEEEEEEECCCCCCHHHHHHHHHCCCCEEEEEEECCCCCCCCCCCCCHHHHHHHHHCCCCEEEEEEEEEEEEECCEEEEEEEEEEEEECCEEEEEEEEEECCHHHHHHHHHHHHHHHHHHHHHHCCCEEEEEECCCCCHHHEEEECCEECCCC
CHHHHHHEEECCCHHHHHHHHHHHHCCCCCCECCEECCCCCCCCCCHHCHHHHHHHHCCCCCEEEEEEECCCCCCCCEECCEEHEEEEEECCEEEEEEECCCCHHHHHCHHHHHHHHHHHHHHHHCCCCEEEECECCCCCCCEEEECCCCCCCCC
> 6cp8_3
VNSTAKDIEGLESYLANGYVEANSFNDPEDDALECLSNLLVKDSRGGLSFCKKILNSNNIDGVFIKGSALNFLLLSEQWSYAFEYLTSNADNITLAELEKALFYFYCAKNETDPYPVPEGLFKKLXKRYEELKNDPDAKFYHLHETYDDFSKAYPLNN
CCCHHHHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHCHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHCCCCCHHHHCHHHHHHHHHHHCCCCC
CCCHHHHHHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHHCCHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHCCHHHHHHHHHHHHEECCCCCCCCCCHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHCCCC
> 6cp8_1
NSFEVSSLPDANGKNHITAVKGDAKIPVDKIELYXRGKASGDLDSLQAEYNSLKDARISSQKEFAKDPNNAKRXEVLEKQIHNIERSQDXARVLEQAGIVNTASNNSXIXDKLLDSAQGATSANRKTSVVVSGPNGNVRIYATWTILPDGTKRLSTVTGTFK
CCCEEEEEECCCCCEEEEEEECCEEEECHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHCCCCECCECEEEEEEEECCEEEEEEEEEEECCCCCEEEEEEECCCC
CCHCECCCCCCCCCEEEEEECCCEEECCCHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHCHHHHCCCCCCCHHHHHHCCCCCCHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHCCCCCCCEEEEEEEECCCCCEEEEEEEEECCCCCCCEEEEHCCCC
> 6e4b_1
AMRLWLIRHGETQANIDGLYSGHAPTPLTARGIEQAQNLHTLLHGVSFDLVLCSELERAQHTARLVLSDRQLPVQIIPELNEMFFGDWEMRHHRDLMQEDAENYSAWCNDWQHAIPTNGEGFQAFSQRVERFIARLSEFQHYQNILVVSHQGVLSLLIARLIGMPAEAMWHFRVDQGCWSAIDINQKFATLRVLNSRAIGVEN
CEEEEEEECCCEHHHHHCECCCCCCCCECHHHHHHHHHHHHHCCCCCCCEEEECCCHHHHHHHHHHCCCCCCCEEECHHHCCCCCHHHCCCEHHHHHHHCHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECHHHHHHHHHHHCCCCHHHHHHECCCCCCEEEEEEECCEEEEEEEEECCCCCCC
CCEEEEEECCCCHHHHHCCEECCCCCCCCHHHHHHHHHHHHHHCCCCCCEEEECCHHHHHHHHHHHHHCCCCCEEECCHHHHHCCCCEECCCHHHHHHHCHHHHHHHHHCHHHEECCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECCHHHHHHHHHHHCCCHHHHCEEEECCCEEEEEEECCCCEEEEEHCCCCEHECC
> 6rbk_1
SLLERGLSKLTLNAWKDREGKIPAGSMSAMYNPETIQLDYQTRFDTEDTINTASQSNRYVISEPVGLNLTLLFDSQMPGNTTPIETQLAMLKSLCAVDAATGSPYFLRITWGKMRWENKGWFAGRARDLSVTYTLFDRDATPLRATVQLSLVADESFVIQQSLKTQSAPDRALVSVPDLASLPLLALSAGGVLASSVDYLSLAWDNDLDNLDDFQTGDFLRAT
CCCCCCCCCCEEEEECCCCCCCEEEEEECCCCCCCEEEEEEEEEEECCCCCCCCCCEEEEEEEEEEEEEEECCECCCCCCCCCHHHHHHHHHHHCCCCCCCCCCCCCEEECCCCCCCCCCEECCCEEECCCCCCCECCCCCECECCEEEEECCCCCCCHHHHHCCCCCCCCCCEECCCCCCHHHHHHHHHHHCCCCCCHHHHHHHCCCCCCCCCCCCCEECCC
CCCCCHHHEEEEEECCCCCCCCCCCEEEEEECCCEEEEEEEEEECCCCCCCCCCCCCEECCCCCCEEEEEEEEECCCCCCCCEHHHHHHHHHHHEEECCCCCCCCEEEEEECCCCCCCCCCEEEEEEEEEEEEEEECCCCCEEEEEEEEEEEECCCHHHHHHHHCCCCCCCCEEEEEECCCCCHHHHHHHHHHCCHHHHHHHHHHCCCCCCCCCCCCCEEECC
> 6f45_1
AVQGPWVGSSYVAETGQNWASLAANELRVTERPFWISSFIGRSKEEIWEWTGENHSFNKDWLIGELRNRGGTPVVINIRAHQVSYTPGAPLFEFPGDLPNAYITLNIYADIYGRGGTGGVAYLGGNPGGDCIHNWIGNRLRINNQGWICGGGGGGGGFRVGHTEAGGGGGRPLGAGGVSSLNLNGDNATLGAPGRGYQLGNDYAGNGGDVGNPGSASSAEMGGGAAGRAVVGTSPQWINVGNIAGSWL
CCCCCCHHHHHHHHHCCCEHHHHHHHCCCCCCCEEHHHHCCCCCCEEEEECCCEEEECHHHHHHHHHHCCCCCEEEEECCCEEECCCCCCCEEECCCCCCCCEEEEECCEEECCCCCCCECCCCCCCCCCCEEECCHHHEEEEECCEEECCCCCCCCEEECCEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEECCEECCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCEEEECCEEECCCC
CCCCCCCCCCHHHCCCCCCHHHHCHEEEEEECCCCCHHCCCCCCCCCCECECCCCCCCCCCEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHCC
> 6mxv_1
IXQHSSGFLKLVDDAKSRIQECSVDDIQKXNETQTLDGLLIDTREESEVANGYIPNAIHLSKGIIESAIESAVPNKNQKXYFYCGGGFRSALVADKLREXGYKNVISVDGGWRAWNAKGYPTVSPNQFRPNEFLKLVNNAKTQIKECSTTELYNKINSQELDGIVFDVREDSEFNRFHIQGATHLSKGQIEVKIENLVPNKQQKIYLYCGSGFRSALAAESLQHXGYTNVVSIAGGIKDWLANNYPVSQN
CCCCCHHHHHHHHHHHCCCEEECHHHHHHHHHHCCCCCEEEECCCHHHHCCCECCCEEECCCCCHHHHHHHHCCCCCCCEEEECCCCCHHHHHHHHHHHCCCCCEEEECCHHHHHHHCCCCCECHHHCCCHHHHHHHHHHHHHCEEECHHHHHHHHHCCCCCCEEEECCCHHHHHHCECCCCEECCCCCHHHHHHHHCCCCCCCEEEECCCCHHHHHHHHHHHHCCCCCEEEECCHHHHHHHCCCCCECC
CCHCHHHHHHHHHHHHHCCCCCCHHHHHHHHHCCCCCEEEEECCCHHHHHHCCCCCCCECCCCHHHHHHHHCCCCCCCEEEEEECCCCHHHHHHHHHHHCCCCEEEEECCCHHHHHHCCCCEECCCCCCCHHHHHHHHHHHCCCCCCCHHHHHHHHHCCCCCEEEEECCCHHHHHHCCCCCCEECCCHHHHHHHHHCCCCCCCEEEEEECCCCHHHHHHHHHHHCCCCHEEEHCCCHHHHHHCCCCECCC
> 5z82_1
SIGLAHNVTILGSGETTVVLGHGYGTDQSVWKLLVPYLVDDYKVLLYDHMGAGTTNPDYFDFDRYSSLEGYSYDLIAILEEFQVSKCIYVGHSMSSMAAAVASIFRPDLFHKLVMISPTPRLINTEEYYGGFEQKVMDETLRSLDENFKSLSLGTAPLLLACDLESAAMQEYCRTLFNMRPDIACCITRMICGLDLRPYLGHVTVPCHIIQSSNDIMVPVAVGEYLRKNLGGPSVVEVMPTEGHLPHLSMPEVTIPVVLRHIRQDITD
CHHHHCCCEEEECCCCEEEEECCCCCCHHHHCCCHHHCCCCCEEEEECCCCCCCCCHHHCCCCHHHCCHHHHHHHHHHHHHCCCCCEEEEEECHHHHHHHHHHHHCHHHEEEEEEECCCCCCECCCCCCCCECHHHHHHHCCCHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHCCHHHHHHHHHHHHCCCCHHHHHHCCCCEEEEEEEECCCCCHHHHHHHHHHCCCCEEEEEEEEECCCHHHHCHHHHHHHHHHHHHCCCCC
CHHHHCCEEEEECCCCEEEEECCCCCCCHHHHHHHHHHHCCCEEEEEECCCCCCCCHHHCCHHHHCCHHHHHHHHHHHHHHHCCCCEEEEECCHHHHHHHHHHHCCCHHHHHEEEEECCCCECCCCCCECCCCHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHCCCHHHHHHHHHHEEECCCHHHHCCCCCCEEEEECCCCEECEHHHHHHHHHHCCCCCEEEEECCCCCCCCCCCCHHHHHHHHHHHHHHHCC
> 6rap_5
SNYQTLVDVNNAMNKMLRAYVNEAVAIRFDLPDTQADAAISVFLYDIHEDLQLRTAESRGFNAGAGRLLPGWVNVKCNYLITYWESPDSQPDNQAIQVMSQVLAALINNRQLADIGAYTQVMPPKENLNSLGNFWQSLGNRPRLSLNYCVTVPISLSDKGEEMTPVKSLSTTVEPKAPLSPLVITDALREQLRVALDACLAMTHVNLDSSPVANSDGSAAEIRVSLRVYGMTPTEYLAPMNTVFNEWEKSEAAAVTPDGYRVYINAVDKTDLTGI
CCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCEEECCCCECHHHCCCCCCCCCCCCCCCCCCCEEEECCEEECCCCCCCCCCCCCCCCCHHHHHHHHHHCCCCCCCCCEEECCCCCCCHHHHHHHHHHCCCCCCCCEEEEEEEEECCCCCCCCCCECCCEEEEEEECCCCCHHHHHHHHHHHHHHHCCHHHHHHHECCEEEEECCCCCCCCCCEEEEECEECCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCCEECCCCCCECCCC
CCHHHHHHHHHHHHHHHHHHCCCCCEEEECCCCCCCCCEEEEEEEEECCCHHHHCCCCCCCCCCCCCCCCCCEEEEEEEEEEEECCCCCCCCHHHHHHHHHHHHHHHCCCHHHHCCCCEEEECCCCCCCCHHHHHHHCCCCCCEEEEEEEEEEECCCCCCCCCCCEEEEEEEEECCCCCCHHHHHHHHHHHHHHHHHHHHHCCEEECCCCCCCCCCCCCCCCEEEEEEECCCCHHHHHHHHHHHHHHHCCCEEEEEECCEEEEEEECCHHHHEEC
> 6d2v_1
GLVPRGSHMEGKKILVTGGTGQVARPVAEALAERNEVWCLGRFGTPGVEKELNDRGITTFHWDMDDPGAAAYEGLPDDFTHVLHSAVRRGEDGDVNAAVEVNSVACGRLMTHCRGAEAFLFVSTGALYKRQTLDHAYTEDDPVDGVADWLPAYPVGKIAAEGAVRAFAQVLNLPTTIARLNIAYGPGGYGGVPMLYFKRMLAGEPIPVPKEGQNWCSLLHTDDLVAHVPRLWEAAATPATLVNWGGDEAVGITDCVRYLEELTGVRARLVPSEVTRETYRFDPTRRREITGPCRVPWREGVRRTLQALHPEHLPS
CCCCHHHHCCCCEEEEECCCCCCHHHHHHHHHCCCEEEEEECCCCCCHHHHHHHCCCEEEECCCCCCCHHHHCCCCCCCCEEEECCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEEEEEHHHECCCCCCCEECCCCCECCCCCCCCHHHHHHHHHHHHHHHHHHHHCCCEEEEEECCEECCCCCCEHHHHHHHHHHHCCCEEEECCCCCEECCEEHHHHHHHHHHHHHCCECCCEEEEECCCCCEEHHHHHHHHHHHHCCCCCEEEECCCCCCCEECCHHHHHHHCCCCCCHHHHHHHHHHHHCHHHCCC
CCCCCCCCCCCCEEEEECCCCCCHHHHHHHHHCCCCEEEEECCCCHHHHHHHHCCCCEEEEEEHCCHHHHHHHHCCCCCCEEEEHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHCHCEEEEECCCCHCCCCCCCCCCCECCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCHHEEEEEEECCCCCCCCCHHHHHHHHHCCCCEEECCCCCCCCCCCEHHHHHHHHHHHHHCCCCCCEEEEECCCCCEEHHHHHHHHHHHHCCCCCECECCCCCCCCCCHHHHHHHHCCCCCCCHHHHHHHHHHHHCCCCCCC
> 6q64_1
LIPLIEAQTEEDLTPTXREYFAQIREYRKTPHVKGFGWFGNWTGKGNNAQNYLKXLPDSVDFVSLWGTRGYLSDEQKADLKFFQEVKGGKALLCWIIQDLGDQLTPKGLNATQYWVEEKGQGNFIEGVKAYANAICDSIEKYNLDGFDIDYQPGYGHSGTLANYQTISPSGNNKXQVFIETLSARLRPAGRXLVXDGQPDLLSTETSKLVDHYIYQAYWESSTSSVIYKINKPNLDDWERKTIITVEFEQGWKTGGITYYTSVRPELNSXEGNQILDYATLDLPSGKRIGGIGTYHXEYDYPNDPPYKWLRKALYFGNQVYPGKFD
CCCHHHCCCHHHCCHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCCCHHHCHHHCCCCCCEEECCCCCCCCCHHHHHHHHHHHHCCCCEEEEEEEECCCCCCCCCCCCCHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCCCCCCCCCCCECCCCCHHHHHHHHHHHHHHCCCCCEEEEEECHHHECHHHHCCCCCEEEECCCCCEHHHHHHHHCCCCCCCHHHHEEEEEEHHHHCCCCCCCCCECCCHHHHHCCCHHHHHHHCCECCCCCECCEEEEECHHHHCCCCCCCHHHHHHHHHHHHHCCCCCC
CCCCHCCCCHHHCCHHHHHHHHHHHHHHCCCCCEEEEEECCCCCCCCCCCHHHHHCCCCCCEEEECCCCCCCCHHHHHHHHHHHHCCCCEEEEEEEHHHHCCCCCCCCCCCHHCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHCCCCCCEEEECCCCCCCCHHHHHHHCHEEEECCCCCCCCCCCCCHCCCCCCCCCCCEEEECCHHHHHHCCCCCCCCCCCCCCCCCCCHHHHHHHEECCCCCCCEEEEEEEEHHCCCCCCCCHHHHHHHHHHCCCCCCCCCC
> 6n91_1
MITSSLPLTDLHRHLDGNIRTQTILELGQKFGVKLPANTLQTLTPYVQIVEAEPSLVAFLSKLDWGVAVLGDLDACRRVAYENVEDALNARIDYAELRFSPYYMAMKHSLPVTGVVEAVVDGVRAGVRDFGIQANLIGIMSRTFGTDACQQELDAILSQKNHIVAVDLAGDELGQPGDRFIQHFKQVRDAGLHVTVHAGEAAGPESMWQAIRDLGATRIGHGVKAIHDPKLMDYLAQHRIGIESCLTSNLQTSTVDSLATHPLKRFLEHGILACINTDDPAVEGIELPYEYEVAAPQAGLSQEQIRQAQLNGLELAFLSDSEKKALLAKAALRG
CCCCCCCCEEEEEEHHHCCCHHHHHHHHHHHCCCCCCCCHHHHHHHHCCCCCCCCHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHCCCCEEEEEECHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHCCEEEEEEEEEHHHCHHHHHHHHHHHHCCHHHCCEEEEECCCCCCCHHHHHHHHHHHHHCCCEEEEEECCCCCHHHHHHHHHHCCCCEEEECHHHHHCHHHHHHHHHHCCEEEECHHHHHHCCCCCCHHHCCHHHHHHCCCCEEECCECHHHHCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHCCC
CCCCCCCEEEEEECCCCCCCHHHHHHHHHHHCCCCCCCCHHHHHHHEEECCCCCCHHHHHHHEECCHHHCCCHHHHHHHHHHHHHHHHHHCCEEEEEEECCHHHHHHCCCCHHHHHHHHHHHHHHHHHHCCCEEEEEEEECCCCCHHHHHHHHHHHHHHCCCEEEEECCCCCCCCCHHHHHHHHHHHHHCCCCEEEECCCHCCCHHHHHHHHHCCCEEECCHHHHHCCHHHHHHHHHCCCCEEECCCHHHEEECCHHHHHCCHHHHHHCCCEEEEECCCCCCCCCCCHHHHHHHHHHCCCCHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHCCC
> 6cvz_1
KHKYHFQKTFTVSQAGNCRIMAYCDALSCLVISQPSPQASFLPGFGVKMLSTANMKSQYIPMHGKQIRGLAFSSYLRGLLLSASLDNTIKLTSLETNTVVQTYNAGRPVWSCCWCLDEANYIYAGLANGSILVYDVRNTSSHVQELVAQKARCPLVSLSYMPRAASAAFPYGGVLAGTLEDASFWEQKMDFSHWPHVLPLEPGGAIDFQTENSSRHCLVTYRPDKNHTTIRSVLMEMSYRLDDTGNPICSCQPVHTFFGGPTAKLTKNAIFQSPENDGNILVCTGANSALLWDAASGSLLQDLQTDQPVLDICPFEVNRNSYLATLTEKMVHIYKWE
CCCEEEEEEEECCCCCCCCCEEEECCCCEEEEEEECCCCCCCCCEEEEEEECCCCCCEEEEEECCCEEEEEECCCCCCEEEEEECCCEEEEEECCCCEEEEEEECCCCEEEEEECCCCCCEEEEEECCCCEEEEECCCCCCCCEEECCCCCCCCEEEEEEECCCCCCCCCCCEEEEEECCEEEEEEECCCCCEEEEECCCCEEEEEEEEEECCCCEEEEEEEECCCCCCCEEEEEEEEEEECCCCCEEEEEEEEEEEECCCCCCCCECEEEECCCCCCCEEEEECCCEEEEEECCCCCEEEEEECCCCCCEEEEEEECCEEEEEEECCCEEEEEEEC
CCCEEEEEEEEECCCCCCEEEEECCCCCEEEEECCCCCCCCCCCCCEEEECCCCCCCEEECCCCCHHEEEEECCCCCCEEEEEHCCCCEEEEECCCCEEEEEEECCCCCEEEEECCCCCCEEEEECCCCEEEEEECCCCCCCCHEECCCCCCCCEEEEEECCCCCCCCCCCCCEEEEEECCEEEEECCCCCCCCCCCCCCCCCCEEEEEECCCCCEEEEEECCCCCCCCCCHEHHHCCCCCCCCCCCEEEEEEEEEECCCCCCECCHHCEEECCCCCCEEEEEECCCCEEEEECCCCCEEEECCCCCCEEEEEEEECCCCCEEEEECCCEEEEEECC
> 6ek4_1
VVYPEINVKTLSQAVKNIWRLSHQQKSGIEIIQEKTLRISLYSRDLDEAARASVPQLQTVLRQLPPQDYFLTLTEIDTELEDPELDDETRNTLLEARSEHIRNLKKDVKGVIRSLRKEANLMASRIADVSNVVILERLESSLKEEQERKAEIQADIAQQEKNKAKLVVDRNKIIESQDVIRQYNLADMFKDYIPNISDLDKLDLANPKKELIKQAIKQGVEIAKKILGNISKGLKYIELADARAKLDERINQINKDCDDLKIQLKGVEQRIAGIEDVHQIDKERTTLLLQAAKLEQAWNIFAKQLQNTIDGKIDQQDLTKIIHKQLDFLDDLALQYHSMLLS
CCCCCCCHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHCCCECCCCCHHHHCCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCCEHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHCCCCC
CCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHC
> 6rap_3
TTTYPGVYLSEDAVSSFSVNSAATAVPLFAYDSENTNTINKPIQVFRNWAEFTVEYPTPLEDAFYTSLSLWFMHGGGKCYLVNEANIADAVAQYDDITLIVAAGTDTTTYTAFTTVVGQGYRIFGLFDGPKEKIAGTAKPDEVMEEYPTSPFGAVFYPWGTLASGAAVPPSAIAAASITQTDRTRGVWKAPANQAVNGVTPAFAVSDDFQGKYNQGKALNMIRTFSGQGTVVWGARTLEDSDNWRYIPVRRLFNAVERDIQKSLNKLVFEPNSQPTWQRVKAAVDSYLHSLWQQGALAGNTPADAWFVQVGKDLTMTQEEINQGKMIIKIGLAAVRPAEFIILQFSQDI
CCCCCCCCCCCCCCCCCCCCCCCCCCCEEEECCCCCCCCCCCEEEECCHHHHHHHCCCCCCCCCHHHHHHHHCCCCCCEEEEECCCHHHHHHHHCCCCCEEECCCCCCHHHHHHHHHHCCCCCCEECCCCCCCCCCCCCHHHCCCCCCCCCCEECCCCCECCCCCCCECHHHHCCHHHHHHHCCCCCCCCCCCCECCCCCECCCCCHHHHCCCCCCCCCCEEECCCCCCCEEECCCCCCCCCCCCCHHHHHHHHHHHHHCCCCCHHHCCCECCHHHHHHHHHHHHHHHHHHHHCCCECCCCHHHHEEEECCCCCCCCHHHHCCCCEEEEEEECECCEECEEEEEEECCC
CCCCCCEEEEECCCCCCCCCCCCCCCEEEEECCCCCCCCCCCEEEECCHHHHHHHHCCCCCCHHHHHHHHHHHCCCCHCEEEECCCHHHHHHHCCCCEEEEECCCCCHHHHHHHHHHHHCCCEEEEEECCCCCCCCCCCHHHHCCCCCCCHHHHHHCCHEEECCEEEECCCHHHHHHHHHECCCCCCEECCCCHHHHCCCCEEECCCHHHHHHCCCCCEEEEEECCCCCEEEECCEECCCCCCCEEEEHHHHHHHHHHHHHHHCCHEEECCCCHHHHHHHHHHHHHHHHHHHHCCCECCCCHHHHEEEECCCCCCCCHHHHHCCEEEEEEEEEECCCCEEEEEEEHHCC
> 6cci_1
VELPPEEADLFTGEWVFDNETHPLYKEDQBEFLTAQVTCMRNGRRDSLYQNWRWQPRDASLPKFKAKLLLEKLRNKRMMFVGDSLNRNQWESMVBLVQSVVPPGRKSLNKTGSLSVFRVEDYNATVEFYWAPFLVESNSDDPNMHSILNRIIMPESIEKHGVNWKGVDFLVFNTYIWWMNTFAMKVLRGSFDKGDTEYEEIERPVAYRRVMRTWGDWVERNIDPLRTTVFFASMSPLHIKSLDWENPDGIKDALETTPILNMSMPFSVGTDYRLFSVAENVTHSLNVPVYFLNITKLSEYRKDAHTSVHTIRQGKMLTPEQQADPNTYADDIHWCLPGLPDTWNEFLYTRIISR
CCCCCCCCCCCCEEEEECCCCCCCCCHHHCCCCCCCCCCCCCCCCCCHHHHEEEEECCCCCCCCCHHHHHHHCCCCEEEEEECHHHHHHHHHHHHHHHCCCCCCCEEEEEECCEEEEEECCCCEEEEEEECCCCCCECCCCCCCCCCCCCEECCCCCHHHHHHHCCCCEEEECCCHHHCCCCEEEEECCCHHHCCCCEEEEEHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHCCCCEEEECCHHHHHCCCCCEEECCCEECCEECCHHHHCCHHHHCEEEEECCCCHHHHHHHHHHHHHHCC
CCCCCCCCCCCCCEEEECCCCCCCCCCCCCCCCCCHHHHHHCCCCCCHHCEEEECCCCCCCCCCCHHHHHHHHCCCEEEEECCCCCCCHHHHHHHHHHCCCCCCCCCEECCCCEEEEEHEEECEEEEHHHHHHHEECCCCCCCCCCCCCCEEEHHHHHHHHHHCCCCEEEEEEHHHHHHCCCCEEEEECCCCCCCCCEEECCHHHHHHHHHHHHHHHHHHCCCCCCCEEEEEHCCCCCCCHHHCCCCCCCEECCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHCCCCCEEEEEEHHHHHHHHCCCCCCEEHHCCCCCCHHHHCCCCCCCCCCHCCCCCCCHHHHHHHHHHHHCC
> 6cl6_1
ALAATDIPGLDASKLVSGVLAEQRLPVFARGLATAVSNSSDPNTATVPLMLTNHANGPVAGRYFYIQSMFYPDQNGNASQIATSYNATSEMYVRVSYAANPSIREWLPWQRCDIGGSFTKEADGELPGGVNLDSMVTSGWWSQSFTAQAASGANYPIVRAGLLHVYAASSNFIYQTYQAYDGESFYFRCRHSNTWFPWRRMWHGGDFNPSDYLLKSGFYWNALPGKPATFPPSAHNHDVGQLTSGILPLARGGVGSNTAAGARSTIGAGVPATASLGASGWWRDNDTGLIRQWGQVTCPADADASITFPIPFPTLCLGGYANQTSAFHPGTDASTGFRGATTTTAVIRNGYFAQAVLSWEAFGR
CCCHHHCCCEEHHHEEECCCCHHHCCHHHHCECCCCCCCCCHHHCCCCEEEECCCCCCCCCCCEEEEEEEECCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCECCCEECCHHHECECCCCEECCCCCEHHHCCCCEEEEECCHHHHHHCECCCCCCCEEEEEEEEECCEEEEEEEECCCCCEEEEEEECCEECCCEEECECCCCCHHHECEHHHCCHHHCCCCCCCECECCCCEEHHHEEECCCCHHHCCCCCCCHHHHHHHHCCCCCCEEECCCCEEEEECCCCEEEEEEEEEECCCEEEEEECCCCCCCCEEEEEEEECCCCCCCCCCCEEEECCCCCEEEEEECCCCCEEEEEEEEEC
CCCCCCCCCHHHHHHHCCCHCCCCCCCCCCCEHCECCCCCCCCCCCCCEEEEECCCCCCCCCEECEHHEECCCCCCCCHHEEEEECCCCHEEHHHEHCCCCCCCCCCCHHHCCCCCCCCCCCCCCCCCCCCHHHHCCCCEEEEECCCCCCCCCCCCCHHHCEEEEEECCCCEEEEEEEECCCCEEEEEEECCCCCCECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEECCCCCCCCCCCCHHCCCCCCCCCCCCCCECECCCCCCEEEEEEEEECCCCCCEEEEEEECCCCCEEEEEEEECCCCCCCCCCCEEEEECCCCEEEEECCCCCCEEEEEEEECC
> 6cl5_1
ALAATDIPGLDASKLVSGVLAEQRLPVFARGLATAVSNSSDPNTATVPLMLTNHANGPVAGRYFYIQSMFYPDQNGNASQIATSYNATSEMYVRVSYAANPSIREWLPWQRCDIGGSFTKTTDGSIGNGVNINSFVNSGWWLQSTSEWAAGGANYPVGLAGLLIVYRAHADHIYQTYVTLNGSTYSRCCYAGSWRPWRQNWDDGNFDPASYLPKAGFTWAALPGKPATFPPSGHNHDTSQITSGILPLARGGLGANTAAGARNNIGAGVPATASRALNGWWKDNDTGLIVQWMQVNVGDHPGGIIDRTLTFPIAFPSACLHVVPTVKEVGRPATSASTVTVADVSVSNTGCVIVSSEYYGLAQNYGIRVMAIGY
CCCHHHCCCEEHHHEEECCCCHHHCCHHHHCECCCCCCCCCHHHCCCCEEEECCCCCCCCCCCEEEEEEEECCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCECCCEECCHHHECECCCCEECCCCCEHHHCCCCEEEEECCHHHHHCCECCCCCCCEEEEEEEEECCEEEEEEEECCCCEEEEEEECCEECCCEEECECCCCCHHHECEHHHCCCCCCCCCCCCECECCCCEEHHHEEECCCCHHHCCCCCCCHHHHHHHHCCCCCCEEECCCCEEEEECCCCEEEEEEEEEEECCCCCCEEEEEECCCCCCCCEEEEEEEEEEEECCCCCHHHEEEEEEEECCCEEEEEEEECCCCCCEEEEEEEEEEC
CHCCCCCHHHHHHHHHCCCHHHCCCCCCCCCCHCCCCCCCCCCCCCCCEEEEECCCCCCCCCEEEECCCEECCCCCCCEEEEEECCCCCHEEEHHEECCCCCCCCCCCHHHCCCCCCCCCCCCCCCCCCCCCHHHCCCCEEEECCCHCCCCCCCCCCHHCCEEEEEECCCCEEEEEEECCCCCEEEEECCCCCCCHHHHECCCCCCCCCCCCCCCCCCCCCCCCCCCECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCCEEEEEEEEEECCCCCCCCCEEEEECCCCCCCCEEEEEEEECCCCCCCCCCEEEEEEEECCCCCEEEEECCCCCCCCCCEEEEEEECC
> 6hrh_1
LYFQSMFSYDQFFRDKIMEKKQDHTYRVFKTVNRWADAYPFAQHFSSKDVSVWCSNDYLGMSRHPQVLQATQETLQRHGVGAGGTRNISGTSKFHVELEQELAELHQKDSALLFSSCFVANDSTLFTLAKILPGCEIYSDAGNHASMIQGIRNSGAAKFVFRHNDPDHLKKLLEKSNPKIPKIVAFETVHSMDGAICPLEELCDVSHQYGALTFVDEVHAVGLYGSRGAGIGERDGIMHKIDIISGTLGKAFGCVGGYIASTRDLVDMVRSYAAGFIFTTSLPPMVLSGALESVRLLKGEEGQALRRAHQRNVKHMRQLLMDRGLPVIPCPSHIIPIRVGNAALNSKLCDLLLSKHGIYVQAINYPTVPRGEELLRLAPSPHHSPQMMEDFVEKLLLAWTAVGLPLQCRRPVHFELMSEWERSYFGNM
CCCCCCCCHHHHHHHHHHHHHHHCCCCCCCEEEECCCCCCEEEEECCEEEEECCCCCCCCHHHCHHHHHHHHHHHHHHCCCCCCCCCCCCECHHHHHHHHHHHHHCCCCEEEEECCHHHHHHHHHHHHHHHCCCCEEEEECCCCHHHHHHHHHHCCEEEEECCCCHHHHHHHHHCCCCCCCEEEEEECECCCCCCECCHHHHHHHHHHCCEEEEEECCCCCCCCCCCCCCHHHHCCCHHHCCEEEEECCCCCCCCCEEEEECHHHHHHHHHHCHHHHCCCCCCHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHCCCCECCCCCCCEEEECCCHHHHHHHHHHHHHCCCEECCEECCCCCCCCCCEEEECCCCCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHCCCC
CCCCCCCCHHHHHHHHHHHHHHCCCEEEEHHHHHCCCCCCCCECCCCCCEEEEECCCCCCCCCCHHHHHHHHHHHHHCCCCCCCHECCCCCCHHHHHHHHHHHHHHCCCHHEEHHHHHHHHHHHHHHHHHHCCCCEEEECCCCHHHHHHHHHCCCCCEEEECCCCHHHHHHHHHHCCCCCCEEEEEEEEEECCCCECCHHHHHHHHHHHCCCEEEHEHCHCCCCCCCCCCEEHHCCHHCHEEEEECHHHHHHCECCCEEHHHHHHHHHHHCCCCCCHHHHCCHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHCCCCECCCCCCEEEEEECCHHHHHHHHHHHHHHCCCEECCCCCCCCCCCCEEEECCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCCHHHHHHHHHHHCCCCCC
> 6m9t_1
DCGSVSVAFPITMLLTGFVGNALAMLLVSRSYRRRESKRKKSFLLCIGWLALTDLVGQLLTTPVVIVVYLSKQRWEHIDPSGRLATFFGLTMTVFGLSSLFIASAMAVERALAIRAPHWYASHMKTRATRAVLLGVWLAVLAFALLPVLGVGQYTVQWPGTWAFISTNWGNLFFASAFAFLGLLALTVTFSCNLATIKALVSRGSNIFEMLRIDEGLRLKIYKDTEGYYTIGIGHLLTKSPSLNAAKSELDKAIGRNTNGVITKDEAEKLFNQDVDATVRGILRNAKLKPVYDSLDAVRRAALINMVFQMGETGVAGFTNSLRMLQQKRWDEAAVNLAKSRWYNQTPNRAKRVITTFRTGTWDAYGSWGRITTETAIQLMAIMCVLSVCWSPLLIMMLKMIFNEKQKECNFFLIAVRLASLNQILDPWVYLLLRKILGRPLEVL
CCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCHHHHHHCCCCHHHHHHHHHHHHHHHHHHCHHHHCCCCEEEECCCCEEEECCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHCCEEEEEECCCCCEEEECCEEEECCCCHHHHHHHHHHHHCCCCCCECCHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHCCHHHHHHHHHHHHHHCHHHHHCCHHHHHHHHCCCHHHHHHHHHCCHHHHHCHHHHHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHC
CCCCCCEEEHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCEEEEHHHHHHHHHHHHHECCCHHHHHHHHHCCCCCECCCCCCHHHHHHEEHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHCCHHHHHHHHHHHHHHHHHHHHCCHCCCCCEEEECCCCEEEECCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCECCHHHHHHHHHHHHHHHHEHHHHHHHEHHHHCHHHHHHHCCCCCCHCCHHHHHHHHHHHCCHHCHHHHEEECHHHHHHHHHHHHHCCCCCHHHCEECCCCCCHHHHHHHHHHHHHCCCCCCCCCCCCEHHCCCCCCCHHHHHHHHCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCHHHHHHHHHHHCCCCCCHCHHHHEHHHHHHCHHHCHHHHEEEEHHHHHHHHCC
> 5w6l_1
NAQELKERAKVFAKPIGASYQGILDQLDLVHQAKGRDQIAASFELNKKINDYIAEHPTSGRNQALTQLKEQVTSALFIGKXQVAQAGIDAIAQTRPELAARIFXVAIEEANGKHVGLTDXXVRWANEDPYLAPKHGYKGETPSDLGFDAKYHVDLGEHYADFKQWLETSQSNGLLSKATLDESTKTVHLGYSYQELQDLTGAESVQXAFYFLKEAAKKADPISGDSAEXILLKKFADQSYLSQLDSDRXDQIEGIYRSSHETDIDAWDRRYSGTGYDELTNKLASATGVDEQLAVLLDDRKGLLIGEVHGSDVNGLRFVNEQXDALKKQGVTVIGLEHLRSDLAQPLIDRYLATGVXSSELSAXLKTKHLDVTLFENARANGXRIVALDANSSARPNVQGTEHGLXYRAGAANNIAVEVLQNLPDGEKFVAIYGKAHLQSHKGIEGFVPGITHRLDLPALKVSDSNQFTVEQDDVSLRVVYDDVANKPKITFKG
CCCCHHHHHHCCCCCCCHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHCHHHCCCCCCCCCCCCCCCCCCCEEEECCCCHHHHHHHHHHHHHCCCCCCEEEECCCCEEEECCCHHHHHHCCCCHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHECHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHHHCCCCCCCHHHHHHHCCCHHHHHHHHHCCCCEEEECCCCCCCCHHHHHHHHCHHHHHHHCEEEEEECCCCCCCHHHHHHHHHHHCCCCHHHHHHHHHCCCCHHHHHHHHHCCCEEEECCCHHHCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCHHHHCCEEECCCEECCHHHHHCCCEEEECCCCCEEECCCCHHHCCECCCCCCCCCCCCCC
CHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHCCCCCCCCCHHHHHHHHCCCCCCCCCCCCCCCHHHCCCCCEEEEECCCHHHHHHHHHHHHCCCCHCCHEEECCCCCEEECCCCHHHHHCCCCCCHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHCCHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCHHHHHHHHHHCCCCEEEEECCCCCCCHHHHHHHHHHHHHHCCCCEEEEECCCCCHCHHHHHHHHHCCCCCHHHHHHHCCCCCHHHHHHHHHHCCCEEEEECCCHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCCCCCCCCCCHHHHCCCEEEEEEECCCCCCCCCCCCCCEECCCCCCCCCEEECC
> 6rbk_3
HITLDIAGQRSTLGIRRLRVQQLINEIPLAQLELHIPTDNHGAADNAVQHEVSRFTLGVRVGIAQDNKPLFDGYLVQKKMQLKGKEWSVRLEARHALQKLTFLPHSRVFRQQDDSTVMKGLLQSAGVKLTQSKHDQLLQFRLSDWQFIRSRLLSTNCWLLPDAASDTVVIRPLSSRTLARDSHDYTLYEINLNFDNRFTPDSLSLQGWDIAAQRLTAAQKSPAGAFRPWKPAGQDYALAFSMLPEATLQTLSNSWLNYQQMTGVQGHIVLAGTRDFAPGESITLSGFGAGLDGTAMLSGVNQQFDTQYGWRSELVIGLPASMLEPAPPVRSLHIGTVAGFTADPQHLDRIAIHLPALNLPDSLIFARLSKPWASHASGFCFYPEPGDEVVVGFIDSDPRYPMILGALHNPKNTAPFPPDEKNNRKGLIVSQADQTQALMIDTEEKTLRLMAGDNTLTLTGEGNLTMSTPNALQLQADTLGLQADSNLSIAGKQQVEITSAKINM
CEEEEECCECCCCCEEEEEEEECCCCCCEEEEEEECCCCCECHHHHCCHHHHCCCCCCCEEEEEECCEECCCEEEEEEEECCCCCCCEEEEEEECHHHHHCCCCECCCCCCCCHHHHHCCCHHHCCCEEECCCCCCCCCCCECCCCCCCCCCCCCCEECCECCCCCEEECEECCCCCECCCCCCCCEEEEEECCCCCCCCCCCEEEECCCCCCCCCCCECCCCCCCCCEEECCCCCEEEECCCCCCCCHHHHHHHHHHHHCCCCCEEEEECCCCCCCCCCCEEECCCCCCCCEEECEEEEECCEECCCCECCEEEECCCCCCCCCCCCCCCCEEECECCCCCCCCCCCCEEECCCCCCCCCCCEEEEECCCCCCCCCCCCCCCCCCCCEEEEEHHHCCCCEEEEECCCCCCCCCCCCCCCCCCECCCCCECCCCECCEECECCCCCCEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CEEECCCCCCCCHEEEEEEEEHHHCCCCCEEEEEEECCCCCCCCCCCCHHCCCCCEEEEEEEECCCCCEEEEEEEEEEEEEECCCCEEEEEEEEHHHHHHHCCCCEEEECCCCHHHHHHHHHHHCCCCCCCCCHHEEEECCCCCHHHHHHHHHHCCEEEEECCCEEEEECCCCCCCCCEEECCCCCEEEEEEEEHHHHHHHHEEEEEECCCCCEEEEEECCCCCCCCCCCCCHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHEEEEEEEEECCCCCCCCEEEEECCCCCCCCEEEEEEEEEEEECCCCEEEEEEEECCCCCCCCCCCCCCCEEEEEECCCCCCCCCCEEEEEECECCCCCCEEEEEEECCCCCCCCCEEEECCCCCEEEEEECCCCCCCCEEEECCCCCCCCCCCCCCCCCEEEEEEEECCCCCEEEEECCCCCEEEEECCCCEEEEECCCCEEEECCCEEEEECCCEEEEECCCEEEEECCEEEEEECECCC
> 6n9y_1
MERFLRKYNISGDYANATRTFLAISPQWTCSHLKRNCLFNGMCAKQNFERAMIAATDAEEPAKAYRLVELAKEAMYDRETVWLQCFKSFSQPYEEDIEGKMKRCGAQLLEDYRKNGMMDEAVKQSALVNSERVRLDDSLSAMPYIYVPIKEGQIVNPTFISRYRQIAYYFYSPNLADDWIDPNLFGIRGQHNQIKREIERQVNTCPYTGYKGRVLQVMFLPIQLINFLRMDDFAKHFNRYASMAIQQYLRVGYAEEVRYVQQLFGKIPTGEFPLHHMMLMRRDFPTRDRSIVEARVRRSGDENWQSWLLPMIIVREGLDHQDRWEWLIDYMDRKHTCQLCYLKHSKQIPTCGVIDVRASELTGCSPFKTVKIEEHVGNNSVFETKLVRDEQIGRIGDHYYTTNCYTGAEALITTAIHIHRWIRGCGIWNDEGWREGIFMLGRVLLRWELTKAQRSALLRLFCFVCYGYAPRADGTIPDWNNLGSFLDTILKGPELSEDEDERAYATMFEMVRCIITLCYAEKVHFAGFAAPACESGEVINLAARMSQMRMEY
CHHHHHHHCCCHHHHHHHHHHHHHCCCCCCCCCCHHHECCCCECCCCHHHHHHHCCCCCCHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCHHHHHHCCCCCCCCCCCCCCCCEECHHHHCEEECCCCECCCCEEEECCCCEEEEECCCCCCEEECCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCEEEECCCHHHHCCHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHCCCCCCCCCCHHHHCCCCCCCCCCCCHHHHHHCCCCCCCHHHCCEECHHHHHHCCCCCCHHHHHHHHHHCCCCCHHHHHCCCCCCEEEEECHHHHHHCCCCCEEEEEEEEECCCCCCCCCCCCCCEEEEEECCEEEEEECCCCCHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHCCC
CHHHHHHCCCCHHHHHHHHHHHHHCCCCCCCECCCCCEECCEEEHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHCCHHHHHCCCCCCCCCCEEEECCHHHCHHCEEEECCCCECCCEEEEECCCEEEEEECCCCCCCECCCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCEEEEEEEEHHHHHHHCCHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHEECCCCCCHHHHHHHCECCCCCCCCEHHHHHECCCCCCCHEEHHHHHHHHHHHHCCCCCHHHHHHHHCCCCCCHHHHHHHCCCCCEEEEEECCHHHHCCCCHEEEEEEEEECCCCCCHHEEECCCCEEEECCCEEEEEECCCHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHEEEEECCCCHHHEHHHHHHHHHHHCCCCCCCCCCCCCHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHEEECCCHCEECCCCCCCCCCCHHHHHHHHHHHHHCCC
> 6nq1_1
AARWDLCIDQAVVFIEDAIQYRSINHRVDASSMWLYRRYYSNVCQRTLSFTIFLILFLAFIETPSSLTSTADVRYRAAPWEPPAGLTESVEVLCLLVFAADLSVKGYLFGWAHFQKNLWLLGYLVVLVVSLVDWTVSLSLVAHEPLRIRRLLRPFFLLQNSSMMKKTLKCIRWSLPEMASVGLLLAIHLCLFTMFGMLLFAGLTYFQNLPESLTSLLVLLTTANNPDVMIPAYSKNRAYAIFFIVFTVIGSLFLMNLLTAIIYSQFRGYLMKSLQTSLFRRRLGTRAAFEVLSSMVGAVGVKPQNLLQVLQKVQLDSSHKQAMMEKVRSYGSVLLSAEEFQKLFNELDRSVVKEHPPRPEYQSPFLQSAQFLFGHYYFDYLGNLIALANLVSICVFLVLDADVLPAERDDFILGILNCVFIVYYLLEMLLKVFALGLRGYLSYPSNVFDGLLTVVLLVLEISTLAVYRLSLWDMTRMLNMLIVFRFLRIIPSMKPMAVVASTVLGLVQNMRAFGGILVVVYYVFAIIGINLFRGVIVALSAPBGSFEQLEYWANNFDDFAAALVTLWNLMVVNNWQVFLDAYRRYSGPWSKIYFVLWWLVSSVIWVNLFLALILENFLHKW
CHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHCCHHHHHHHHHHHHHHCCHHHHCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHCCCHHHHHHCCCCHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHCCCCCCCCCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCECHHHHHHHHHHCCCCHHHHHHHHHHHHHCCCCCECHHHHHHHHHHHHCCCCCCCCCCCCCCHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCHHHCCCCCHHHHHHHHHHHHCCCCCHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCC
CCCHHHHHHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCECCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHEEEECHHHHHHCHHHHHHHHHHHHHHHHHHHEEEEECCEEEEECHHHHHHHHEHCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHEECCCCCHHHHHHHCCCHHHHHHHHHHHHHHEHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHECCCCHHHHHHHHHHHCCCCCCEECHHHHHHHHHHHCCCCEECCCCCCCECCHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCEEHHHHHHHHHHHHHHHHHHHHEECCHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHECCCECCCCCCCCCHHHHHHHHHCHHHHHHHHHHHHHHHHECCHHHHHHHHHHHCCHHHHHHHHHHHHHHEHHEEHHHHHHHHHHHHHCC
> 6dru_1
TYFAPNSTGLRIQHGFETILIQPFGYDGFRVRAWPFRPPSGNEISFIYDPPIEGYEDTAHGMSYDTATTGTEPRTLRNGNIILRTTGWGGTTAGYRLSFYRVNDDGSETLLTNEYAPLKSLNPRYYYWPGPGAEFSAEFSFSATPDEQIYGTGTQQDHMINKKGSVIDMVNFNSYIPTPVFMSNKGYAFIWNMPAEGRMEFGTLRTRFTAASTTLVDYVIVAAQPGDYDTLQQRISALTGRAPAPPDFSLGYIQSKLRYENQTEVELLAQNFHDRNIPVSMIVIDYQSWAHQGDWALDPRLWPNVAQMSARVKNLTGAEMMASLWPSVADDSVNYAALQANGLLSATRDGPGTTDSWNGSYIRNYDSTNPSARKFLWSMLKKNYYDKGIKNFWIDQADGGALGEAYENNGQSTYIESIPFTLPNVNYAAGTQLSVGKLYPWAHQQAIEEGFRNATDTKEGSAADHVSLSRSGYIGSQRFASMIWSGDTTSVWDTLAVQVASGLSAAATGWGWWTVDAGGFEVDSTVWWSGNIDTPEYRELYVRWLAWTTFLPFMRTHGSRTBYFQDAYTBANEPWSYGASNTPIIVSYIHLRYQLGAYLKSIFNQFHLTGRSIMRPLYMDFEKTDPKISQLVSSNSNYTTQQYMFGPRLLVSPVTLPNVTEWPVYLPQTGQNNTKPWTYWWTNETYAGGQVVKVPAPLQHIPVFHLGSREELLSGNVF
CCCCCCCCCEEEEECCEEEEEEECCCCEEEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCCCEEEEECCEEEEEECCCCCCCCCECEEEEECCCCCEEEEEEECCCCCCCCCEEEECCCCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCCCEEECCCECCEEEEEEEEECCCEEEEECCCCCEEEEECCCEEEEEEEEECCEEEEEEECCCCCHHHHHHHHHHHHCCCCCCCHHHHCEEECCCCCCCHHHHHHHHHHHHHCCCCCCEEEECCCCECCCCCCCECCCCCCCHHHHHHHHHHHHCCEEEEEECCEECCCCCCHHHHHHCCCECEECCCCCCCEEECCEEEEEECCCCHHHHHHHHHHHHHHCHHHCCCEEEECCCCCCCCCCCCCCCCCCHHHHHCCCCCCCEECCCCEHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCEEECCCCCCHHHCCEEEECCCCECCHHHHHHHHHHHHHHHHHCCCCEECCECCCECCCCCCCCCECCCHHHHHHHHHHHHHHCCCCCEEECCCCECCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHCHHHHHHHHHHHHHHCCCCEECCHHHHCCCCCCHHHHHHCCCHHHHCCEEECCCEEECCCCCCCCCEEEEEECCCCCCCCCCEEECCCCCEECCCEEEEEECCCCCCCEEECCCHHHHCCCCCC
CEEEECCCEEEEECCCEEEEEEECCCCEEEEEEECCCCCCCCCHCCCCCCCCCCCCCCCCCCCCEEEECCCCEEEEECCCEEEEEECCCCCCCCCEEEEEECCCCCCHEEHHHHHCCCCCCCHHHHCCCCCCCCEEEEEEEECCCCHHEECCCCCCCCCCCCCCCEEEEHHHHCCCCEEEEECCCCCEEEECCCCCCEEEECCCCEEEEEHHHCCEEEEEEECCCCCHHHHHHHHHHHHCCCCCCCCHHHCHHHHHHHHCCHHHHHHHHHHHHHCCCCEEEEEEEHCHCCCCCCECCCCCCCCCCHHHHHHHHHCCCCEEEEEECCECCCCCCCHHHHHHCCCEEECCCCCCCECCCCCCCEEEECCCCHHHHHHHHHHHHHHHHHHCCCEEEEEHCCCCCCCCCCCCCCCCCCCECCCCCCCHHEEEECCHHHHHCHCHHHHHHHHHHHHHCCCCCCCCCCCCEEEEEEEHHHCHCCHCEEEECCCCCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCECCCCCCCCCCCCCCCCCCEEHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCEHHHHHCCCCCCHHHHCCCCCCCCCCCCEEEECCCEEECCCCCCCCEEEEEEECCCCCCCCCEEEECCCCCEECCCEEEEEECCCCECEEEEECCCCCCCCCCCC
