> 6gnx_2
GYISIDAXKKFLGELHDFIPGTSGYLAYHVQN
CCEEHHHCCCCCCCEEECCCCCCCEEEEECCC
CCCCHHHHHHHHCCECCCCCCCCCHHEEEECC
> 6fxa_1
ENIEETITVMKKLEEPRQKVVLDTAKIQLKEQDEQ
CHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHCC
CCHHHHHHHHHHHHHHHHHEEECCCCHCHHHHHCC
> 5w9f_1
SQETRKKATEMKKKFKNBEVRADESNHCVEVRBSDTKYTLC
CCHHHHHHHHHHHHCCCCEEECCCCCCCEEEECCCCEEEEC
CEEEEECCEEEECHHHHHHHHHHHHHHHHHCCCHCCCCCCC
> 6f45_2
SASIAIGDNDTGLRWGGDGIVQIVANNAIVGGWNSTDIFTEAGKHITSNGNLNQWGGGAIYCRDLNVS
CCEEECCCCCCEEEECCCCCEEEEECCEEEEEECCCEEEECCCCEEEECCCEEECCCCCEEECCEEEC
CCCEEECCCCCCEECCCCCEEEEEHHCCEEEEECCCCEEEECCCEEECCCCEECCCCCCEEEEECCCC
> 6qek_1
DIYGDEITAVVSKIENVKGISQLKTRHIGQKIWAELNILVDPDSTIVQGETIASRVKKALTEQIRDIERVVVHFEPAR
CHHHHHHHHHHCCCCCCCEEEEEEEEEECCEEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCCCEEEEEEEEEECC
CCCHHHHHHHHHCCCCCCCHHHHEHHHCCCEEEEEEEEECCCCCEEHHHHHHHHHHHHHHHHHCCCHCEEEEECCCCC
> 6qfj_1
SHMEDYIEAIANVLEKTPSISDVKDIIARELGQVLEFEIDLYVPPDITVTTGERIKKEVNQIIKEIVDRKSTVKVRLFAAQEEL
CHHHHHHHHHHHHHHCCCCCCEEEEEEEEEECCEEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEEEEECCCEC
CCHHHHHHHHHHHHHCCCCHEECHHHHHHHCCCEEEEEEEEECCCCCCEHHHHHHHHHHHHHHHHHCCCCCEEEEEECCCCCCC
> 6btc_1
XNKKSKQQEKLYNFIIAKSFQQPVGSTFTYGELRKKYNVVCSTNDQREVGRRFAYWIKYTPGLPFKIVGTKNGSLLYQKIGINPC
CCCCCHHHHHHHHHHHHHHHHCCCCCEECHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHCCCCCEEEEEEECCEEEEEECCCCCC
CCCCHHHHHHHHHHHHHEEECCCCCCEEEEECCCCECCCCCCCHHHHHHHHHHHHHHHCCCCCCEEEECCCCCCEEEEECCCCCC
> 6d7y_1
LDTAQAPYKGSTVIGHALSKHAGRHPEIWGKVKGSXSGWNEQAXKHFKEIVRAPGEFRPTXNEKGITFLEKRLIDGRGVRLNLDGTFKGFID
CCCCCCEECCEEHHHHHHHHHHHHCHHHHCCCCCCHHHHHHHHHHHHHHHHHCCCCCEEEECCCCCEEEEEECCCCCEEEEECCCCEEEEEC
CCCCCCCCCCCCEHHHHHHHHCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHCCCCCEEEEECCCCCEEEEEECCCCCEEEECCCCCEEEEEC
> 6g57_1
KRSGFLTLGYRGSYVARIMVCGRIALAKEVFGDTLNESRDEKYTSRFYLKFTYLEQAFDRLSEAGFHMVACNSSGTAAFYRDDKIWSSYTEYIFFRP
CCCEEEEEEEEEECCCEEEEEEEHHHHHHHHHHHCCECCCCCCEEEEECCCCCHHHHHHHHHHCCCEEEEEEEEEEEECCCCCCEEEEEEEEEEEEC
CCCCEEEEEECCCEEEEEEEECCEEHHHHHCCCHHCCCCCCCCCCEEEECCCHHHHHHHHHHHHCCEEEEECCCCCCCCCCHHHHCCCCEEEEEECC
> 6msp_1
MGHHHHHHENLYFQSHMTDELLERLRQLFEELHERGTEIVVEVHINGERDEIRVRNISKEELKKLLERIREKIEREGSSEVEVNVHSGGQTWTFNEK
CCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHCCCCCEEEEEEEECCEEEEEEEECCCHHHHHHHHHHHHHHHHHCCCCCEEEEEEECCEEEEEECC
CCHHHHHHHHHHHHHHHECCCCECCCCCHHHHHHCCCCCCCCCCCHCHHHCCCCCCCCCCECCCCCCCCCCCCHCCCCCCCCCCCCCCCCHHEEECC
> 6gnx_1
SLKPFTYPFPETRFLHAGPNVYKFKIRYGNKEVITQELEDSVRVVLGNLDNLQPFATEHFIVFPYKSKWERVSHLKFKHGEIILIPYPFVFTLYVEXKW
CCCCCCCCCCEEEEEEECCEEEEEEEEECCHHHHHHHHHHHHHHHHHCCCCCCCEECCCEEEEEEEEECCCCCCEEEEECCEEEEEEEEEEEEEEEECC
CCCCCCCCCCCEEEEECCCEEEEEEEECCCCCCEHHHHHHHHEEEHCCCCCCCCCCCCCEEECCCCCCCCCCCCCEEECCCCECCCCCEEEEEEEECCC
> 6cp9_2
XFIENKPGEIELLSFFESEPVSFERDNISFLYTAKNKAGLSVDFSFSVVEGWIQYTVRLHENEILHNSIDGVSSFSIRNDNLGDYIYAEIITKELINKIEIRIRPDIKIKSSSVI
CCCCECCCHHHHHHHHCCCCCEEECCCCEEEEEEECCCCEEEEEEEECCCCEEEEEEEECCEEEEEEEEECCCECEEEEECCEEEEEEEEECCCCEEEEEEECCCCCEEEEEEEC
CCHCCCCCHHHHHHHCCCCCEEECCCCCCEEEEECCCCCEEEEEEECCHHCHEEEEEEECCCEEEEEEHCCCCEEEEECCCCCCEEEEEEECCCCEEEEEEEECCCEEEEECECC
> 6cp9_1
TAQTIANSVVDAKKFDYLFGKATGNSHTLDRTNQLALEXKRLGVADDINGHAVLAEHFTQATKDSNNIVKKYTDQYGSFEIRESFFIGPSGKATVFESTFEVXKDGSHRFITTIPKNG
CHHHHHCCCCCHHHHHHHCCCCCCECCECCCHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHCCCHHHEEEEEECCCCEEEEEEEEEECCCCCEEEEEEEEEECCCCCEEEEEEEEECC
CHHCCCEEEECCCCCHHEECCCCCCCCCHHHHHHHHHHHHECCCECCHCCHHHHHHHHHHHHCCCCCEEEEECCCCCCEEEEEEEEECCCCCCEEEEEEEECCCCCCCEEEEEECCCC
> 6d34_1
PASPNIEAILASYAGFRDRDIEGILSGMHPDVEWVHPEGMGKYGLGGTKLGHAGIKEFLAHVPTVLGGMRLAPREFIEQGDRVVVFGTREVTSLRGTTATLDFVHSWTMRDGKATRMEDIFDTVAFHELIES
CCCHHHHHHHHHHHHHHCCCHHHHHCCEEEEEEEEECHHHHHHCCCEEEEHHHHHHHHHHHHHHCEEEEEEEEEEEEEECCEEEEEEEEEEEECCCCEEEEEEEEEEEEECCEEEEEEEECCHHHHHHHHCC
CCCCCHHHHHHHHHHHHHCCHHHHHHHCCCCEEEEECCCCCCCCCCCCECCHHHHHHHHHHHHHHCCCCEECCEEEECCCCEEEEEEEEEEEECCCCCEEEEEEEEEEECCCCEEEEEHHHHHHHHHHHHCC
> 6rap_1
AITADDIAVQYPIPTYRFIVTLGDEQVPFTSASGLDINFDTIEYRDGTGNWFKMPGQRQAPNITLSKGVFPGKNAMYEWINAIQLNQVEKKDIMISLTNEAGTEVLVSWNVSNAFPTSLTSPSFDATSNEIAVQQITLMADRVTIQTA
CCCCCCHHHHCCCCCCCEEEEECCEECCCCEEECCCCCCCEEEEECCCCCEEEEECCCCCCEEEEEEECCCCCCCCHHHHCCCECCECCCEEEEEEEECCCCCCEEEEEEEEEEEEEEEECCCCCCCCCCCCEEEEEEECCCEEEEEC
CCCHHHCCCCCCCCCCEEEEEECCCCCEEEEECCCCCEEEEEEEECCCCCCEECCCCECCCCEEEEEEEECCCHHHHHHHHHHHCCCECCCCEEEEEECCCCCCEEEEEEEECCCEEEECCCCCCCCCCHEEEHHHHHHHHCEEEECC
> 6d7y_2
XKELFEVIFEGVNTSRLFFLLKEIESKSDRIFDFNFSEDFFSSNVNVFSELLIDSFLGFNGDLYFGVSXEGFSVKDGLKLPVVLLRVLKYEGGVDVGLCFYXNDFNSAGKVXLEFQKYXNGISADFGFENFYGGLEPASDQETRFFTNNRLGPLL
CCEEEEEEEECCCCCCHHHHHHHHHCCCCEEEEEEECCCCCCCCCCCCCHHHHHHHHHCCCCEEEEEEEEEEEEECCEEEEEEEEEEEEECCEEEEEEEEEECCHHHHHHHHHHHHHHHHHHHHHHCCCEEEEEECCCCCHHHEEEECCEECCCC
CHHHHEEEEHCCCHHCHHHHHHHHCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHCCCCCCEEEEEEECCCCECCEEECCEEEEEEEECCCCEEEEEECCCCCCCCHHHHHHHHHHHHHHHHHHCCCCHEECCCCCCCCCCEEEECCCCCCCCC
> 6cp8_3
VNSTAKDIEGLESYLANGYVEANSFNDPEDDALECLSNLLVKDSRGGLSFCKKILNSNNIDGVFIKGSALNFLLLSEQWSYAFEYLTSNADNITLAELEKALFYFYCAKNETDPYPVPEGLFKKLXKRYEELKNDPDAKFYHLHETYDDFSKAYPLNN
CCCHHHHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHCHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHCCCCCHHHHCHHHHHHHHHHHCCCCC
CCCHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHCCCHHHHHHHHHHHCCCCCCCHHHHHHHHHHHCCCCCHHHHHHHHHHHHHCCCHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHCCCCCCHHHHHHCHHHHHHHHHCCCCC
> 6cp8_1
NSFEVSSLPDANGKNHITAVKGDAKIPVDKIELYXRGKASGDLDSLQAEYNSLKDARISSQKEFAKDPNNAKRXEVLEKQIHNIERSQDXARVLEQAGIVNTASNNSXIXDKLLDSAQGATSANRKTSVVVSGPNGNVRIYATWTILPDGTKRLSTVTGTFK
CCCEEEEEECCCCCEEEEEEECCEEEECHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHCCCCECCECEEEEEEEECCEEEEEEEEEEECCCCCEEEEEEECCCC
CCCCEEEECCCCCCCEEEEECCCCEECCCEEEEEECCCCCCCHHHHHHHHHHHHHHHHHCHHHHHHCCCCHHHHHHHHCCCCCCHHHHHHHHHHHHCCCECCCCCHHHHHHHHHHHHCCCCCCCCEEEEEEECCCCCEEEEEEEEECCCCCCEEEEEECCCC
> 6e4b_1
AMRLWLIRHGETQANIDGLYSGHAPTPLTARGIEQAQNLHTLLHGVSFDLVLCSELERAQHTARLVLSDRQLPVQIIPELNEMFFGDWEMRHHRDLMQEDAENYSAWCNDWQHAIPTNGEGFQAFSQRVERFIARLSEFQHYQNILVVSHQGVLSLLIARLIGMPAEAMWHFRVDQGCWSAIDINQKFATLRVLNSRAIGVEN
CEEEEEEECCCEHHHHHCECCCCCCCCECHHHHHHHHHHHHHCCCCCCCEEEECCCHHHHHHHHHHCCCCCCCEEECHHHCCCCCHHHCCCEHHHHHHHCHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECHHHHHHHHHHHCCCCHHHHHHECCCCCCEEEEEEECCEEEEEEEEECCCCCCC
CCHEEEEECCCCHHHHHCCECCCCCCCCCHHHHHHHHHHHHHHHCCCCEEEECCCCHHHHHHHHHHHHCCCCCEECCHHHHHCCCCCECCCCHHHHHHHCHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCHHHHHHHHHHCCCHHHHHEECCCCCEEEEEEECCCCCEEEEECCCCEEECC
> 6rbk_1
SLLERGLSKLTLNAWKDREGKIPAGSMSAMYNPETIQLDYQTRFDTEDTINTASQSNRYVISEPVGLNLTLLFDSQMPGNTTPIETQLAMLKSLCAVDAATGSPYFLRITWGKMRWENKGWFAGRARDLSVTYTLFDRDATPLRATVQLSLVADESFVIQQSLKTQSAPDRALVSVPDLASLPLLALSAGGVLASSVDYLSLAWDNDLDNLDDFQTGDFLRAT
CCCCCCCCCCEEEEECCCCCCCEEEEEECCCCCCCEEEEEEEEEEECCCCCCCCCCEEEEEEEEEEEEEEECCECCCCCCCCCHHHHHHHHHHHCCCCCCCCCCCCCEEECCCCCCCCCCEECCCEEECCCCCCCECCCCCECECCEEEEECCCCCCCHHHHHCCCCCCCCCCEECCCCCCHHHHHHHHHHHCCCCCCHHHHHHHCCCCCCCCCCCCCEECCC
CHHHHHHHHEEEEECCCCCCCCCCCEEEEEECCCCEEEEEEEEECCCCCCCCCCCCCEEECCCCCEEEEEEEECCCCCCCCCCHHHHHHHHHHEECCCCCCCCCCEEEEEECCECCCCCCCEEEEEEEEEEEEEEECCCCCEEEEEEEEEEEECCCCHHHHCCCCCCCCCCCEEEEECCCCCHHHHHHHHHHHCCCHHHHHHHHHHCCCCCCCCCCCCEEEEC
> 6f45_1
AVQGPWVGSSYVAETGQNWASLAANELRVTERPFWISSFIGRSKEEIWEWTGENHSFNKDWLIGELRNRGGTPVVINIRAHQVSYTPGAPLFEFPGDLPNAYITLNIYADIYGRGGTGGVAYLGGNPGGDCIHNWIGNRLRINNQGWICGGGGGGGGFRVGHTEAGGGGGRPLGAGGVSSLNLNGDNATLGAPGRGYQLGNDYAGNGGDVGNPGSASSAEMGGGAAGRAVVGTSPQWINVGNIAGSWL
CCCCCCHHHHHHHHHCCCEHHHHHHHCCCCCCCEEHHHHCCCCCCEEEEECCCEEEECHHHHHHHHHHCCCCCEEEEECCCEEECCCCCCCEEECCCCCCCCEEEEECCEEECCCCCCCECCCCCCCCCCCEEECCHHHEEEEECCEEECCCCCCCCEEECCEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEECCEECCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCEEEECCEEECCCC
CECCCCCCCCCCCCCCCCHHHHHHHHEEEECCCCHHHHHCCCCCCCEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
> 6mxv_1
IXQHSSGFLKLVDDAKSRIQECSVDDIQKXNETQTLDGLLIDTREESEVANGYIPNAIHLSKGIIESAIESAVPNKNQKXYFYCGGGFRSALVADKLREXGYKNVISVDGGWRAWNAKGYPTVSPNQFRPNEFLKLVNNAKTQIKECSTTELYNKINSQELDGIVFDVREDSEFNRFHIQGATHLSKGQIEVKIENLVPNKQQKIYLYCGSGFRSALAAESLQHXGYTNVVSIAGGIKDWLANNYPVSQN
CCCCCHHHHHHHHHHHCCCEEECHHHHHHHHHHCCCCCEEEECCCHHHHCCCECCCEEECCCCCHHHHHHHHCCCCCCCEEEECCCCCHHHHHHHHHHHCCCCCEEEECCHHHHHHHCCCCCECHHHCCCHHHHHHHHHHHHHCEEECHHHHHHHHHCCCCCCEEEECCCHHHHHHCECCCCEECCCCCHHHHHHHHCCCCCCCEEEECCCCHHHHHHHHHHHHCCCCCEEEECCHHHHHHHCCCCCECC
CCCCCCCHHHHHHHHHCCCCCCCHHHHHHHHHHCCCCEEEEEECCHHHHCCCCCCCEEECCCCCEEEEHHHCCCCCCCEEEEEECCCCHHHHHHHHHHHCCCCEEEEHHCHHHHHHHCCCCECCCCCCCCHHHHHHHHHHCCCCCECCHHHHHHHHHCCCCCEEEEECCCCHHHHHCCCCCCCCCCCCHHHHHHHHCCCCCCCEEEEEECCCCCHHHHHHHHHHCCCCEEEHHHCCHHHHHHCCCCCCCC
> 5z82_1
SIGLAHNVTILGSGETTVVLGHGYGTDQSVWKLLVPYLVDDYKVLLYDHMGAGTTNPDYFDFDRYSSLEGYSYDLIAILEEFQVSKCIYVGHSMSSMAAAVASIFRPDLFHKLVMISPTPRLINTEEYYGGFEQKVMDETLRSLDENFKSLSLGTAPLLLACDLESAAMQEYCRTLFNMRPDIACCITRMICGLDLRPYLGHVTVPCHIIQSSNDIMVPVAVGEYLRKNLGGPSVVEVMPTEGHLPHLSMPEVTIPVVLRHIRQDITD
CHHHHCCCEEEECCCCEEEEECCCCCCHHHHCCCHHHCCCCCEEEEECCCCCCCCCHHHCCCCHHHCCHHHHHHHHHHHHHCCCCCEEEEEECHHHHHHHHHHHHCHHHEEEEEEECCCCCCECCCCCCCCECHHHHHHHCCCHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHCCHHHHHHHHHHHHCCCCHHHHHHCCCCEEEEEEEECCCCCHHHHHHHHHHCCCCEEEEEEEEECCCHHHHCHHHHHHHHHHHHHCCCCC
CHHHHCCEEEECCCCCEEEEECCCCCHHHHHHHECHHHHHCCEEEEEECCCCCCCCCCCCCCCHCCCCCCHHHHHHHHHHHHCCCCEEEEECCHHHHHHHHHHHCCHHHHHHEEEECCCCCECCCCCCCCCCCHHHHHHHHHHHHHCHHHHHHHCCHHEECCCCCHHHHHHHHHHHCCCCCHHHHHHHHEEECCCCHHCCCCCCCCEEEECCCCCCECCEHHHHHHHHHCCCCCEEEEECCCCCCCCCCCCHHHHHHHHHHHHHHHCC
> 6rap_5
SNYQTLVDVNNAMNKMLRAYVNEAVAIRFDLPDTQADAAISVFLYDIHEDLQLRTAESRGFNAGAGRLLPGWVNVKCNYLITYWESPDSQPDNQAIQVMSQVLAALINNRQLADIGAYTQVMPPKENLNSLGNFWQSLGNRPRLSLNYCVTVPISLSDKGEEMTPVKSLSTTVEPKAPLSPLVITDALREQLRVALDACLAMTHVNLDSSPVANSDGSAAEIRVSLRVYGMTPTEYLAPMNTVFNEWEKSEAAAVTPDGYRVYINAVDKTDLTGI
CCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCEEECCCCECHHHCCCCCCCCCCCCCCCCCCCEEEECCEEECCCCCCCCCCCCCCCCCHHHHHHHHHHCCCCCCCCCEEECCCCCCCHHHHHHHHHHCCCCCCCCEEEEEEEEECCCCCCCCCCECCCEEEEEEECCCCCHHHHHHHHHHHHHHHCCHHHHHHHECCEEEEECCCCCCCCCCEEEEECEECCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCCEECCCCCCECCCC
CCCHHHHHHHHHHHHHHHHCCCCCEEEEECCCCCCCCCEEEEEEECHHHCCCHCCCCCCCCCCCCCCCCCCCHHCCHEEEEEECCCCCCCCCHHHHHHHHHHHHHHHCCCCCCHCCCCCEECCCCCCCCCHHHHHHHHCCCCCCEEEEEEEEEECCCCCCCCCCCEEEEEEECCCCCCCCCHHHHHHHHHHHHHHHHHHHHEEEECCCCCCCCCCCCCCCCCEEEEEECCCCCHHHHHHHHHHHHHHCCCCEEEEEECCCEEEEEECCCCCCCCC
> 6d2v_1
GLVPRGSHMEGKKILVTGGTGQVARPVAEALAERNEVWCLGRFGTPGVEKELNDRGITTFHWDMDDPGAAAYEGLPDDFTHVLHSAVRRGEDGDVNAAVEVNSVACGRLMTHCRGAEAFLFVSTGALYKRQTLDHAYTEDDPVDGVADWLPAYPVGKIAAEGAVRAFAQVLNLPTTIARLNIAYGPGGYGGVPMLYFKRMLAGEPIPVPKEGQNWCSLLHTDDLVAHVPRLWEAAATPATLVNWGGDEAVGITDCVRYLEELTGVRARLVPSEVTRETYRFDPTRRREITGPCRVPWREGVRRTLQALHPEHLPS
CCCCHHHHCCCCEEEEECCCCCCHHHHHHHHHCCCEEEEEECCCCCCHHHHHHHCCCEEEECCCCCCCHHHHCCCCCCCCEEEECCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEEEEEHHHECCCCCCCEECCCCCECCCCCCCCHHHHHHHHHHHHHHHHHHHHCCCEEEEEECCEECCCCCCEHHHHHHHHHHHCCCEEEECCCCCEECCEEHHHHHHHHHHHHHCCECCCEEEEECCCCCEEHHHHHHHHHHHHCCCCCEEEECCCCCCCEECCHHHHHHHCCCCCCHHHHHHHHHHHHCHHHCCC
CCCCCCCCCCCCEEEEECCCCCEEHHHHHHHHHCCCEEEEECCCCHHHHHHHHCCCCCEEEECHCHHHHHHHHCCCCCCCEEEEEHHHCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHEEEECCCCECCCCCCCCCCCECCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCHHHHEEEEECCCCCCCCCHHHHHHHHHCCCCEEECCCCCCCCCCCEEEHHHHHHHHHHCCCCCCCEEEEECCCCCHHHHHHHHHHHHHCCCCCEECCCCCCCCCCCCHHHHHHHCCCCCCCCHHHHHHHHHHHHCHCCCCC
> 6q64_1
LIPLIEAQTEEDLTPTXREYFAQIREYRKTPHVKGFGWFGNWTGKGNNAQNYLKXLPDSVDFVSLWGTRGYLSDEQKADLKFFQEVKGGKALLCWIIQDLGDQLTPKGLNATQYWVEEKGQGNFIEGVKAYANAICDSIEKYNLDGFDIDYQPGYGHSGTLANYQTISPSGNNKXQVFIETLSARLRPAGRXLVXDGQPDLLSTETSKLVDHYIYQAYWESSTSSVIYKINKPNLDDWERKTIITVEFEQGWKTGGITYYTSVRPELNSXEGNQILDYATLDLPSGKRIGGIGTYHXEYDYPNDPPYKWLRKALYFGNQVYPGKFD
CCCHHHCCCHHHCCHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCCCHHHCHHHCCCCCCEEECCCCCCCCCHHHHHHHHHHHHCCCCEEEEEEEECCCCCCCCCCCCCHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCCCCCCCCCCCECCCCCHHHHHHHHHHHHHHCCCCCEEEEEECHHHECHHHHCCCCCEEEECCCCCEHHHHHHHHCCCCCCCHHHHEEEEEEHHHHCCCCCCCCCECCCHHHHHCCCHHHHHHHCCECCCCCECCEEEEECHHHHCCCCCCCHHHHHHHHHHHHHCCCCCC
CCCCCCCCCHHCCCHHHHHHHHHHHHHHCCCCCEEEEEECCCCCCCCCCCCCHCCCCCCCEEEEECCCCCCCCHHHHHHHHHHHHCCCCEEEEEEHHHHCCCCCCCCCCCCHHCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCCEEEEECCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCCEEEECCCCCCCCHHHHHHHHEEEEECCCCCCCCCCCCCCCCCCCCCCCCCEEEEECHHHHHCCCCCCCCCCCCCCCCCCCCHHHHHHHHCCCCCCCCCCCEEEECHCHCCCCCCCCHHHHHHHHHCCCCCCCCCC
> 6n91_1
MITSSLPLTDLHRHLDGNIRTQTILELGQKFGVKLPANTLQTLTPYVQIVEAEPSLVAFLSKLDWGVAVLGDLDACRRVAYENVEDALNARIDYAELRFSPYYMAMKHSLPVTGVVEAVVDGVRAGVRDFGIQANLIGIMSRTFGTDACQQELDAILSQKNHIVAVDLAGDELGQPGDRFIQHFKQVRDAGLHVTVHAGEAAGPESMWQAIRDLGATRIGHGVKAIHDPKLMDYLAQHRIGIESCLTSNLQTSTVDSLATHPLKRFLEHGILACINTDDPAVEGIELPYEYEVAAPQAGLSQEQIRQAQLNGLELAFLSDSEKKALLAKAALRG
CCCCCCCCEEEEEEHHHCCCHHHHHHHHHHHCCCCCCCCHHHHHHHHCCCCCCCCHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHCCCCEEEEEECHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHCCEEEEEEEEEHHHCHHHHHHHHHHHHCCHHHCCEEEEECCCCCCCHHHHHHHHHHHHHCCCEEEEEECCCCCHHHHHHHHHHCCCCEEEECHHHHHCHHHHHHHHHHCCEEEECHHHHHHCCCCCCHHHCCHHHHHHCCCCEEECCECHHHHCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHCCC
CCHCCCCHHHEEEECCCCCCCHHHHHHHHHCCCCCCCCCHHHHHHHEEECCCCCCHHHHHHHHCCCEEECCCHHHHHHHHHHHHHHHHHHCCEEEEEEECHHHHHHCCCCCHHHHHHHHHHHHHHHHHHCCCEEEEEEECCCCCCHHHHHHHHHHHHHCCCCEEEEECCCCCCCCCCHHHHHHHHHHHHCCCCEEEECCCCCCCHHHHHHHHHHCHEEECCCEEHHCCHHHHHHHHHCCCCEEECCCCCHEECEEHHHHHCHHHHHHHCCCEEEEECCCCCCCCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHCCC
> 6cvz_1
KHKYHFQKTFTVSQAGNCRIMAYCDALSCLVISQPSPQASFLPGFGVKMLSTANMKSQYIPMHGKQIRGLAFSSYLRGLLLSASLDNTIKLTSLETNTVVQTYNAGRPVWSCCWCLDEANYIYAGLANGSILVYDVRNTSSHVQELVAQKARCPLVSLSYMPRAASAAFPYGGVLAGTLEDASFWEQKMDFSHWPHVLPLEPGGAIDFQTENSSRHCLVTYRPDKNHTTIRSVLMEMSYRLDDTGNPICSCQPVHTFFGGPTAKLTKNAIFQSPENDGNILVCTGANSALLWDAASGSLLQDLQTDQPVLDICPFEVNRNSYLATLTEKMVHIYKWE
CCCEEEEEEEECCCCCCCCCEEEECCCCEEEEEEECCCCCCCCCEEEEEEECCCCCCEEEEEECCCEEEEEECCCCCCEEEEEECCCEEEEEECCCCEEEEEEECCCCEEEEEECCCCCCEEEEEECCCCEEEEECCCCCCCCEEECCCCCCCCEEEEEEECCCCCCCCCCCEEEEEECCEEEEEEECCCCCEEEEECCCCEEEEEEEEEECCCCEEEEEEEECCCCCCCEEEEEEEEEEECCCCCEEEEEEEEEEEECCCCCCCCECEEEECCCCCCCEEEEECCCEEEEEECCCCCEEEEEECCCCCCEEEEEEECCEEEEEEECCCEEEEEEEC
CCEEEEEEEEEECCCCCCEEEEECCCCCEEEEECCCCCCCCCCCCCEEEECCCCCCCEEECCCCCCCCEEEECCCCCCHEEEHHCCCCEEEEECCCCCEEEEEECCCCCCEEECCCCCCCEEEEECCCCEEEEEECCCCCCCCHEECCCCCCCCEEEEECCCCCCCCCCCCCCEEEECCCCCEEEECCCCCCCCCCCCCCCCCCCEEEECCCCCCCEEEEECCCCCCCCCCHHHCCCCCCCCCCCCCCCCCCEEEEECCCCCCECCCCEEECCCCCCCEEEEEECCCCEEEEECCCCCEEEECCCCCCEEEEEECCCCCCCEEEEEHHHHEEEEECC
> 6ek4_1
VVYPEINVKTLSQAVKNIWRLSHQQKSGIEIIQEKTLRISLYSRDLDEAARASVPQLQTVLRQLPPQDYFLTLTEIDTELEDPELDDETRNTLLEARSEHIRNLKKDVKGVIRSLRKEANLMASRIADVSNVVILERLESSLKEEQERKAEIQADIAQQEKNKAKLVVDRNKIIESQDVIRQYNLADMFKDYIPNISDLDKLDLANPKKELIKQAIKQGVEIAKKILGNISKGLKYIELADARAKLDERINQINKDCDDLKIQLKGVEQRIAGIEDVHQIDKERTTLLLQAAKLEQAWNIFAKQLQNTIDGKIDQQDLTKIIHKQLDFLDDLALQYHSMLLS
CCCCCCCHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHCCCECCCCCHHHHCCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCCEHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHCCCCC
CCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCC
> 6rap_3
TTTYPGVYLSEDAVSSFSVNSAATAVPLFAYDSENTNTINKPIQVFRNWAEFTVEYPTPLEDAFYTSLSLWFMHGGGKCYLVNEANIADAVAQYDDITLIVAAGTDTTTYTAFTTVVGQGYRIFGLFDGPKEKIAGTAKPDEVMEEYPTSPFGAVFYPWGTLASGAAVPPSAIAAASITQTDRTRGVWKAPANQAVNGVTPAFAVSDDFQGKYNQGKALNMIRTFSGQGTVVWGARTLEDSDNWRYIPVRRLFNAVERDIQKSLNKLVFEPNSQPTWQRVKAAVDSYLHSLWQQGALAGNTPADAWFVQVGKDLTMTQEEINQGKMIIKIGLAAVRPAEFIILQFSQDI
CCCCCCCCCCCCCCCCCCCCCCCCCCCEEEECCCCCCCCCCCEEEECCHHHHHHHCCCCCCCCCHHHHHHHHCCCCCCEEEEECCCHHHHHHHHCCCCCEEECCCCCCHHHHHHHHHHCCCCCCEECCCCCCCCCCCCCHHHCCCCCCCCCCEECCCCCECCCCCCCECHHHHCCHHHHHHHCCCCCCCCCCCCECCCCCECCCCCHHHHCCCCCCCCCCEEECCCCCCCEEECCCCCCCCCCCCCHHHHHHHHHHHHHCCCCCHHHCCCECCHHHHHHHHHHHHHHHHHHHHCCCECCCCHHHHEEEECCCCCCCCHHHHCCCCEEEEEEECECCEECEEEEEEECCC
CCCCCCEEEEECCCCCCCCCCCCCCEEEEEECCCCCCCCCCCEEEECCHHHHHHHCCCCCCHHHHHHHHHHHHCCCCCEEEEECCCCHHHHHHHCCCEEEECCCCCHHHHHHHHHHHHHHCCEEEEECCCCCCCCCCCCHHHHCCCCCCCHHHHHHCHHHHCCCCEECCCCHHHHEEEECCCCCCCEEECCCCEEECCCCCEEECCCHHHCCCCCCCCEEEEECCCCCCEEEEEEEECCCCCCEEEEEEHHHHHHHHHHHHHHCCEEEECCCCCHHHHHHHHHHHHHHHHHHHHCHHCCCCHHHHEEEEECCCCCCCHHHHHCCCEEEEEEEECCCCCEEEEEEHHCCC
> 6cci_1
VELPPEEADLFTGEWVFDNETHPLYKEDQBEFLTAQVTCMRNGRRDSLYQNWRWQPRDASLPKFKAKLLLEKLRNKRMMFVGDSLNRNQWESMVBLVQSVVPPGRKSLNKTGSLSVFRVEDYNATVEFYWAPFLVESNSDDPNMHSILNRIIMPESIEKHGVNWKGVDFLVFNTYIWWMNTFAMKVLRGSFDKGDTEYEEIERPVAYRRVMRTWGDWVERNIDPLRTTVFFASMSPLHIKSLDWENPDGIKDALETTPILNMSMPFSVGTDYRLFSVAENVTHSLNVPVYFLNITKLSEYRKDAHTSVHTIRQGKMLTPEQQADPNTYADDIHWCLPGLPDTWNEFLYTRIISR
CCCCCCCCCCCCEEEEECCCCCCCCCHHHCCCCCCCCCCCCCCCCCCHHHHEEEEECCCCCCCCCHHHHHHHCCCCEEEEEECHHHHHHHHHHHHHHHCCCCCCCEEEEEECCEEEEEECCCCEEEEEEECCCCCCECCCCCCCCCCCCCEECCCCCHHHHHHHCCCCEEEECCCHHHCCCCEEEEECCCHHHCCCCEEEEEHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHCCCCEEEECCHHHHHCCCCCEEECCCEECCEECCHHHHCCHHHHCEEEEECCCCHHHHHHHHHHHHHHCC
CCCCCCCCCCCCCEEEECCCCCCCCCCCCCCCCCCHHHHHHCCCCCCHHHCCECCCCCCCCCCCCHHHHHHHHHCCEEEEECCCCCCCCEEEEHEHHHCCCCCCCCCCCCCCCEEEEEEEHCCCEEEEHECCEEEECCCCCCCCCCCCCHEECCCCHHHHHHCCCCCEEEEEEEEEHHECCCCEEEEECCCCCCCCEEEEECHHHHHHHHHHHHHHHHHHCCCCCCCEEEEEECCCCCCCCCCCCCCCCCCECCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHCCCCCEEEEEEHHHHHHHCCCCCCEEEEHCCCCCCHHHCCCCCCCHHHHHHECCCCCCHHHHHHHHHHHCC
> 6cl6_1
ALAATDIPGLDASKLVSGVLAEQRLPVFARGLATAVSNSSDPNTATVPLMLTNHANGPVAGRYFYIQSMFYPDQNGNASQIATSYNATSEMYVRVSYAANPSIREWLPWQRCDIGGSFTKEADGELPGGVNLDSMVTSGWWSQSFTAQAASGANYPIVRAGLLHVYAASSNFIYQTYQAYDGESFYFRCRHSNTWFPWRRMWHGGDFNPSDYLLKSGFYWNALPGKPATFPPSAHNHDVGQLTSGILPLARGGVGSNTAAGARSTIGAGVPATASLGASGWWRDNDTGLIRQWGQVTCPADADASITFPIPFPTLCLGGYANQTSAFHPGTDASTGFRGATTTTAVIRNGYFAQAVLSWEAFGR
CCCHHHCCCEEHHHEEECCCCHHHCCHHHHCECCCCCCCCCHHHCCCCEEEECCCCCCCCCCCEEEEEEEECCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCECCCEECCHHHECECCCCEECCCCCEHHHCCCCEEEEECCHHHHHHCECCCCCCCEEEEEEEEECCEEEEEEEECCCCCEEEEEEECCEECCCEEECECCCCCHHHECEHHHCCHHHCCCCCCCECECCCCEEHHHEEECCCCHHHCCCCCCCHHHHHHHHCCCCCCEEECCCCEEEEECCCCEEEEEEEEEECCCEEEEEECCCCCCCCEEEEEEEECCCCCCCCCCCEEEECCCCCEEEEEECCCCCEEEEEEEEEC
CHHHCCCHHHHHHHHHCCCCCHCCCCHHHHCHHHHHCCCCCCCCHCCCEEEECCCCCCCCCCCCCHHEECCCCCCCCCHHEEEEECCCCHHHHHHHHCCCCCCCCCCCCHHHCCCCCCCCCCHCCCCCCCCHHHHCCCCEEECCCCHHHCCCCCCCCCHHHEEEEECCCCCCEEEEEEECCCCCEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCEEEEECEEECCCCCCCCEEEEECCCCCCEEEEEEECCCCCCCCCCCEEECCCCCCEEEEECCCCCCCCEEEEEECC
> 6cl5_1
ALAATDIPGLDASKLVSGVLAEQRLPVFARGLATAVSNSSDPNTATVPLMLTNHANGPVAGRYFYIQSMFYPDQNGNASQIATSYNATSEMYVRVSYAANPSIREWLPWQRCDIGGSFTKTTDGSIGNGVNINSFVNSGWWLQSTSEWAAGGANYPVGLAGLLIVYRAHADHIYQTYVTLNGSTYSRCCYAGSWRPWRQNWDDGNFDPASYLPKAGFTWAALPGKPATFPPSGHNHDTSQITSGILPLARGGLGANTAAGARNNIGAGVPATASRALNGWWKDNDTGLIVQWMQVNVGDHPGGIIDRTLTFPIAFPSACLHVVPTVKEVGRPATSASTVTVADVSVSNTGCVIVSSEYYGLAQNYGIRVMAIGY
CCCHHHCCCEEHHHEEECCCCHHHCCHHHHCECCCCCCCCCHHHCCCCEEEECCCCCCCCCCCEEEEEEEECCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCECCCEECCHHHECECCCCEECCCCCEHHHCCCCEEEEECCHHHHHCCECCCCCCCEEEEEEEEECCEEEEEEEECCCCEEEEEEECCEECCCEEECECCCCCHHHECEHHHCCCCCCCCCCCCECECCCCEEHHHEEECCCCHHHCCCCCCCHHHHHHHHCCCCCCEEECCCCEEEEECCCCEEEEEEEEEEECCCCCCEEEEEECCCCCCCCEEEEEEEEEEEECCCCCHHHEEEEEEEECCCEEEEEEEECCCCCCEEEEEEEEEEC
CHHCCCCHHHHHHHHHCCCHHHHCCCHHHHCHHHHCCCCCCCCCCCCCEEEECCCCCCCCCCCCEHHEEEECCCCCCCHEEEEECCCCCHHHHHHHHCCCCCCCCCCCHHHHCCCCCCCCCCCCCCCCCCCCCHHCCCCEEECCCCHHHCCCCCCCHHHCCEEEEEHCCCCEEEEEEECCCCCEEEEECCCCCCCCHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCECCCCCEEEEEEEEECCCCCCCCCCCCEEEEECCCCCCEEEEEEECCCCCCCCCCCEEEEECCCCCCCEEEEEECCCCCCCCCCCEEEEEECC
> 6hrh_1
LYFQSMFSYDQFFRDKIMEKKQDHTYRVFKTVNRWADAYPFAQHFSSKDVSVWCSNDYLGMSRHPQVLQATQETLQRHGVGAGGTRNISGTSKFHVELEQELAELHQKDSALLFSSCFVANDSTLFTLAKILPGCEIYSDAGNHASMIQGIRNSGAAKFVFRHNDPDHLKKLLEKSNPKIPKIVAFETVHSMDGAICPLEELCDVSHQYGALTFVDEVHAVGLYGSRGAGIGERDGIMHKIDIISGTLGKAFGCVGGYIASTRDLVDMVRSYAAGFIFTTSLPPMVLSGALESVRLLKGEEGQALRRAHQRNVKHMRQLLMDRGLPVIPCPSHIIPIRVGNAALNSKLCDLLLSKHGIYVQAINYPTVPRGEELLRLAPSPHHSPQMMEDFVEKLLLAWTAVGLPLQCRRPVHFELMSEWERSYFGNM
CCCCCCCCHHHHHHHHHHHHHHHCCCCCCCEEEECCCCCCEEEEECCEEEEECCCCCCCCHHHCHHHHHHHHHHHHHHCCCCCCCCCCCCECHHHHHHHHHHHHHCCCCEEEEECCHHHHHHHHHHHHHHHCCCCEEEEECCCCHHHHHHHHHHCCEEEEECCCCHHHHHHHHHCCCCCCCEEEEEECECCCCCCECCHHHHHHHHHHCCEEEEEECCCCCCCCCCCCCCHHHHCCCHHHCCEEEEECCCCCCCCCEEEEECHHHHHHHHHHCHHHHCCCCCCHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHCCCCECCCCCCCEEEECCCHHHHHHHHHHHHHCCCEECCEECCCCCCCCCCEEEECCCCCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHCCCC
CCCCCCCCHHHHHHHHHHHHHHCCCEEEHHHHHHHCCCCCEECCCCCCCEEEEECCCHHCCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCCCHHHHHHHHHHHHHHCHHHHEHECCCEEHHHHHHHHHHHHCCCCEEEECCCCCHHHHHEECCCCCCEEEEECCCHHHHHHHHHHCCCCCCEEEEEEEEECCCCCCCCHHHHHHHHHHHCCEEEEHHHHCCEEECCCCCCEEHHCCCCCCEEEHHCCCHHHHCCECEHHHCCHHHHHEEHCCCCCCEEECCCCHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHCCCCECCCCCCCCEEEECCCHHHHHHHHHHHHHCCEEEECCCCCCCCCCHEEEEECCCCCCCHHHHHHHHHHHHHHHHHCCCCCHCCCCCHHHHHHHHHHHCCCCC
> 6m9t_1
DCGSVSVAFPITMLLTGFVGNALAMLLVSRSYRRRESKRKKSFLLCIGWLALTDLVGQLLTTPVVIVVYLSKQRWEHIDPSGRLATFFGLTMTVFGLSSLFIASAMAVERALAIRAPHWYASHMKTRATRAVLLGVWLAVLAFALLPVLGVGQYTVQWPGTWAFISTNWGNLFFASAFAFLGLLALTVTFSCNLATIKALVSRGSNIFEMLRIDEGLRLKIYKDTEGYYTIGIGHLLTKSPSLNAAKSELDKAIGRNTNGVITKDEAEKLFNQDVDATVRGILRNAKLKPVYDSLDAVRRAALINMVFQMGETGVAGFTNSLRMLQQKRWDEAAVNLAKSRWYNQTPNRAKRVITTFRTGTWDAYGSWGRITTETAIQLMAIMCVLSVCWSPLLIMMLKMIFNEKQKECNFFLIAVRLASLNQILDPWVYLLLRKILGRPLEVL
CCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCHHHHHHCCCCHHHHHHHHHHHHHHHHHHCHHHHCCCCEEEECCCCEEEECCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHCCEEEEEECCCCCEEEECCEEEECCCCHHHHHHHHHHHHCCCCCCECCHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHCCHHHHHHHHHHHHHHCHHHHHCCHHHHHHHHCCCHHHHHHHHHCCHHHHHCHHHHHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHC
CCCCCCCEEHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCEEEEEECHHHHHHHHHHEECCEEEEEEEHHCCCCCCCCCCCCHHHHHEEEEEHHHHCHHHHHHHHHHHHHHHCCCCCHCHHHCHHHHHHHEHHHHHHHHHHHHHHHHHCCCCEEEECCCEEEEECCCCCHHHHHHHHHHHHHHHHHHEHHHHHEEEHEHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHEECEEEEEEEEEECCHHHEEHCCCCCCCCCCHHHHHHHHHCHCCCCCCHEEEHHCHHHHHHHHHHCCCCCCCCCCCEEEECCCCCHHCCHHHHHHHHHCCCCCCCCCCCEEECCCCCCCCCHHEEHHCCCCCCCCCCCCCCCHHHHHHHHHEEEEEEEECCCHHHHHEEEEECCCCCCCCCHHHHEEHHHCCCCCCCCHEEEHHHHHHHHHHHCC
> 5w6l_1
NAQELKERAKVFAKPIGASYQGILDQLDLVHQAKGRDQIAASFELNKKINDYIAEHPTSGRNQALTQLKEQVTSALFIGKXQVAQAGIDAIAQTRPELAARIFXVAIEEANGKHVGLTDXXVRWANEDPYLAPKHGYKGETPSDLGFDAKYHVDLGEHYADFKQWLETSQSNGLLSKATLDESTKTVHLGYSYQELQDLTGAESVQXAFYFLKEAAKKADPISGDSAEXILLKKFADQSYLSQLDSDRXDQIEGIYRSSHETDIDAWDRRYSGTGYDELTNKLASATGVDEQLAVLLDDRKGLLIGEVHGSDVNGLRFVNEQXDALKKQGVTVIGLEHLRSDLAQPLIDRYLATGVXSSELSAXLKTKHLDVTLFENARANGXRIVALDANSSARPNVQGTEHGLXYRAGAANNIAVEVLQNLPDGEKFVAIYGKAHLQSHKGIEGFVPGITHRLDLPALKVSDSNQFTVEQDDVSLRVVYDDVANKPKITFKG
CCCCHHHHHHCCCCCCCHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHCHHHCCCCCCCCCCCCCCCCCCCEEEECCCCHHHHHHHHHHHHHCCCCCCEEEECCCCEEEECCCHHHHHHCCCCHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHECHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHHHCCCCCCCHHHHHHHCCCHHHHHHHHHCCCCEEEECCCCCCCCHHHHHHHHCHHHHHHHCEEEEEECCCCCCCHHHHHHHHHHHCCCCHHHHHHHHHCCCCHHHHHHHHHCCCEEEECCCHHHCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCHHHHCCEEECCCEECCHHHHHCCCEEEECCCCCEEECCCCHHHCCECCCCCCCCCCCCCC
CCHHHHHHHHECCCCCCCHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHCCCCHCCCCCCCCCCCCCCCCCCEEEEEHCCCHHHHHHHHHHHHCCCCCCCEEECCCCCEEEECCCHHHHHHCCCCCHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHCCCCHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCEEEECCCCCCHCHHHHHHHHHHHHHHCCCEEEEEHHHHHHHHHHHHHHHHCCCCCCHHHHHHHCCCCCHHHHHHHHHHCCCEEEEECCCHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEEEECCCCCCCCCCCCCCCCCHHHHCCCCEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCEEECC
> 6rbk_3
HITLDIAGQRSTLGIRRLRVQQLINEIPLAQLELHIPTDNHGAADNAVQHEVSRFTLGVRVGIAQDNKPLFDGYLVQKKMQLKGKEWSVRLEARHALQKLTFLPHSRVFRQQDDSTVMKGLLQSAGVKLTQSKHDQLLQFRLSDWQFIRSRLLSTNCWLLPDAASDTVVIRPLSSRTLARDSHDYTLYEINLNFDNRFTPDSLSLQGWDIAAQRLTAAQKSPAGAFRPWKPAGQDYALAFSMLPEATLQTLSNSWLNYQQMTGVQGHIVLAGTRDFAPGESITLSGFGAGLDGTAMLSGVNQQFDTQYGWRSELVIGLPASMLEPAPPVRSLHIGTVAGFTADPQHLDRIAIHLPALNLPDSLIFARLSKPWASHASGFCFYPEPGDEVVVGFIDSDPRYPMILGALHNPKNTAPFPPDEKNNRKGLIVSQADQTQALMIDTEEKTLRLMAGDNTLTLTGEGNLTMSTPNALQLQADTLGLQADSNLSIAGKQQVEITSAKINM
CEEEEECCECCCCCEEEEEEEECCCCCCEEEEEEECCCCCECHHHHCCHHHHCCCCCCCEEEEEECCEECCCEEEEEEEECCCCCCCEEEEEEECHHHHHCCCCECCCCCCCCHHHHHCCCHHHCCCEEECCCCCCCCCCCECCCCCCCCCCCCCCEECCECCCCCEEECEECCCCCECCCCCCCCEEEEEECCCCCCCCCCCEEEECCCCCCCCCCCECCCCCCCCCEEECCCCCEEEECCCCCCCCHHHHHHHHHHHHCCCCCEEEEECCCCCCCCCCCEEECCCCCCCCEEECEEEEECCEECCCCECCEEEECCCCCCCCCCCCCCCCEEECECCCCCCCCCCCCEEECCCCCCCCCCCEEEEECCCCCCCCCCCCCCCCCCCCEEEEEHHHCCCCEEEEECCCCCCCCCCCCCCCCCCECCCCCECCCCECCEECECCCCCCEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CEEECCCCCCCCEEEEEEEEHHHCCCCCCEEEEEECCCCCCCCCCCCCHCCCCCCCEEEEECCCCCCEEEEEEEEEEEEEEECCCCCEEEEEECCHHHHHHCCCCCEEEHCCCHHHHHHHHHHHCCCCCCCCCCHEEEECCCCCHHHHHHHHHHCCEEEEECCCCEEEECCCCCCCCCEEHCCCCCEEEEEHHCCHHCCCCHEEEECCCCHHHHEEEECCCCCCCCCCCCCCCHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHEEEEEEEECCCECCCCCEEEEHCCCCCCCCEEEEEEEEEEEECCCCCEEEEEEECCCCCCCCCCCCCCCEEEEEECCCCCCCCCCEEEEEECECCCCCCCEEEEEEHCCCCCCCCEEECCCCCCEEEEEEECCCCCCCEEEECCCCCCCCCCCCCCCCCCCEEEEEECCCCCEEEEECCCCCEEEEECCCCEEEEECCCCEEEECCCCEEEECCCEEEECCCCEEEEECCCEEEEECCECC
> 6n9y_1
MERFLRKYNISGDYANATRTFLAISPQWTCSHLKRNCLFNGMCAKQNFERAMIAATDAEEPAKAYRLVELAKEAMYDRETVWLQCFKSFSQPYEEDIEGKMKRCGAQLLEDYRKNGMMDEAVKQSALVNSERVRLDDSLSAMPYIYVPIKEGQIVNPTFISRYRQIAYYFYSPNLADDWIDPNLFGIRGQHNQIKREIERQVNTCPYTGYKGRVLQVMFLPIQLINFLRMDDFAKHFNRYASMAIQQYLRVGYAEEVRYVQQLFGKIPTGEFPLHHMMLMRRDFPTRDRSIVEARVRRSGDENWQSWLLPMIIVREGLDHQDRWEWLIDYMDRKHTCQLCYLKHSKQIPTCGVIDVRASELTGCSPFKTVKIEEHVGNNSVFETKLVRDEQIGRIGDHYYTTNCYTGAEALITTAIHIHRWIRGCGIWNDEGWREGIFMLGRVLLRWELTKAQRSALLRLFCFVCYGYAPRADGTIPDWNNLGSFLDTILKGPELSEDEDERAYATMFEMVRCIITLCYAEKVHFAGFAAPACESGEVINLAARMSQMRMEY
CHHHHHHHCCCHHHHHHHHHHHHHCCCCCCCCCCHHHECCCCECCCCHHHHHHHCCCCCCHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCHHHHHHCCCCCCCCCCCCCCCCEECHHHHCEEECCCCECCCCEEEECCCCEEEEECCCCCCEEECCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCEEEECCCHHHHCCHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHCCCCCCCCCCHHHHCCCCCCCCCCCCHHHHHHCCCCCCCHHHCCEECHHHHHHCCCCCCHHHHHHHHHHCCCCCHHHHHCCCCCCEEEEECHHHHHHCCCCCEEEEEEEEECCCCCCCCCCCCCCEEEEEECCEEEEEECCCCCHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHCCC
HHHHHHHCCCCHHHHHHHHHHHHHCCCCCCCCCCCCEECCCHEHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHCCHHHHHHHCCCCCCCCEEECCCHCCCCEEEEECCCCCCCCECEHHHHCCEEEEEECCCCCCCCCCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCEEEEEEEHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHCCCCCCCCHHHHHHHHCCCCCCCCCHHHHHHHCCCCCCCHHHHHHHHHHHHHHHCCCCCHHHHHHHCCCCCHHHHHHHHHHCCCCEEEEEEEEHHHHCCCCHHHEEEEEECCCCCCCCEEECCCCCEEEHCCCEEEEEECCCCHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHEEECCCCCHHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHHHEHHCCCCEEEECCCCCCCCCCCHHHHHHHHHHHHHHC
> 6nq1_1
AARWDLCIDQAVVFIEDAIQYRSINHRVDASSMWLYRRYYSNVCQRTLSFTIFLILFLAFIETPSSLTSTADVRYRAAPWEPPAGLTESVEVLCLLVFAADLSVKGYLFGWAHFQKNLWLLGYLVVLVVSLVDWTVSLSLVAHEPLRIRRLLRPFFLLQNSSMMKKTLKCIRWSLPEMASVGLLLAIHLCLFTMFGMLLFAGLTYFQNLPESLTSLLVLLTTANNPDVMIPAYSKNRAYAIFFIVFTVIGSLFLMNLLTAIIYSQFRGYLMKSLQTSLFRRRLGTRAAFEVLSSMVGAVGVKPQNLLQVLQKVQLDSSHKQAMMEKVRSYGSVLLSAEEFQKLFNELDRSVVKEHPPRPEYQSPFLQSAQFLFGHYYFDYLGNLIALANLVSICVFLVLDADVLPAERDDFILGILNCVFIVYYLLEMLLKVFALGLRGYLSYPSNVFDGLLTVVLLVLEISTLAVYRLSLWDMTRMLNMLIVFRFLRIIPSMKPMAVVASTVLGLVQNMRAFGGILVVVYYVFAIIGINLFRGVIVALSAPBGSFEQLEYWANNFDDFAAALVTLWNLMVVNNWQVFLDAYRRYSGPWSKIYFVLWWLVSSVIWVNLFLALILENFLHKW
CHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHCCHHHHHHHHHHHHHHCCHHHHCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHCCCHHHHHHCCCCHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHCCCCCCCCCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCECHHHHHHHHHHCCCCHHHHHHHHHHHHHCCCCCECHHHHHHHHHHHHCCCCCCCCCCCCCCHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCHHHCCCCCHHHHHHHHHHHHCCCCCHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCC
CCCHHHHHHHHHHEHHHHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHECCHHHHHHCHHHEEEEEEEEEEHHHHEEEEHCCCCCHEEHHHHHCHEEEHCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHEEECCCCCCEEEECCCHCHHHEEEEEEEEEECHHHHHHHHHHEEHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCHHHHHHHHCCCCCCCCHHHHHHHHHCCCCCCCCCHHHHHHHHHHHHCCCEECCCCCCCCCCCHHHHHHHHEECCHHHEEEEEEEEHHEEEEEEEEECCCCCCCCCCCCCEEHHHHHHEHHHHHHHHHHHHHHCCCHHHHCCCCCCCCEEEEEEEHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHHHHHHHCEEEEEEHHHHHHEHHHHCCCECCCCCCCCCHHHHHHHHHCHHHHHHHHHHHHHEEEECCCEEEHHHHHHHCCCHHHHEEEEEEEEEEEEEEEEEEEEHHHHHHHCC
> 6dru_1
TYFAPNSTGLRIQHGFETILIQPFGYDGFRVRAWPFRPPSGNEISFIYDPPIEGYEDTAHGMSYDTATTGTEPRTLRNGNIILRTTGWGGTTAGYRLSFYRVNDDGSETLLTNEYAPLKSLNPRYYYWPGPGAEFSAEFSFSATPDEQIYGTGTQQDHMINKKGSVIDMVNFNSYIPTPVFMSNKGYAFIWNMPAEGRMEFGTLRTRFTAASTTLVDYVIVAAQPGDYDTLQQRISALTGRAPAPPDFSLGYIQSKLRYENQTEVELLAQNFHDRNIPVSMIVIDYQSWAHQGDWALDPRLWPNVAQMSARVKNLTGAEMMASLWPSVADDSVNYAALQANGLLSATRDGPGTTDSWNGSYIRNYDSTNPSARKFLWSMLKKNYYDKGIKNFWIDQADGGALGEAYENNGQSTYIESIPFTLPNVNYAAGTQLSVGKLYPWAHQQAIEEGFRNATDTKEGSAADHVSLSRSGYIGSQRFASMIWSGDTTSVWDTLAVQVASGLSAAATGWGWWTVDAGGFEVDSTVWWSGNIDTPEYRELYVRWLAWTTFLPFMRTHGSRTBYFQDAYTBANEPWSYGASNTPIIVSYIHLRYQLGAYLKSIFNQFHLTGRSIMRPLYMDFEKTDPKISQLVSSNSNYTTQQYMFGPRLLVSPVTLPNVTEWPVYLPQTGQNNTKPWTYWWTNETYAGGQVVKVPAPLQHIPVFHLGSREELLSGNVF
CCCCCCCCCEEEEECCEEEEEEECCCCEEEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCCCEEEEECCEEEEEECCCCCCCCCECEEEEECCCCCEEEEEEECCCCCCCCCEEEECCCCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCCCEEECCCECCEEEEEEEEECCCEEEEECCCCCEEEEECCCEEEEEEEEECCEEEEEEECCCCCHHHHHHHHHHHHCCCCCCCHHHHCEEECCCCCCCHHHHHHHHHHHHHCCCCCCEEEECCCCECCCCCCCECCCCCCCHHHHHHHHHHHHCCEEEEEECCEECCCCCCHHHHHHCCCECEECCCCCCCEEECCEEEEEECCCCHHHHHHHHHHHHHHCHHHCCCEEEECCCCCCCCCCCCCCCCCCHHHHHCCCCCCCEECCCCEHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCEEECCCCCCHHHCCEEEECCCCECCHHHHHHHHHHHHHHHHHCCCCEECCECCCECCCCCCCCCECCCHHHHHHHHHHHHHHCCCCCEEECCCCECCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHCHHHHHHHHHHHHHHCCCCEECCHHHHCCCCCCHHHHHHCCCHHHHCCEEECCCEEECCCCCCCCCEEEEEECCCCCCCCCCEEECCCCCEECCCEEEEEECCCCCCCEEECCCHHHHCCCCCC
CCEECCCCEEEEECCCCEEEEECCCCCCEEEEECCCCCCCCCCHHECCCCCCCCCCCCCCCCCCEEEECCCCCEEEECCCEEEEECCCCCCCCCCEEEEEECCCCCCCEEEHHHCCCCCCCCHHHEECCCCCCCEEEEEEECCCCCCHHECCCCCCCCCCCCCCCEEEEEECCCCEEEEEEECCCCCEEEECCCCCCEEEECCCCCEEEEHHCCEEEEEEEECCCCCHHHHHHHHHHHCCCCCCCCHHHHCCHHHHHCCCCHHHHHHHHHHHHHCCCCCEEEEEEHECCCCCCCCCCCCCCCCCCHHHHHHHHHHCCCEEEEEECCCCCCCCHCHHHHHHHCCEEECCCCCEECCCCCCCCEEEECCCCCHHHHHHHHHHHHHHHHHCCEEEEECHCCCCCCCCCCCCCCCCCCEECCCCCCCCHEEECCCCHHHCCHHHHHHHHHHHHHHHCCCCCCCCCCCCEEEEEEHHHHCCCCCCEEEECCCCCCCHHHHHHHHHHCHHHHHCCCCCECCCCCCEECCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHEECCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHCCCCCCCCCCHCCCCCCCCCCCHHHHCCCCHEECCECCCCCCEEEEECCCCCCCCCCCEEEECCCCEECCCCEEEECCCCCCEEEEECCCCCCCCCCCCC
