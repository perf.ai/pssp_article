"""
	Author: Panayiotis Leontiou
	Since: Jul 4, 2020
	Version: 1.0
	Bugs: No known bugs
"""

import string
import io
from collections import OrderedDict

with open('cb513_dataset.txt', 'r') as cb513_f:
	cb513_data = cb513_f.readlines()

def replace_all(text, dic):
	for i, j in dic.items():
		text = text.replace(i, j)
	return text

mappings = OrderedDict([("G", "H"), ("I", "H"), ("B", "E"), ("T", "C"), ("S", "C"), (" ", "C")]) 
# {"G": "H", "I": "H", "B": "E", "T": "C", "S": "C", " ": "C"}

arr = []
with open('cb513_3class.txt', 'w') as out_f:
	for i in range(0, len(cb513_data), 3):
		protein_name = cb513_data[i].rstrip('\n')
		primary_structure = cb513_data[i + 1].rstrip('\n')
		secondary_structure = cb513_data[i + 2].rstrip('\n')

		temp = protein_name
		new_name = '> ' + temp.upper()
		# print(new_name)
		out_f.write(new_name + '\n')

		new_primary = primary_structure.replace('!', '')
		# print(new_primary)
		out_f.write(new_primary + '\n')
		
		new_secondary = secondary_structure.replace('!', '')
		# print(new_secondary)
		out_f.write(new_secondary + '\n')

		arr.append([new_name, new_primary, new_secondary])

# Sort proteins based on primary structure length
arr.sort(key = lambda s: len(s[1]))

with open('cb513_sorted.txt', 'w') as sorted_out_f:
	for p in arr:
		sorted_out_f.write(p[0] + '\n')
		sorted_out_f.write(p[1] + '\n')
		sorted_out_f.write(p[2] + '\n')

