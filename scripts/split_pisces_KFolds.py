from sklearn.model_selection import KFold
import numpy as np
import os

total_folds = 5
dataset_name = "pisces"
out_folder = "./generated_datasets/pisces_folds"
full_dataset_path = "./datasets/PISCES/pisces_sorted.txt"

if not os.path.exists("./generated_datasets"):
	os.makedirs("./generated_datasets")

if not os.path.exists(out_folder):
	os.makedirs(out_folder)

with open(full_dataset_path, 'r') as input_f:
	all_data = input_f.readlines() 

data = []
for i in range(0, len(all_data), 3):
	temp = "{}{}{}".format(all_data[i], all_data[i+1], all_data[i+2])
	# protein_name = all_data[i]
	# primary_struct = all_data[i+1]
	# secondary_struct = all_data[i+2]
	data.append(temp)

data = np.array(data)
kf = KFold(n_splits=total_folds, shuffle=True, random_state=42)
count_folds =  0
for train_index, test_index in kf.split(data):
	# print("TRAIN:", train_index, "TEST:", test_index)
	X_train, X_test = data[train_index], data[test_index]
	with open("{}/{}_train_fold{}.txt".format(out_folder, dataset_name, count_folds), 'w') as fold_f:
		for r in X_train:
			fold_f.write(r)

	with open("{}/{}_test_fold{}.txt".format(out_folder, dataset_name, count_folds), 'w') as fold_f:
		for r in X_test:
			fold_f.write(r)
	# print(X_train)
	count_folds += 1
