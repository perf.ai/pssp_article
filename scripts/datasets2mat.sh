#!/bin/bash

# This script converts all the files in the given directory to matlab datasets 
# and saves them in a new folder.

if [ $# -eq 0 ]; then
	ds_path="./generated_datasets/plus7_cb513"
else
	ds_path="$1"
fi

[[ ! -d "$ds_path" ]] && echo "Error! This directory does not exists!" && exit 0

folderName="$ds_path""_mat"
mkdir "$folderName"

runAll=""
# datasets=$(ls $ds_path | grep -e 'test_fold' -e 'train_fold' | grep -v '\.mat')
datasets=$(ls $ds_path)

for ds in $datasets
do 
	# echo "$ds"
	loaded=$(echo "$ds" | sed "s:.txt::")
	# echo "$loaded"
	outFile=$(echo "$folderName/$loaded.mat")
	# echo "$outFile"
	runMat="load $ds_path/$ds; y = $loaded(1:end, end); Z = $loaded(1:end, 1:end-1); save $outFile y Z -v7.3; clear;"
	# echo "$runMat"
	runAll="$runAll$runMat "
done 
runAll="$runAll exit;"
# echo "$runAll"
matlab -nodisplay -r "$runAll" > "./$folderName/log.txt"
