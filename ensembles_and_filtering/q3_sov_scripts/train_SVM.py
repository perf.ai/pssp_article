# Execute: python train_SVM.py <test_filename> <train_filename> <WINDOW> <pred_file> <out_prediction> <out_sov> <filter_opt>
import sys
import string
import numpy as np
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
SEED = 42
np.random.seed(SEED)

def get_balanced_data(X_train, y_train):
	classH = []
	classE = []
	classC = []
	for i,label in enumerate(y_train):
		if label == 0:
			classH.append(i)
		elif label == 1:
			classE.append(i)
		else:
			classC.append(i)
	rows = min(len(classH), len(classE), len(classC))

	# Create a balanced data set 
	X_balanced = np.concatenate((X_train[classH][0:rows], X_train[classE][0:rows], X_train[classC][0:rows]), axis=0)

	y_balanced = np.concatenate((y_train[classH][0:rows], y_train[classE][0:rows], y_train[classC][0:rows]), axis=0)
	
	balanced = np.zeros((X_balanced.shape[0], X_balanced.shape[1]+1), dtype=int)
	
	balanced[:,-1] = y_balanced
	balanced[:,:-1] = X_balanced
	np.random.shuffle(balanced)
	return balanced[:,:-1], balanced[:,-1]


def create_output_pred(pred, input_f, out_f, outSOV):
	with open(input_f, "r") as pred_file: 
		pred_lines = pred_file.readlines()
	pred = pred.astype(int)
	labels = ['C', 'E', 'H']
	counter = 0
	with open(out_f, 'w') as out_file:
		for line in range(0, len(pred_lines)//4):
			protein_name = pred_lines[line*4][0:-1]
			primary_structure = pred_lines[line*4+1][0:-1]
			secondary_structure = pred_lines[line*4+2][0:-1]
			prediction = ""
			for c in secondary_structure:
				prediction = prediction + labels[pred[counter]]
				counter += 1
			out_file.write(protein_name + "\n")
			out_file.write(primary_structure + "\n")
			out_file.write(secondary_structure + "\n")
			out_file.write(prediction + "\n")

	with open(out_f, "r") as out_file: 
		lines = out_file.readlines()
	with open(outSOV, "w") as f1:
		for i in range(0, len(lines), 4):
			f1.write('>OSEQ\n')
			f1.write(lines[i + 2])
			f1.write('>PSEQ\n')
			f1.write(lines[i + 3])
			f1.write('>AA\n')
			f1.write(lines[i + 1])

train_dataset = np.loadtxt(sys.argv[1], delimiter=",")
# Subtract one because we dropped the middle column to prevent data leaks
win=int(sys.argv[3]) 
X_train = train_dataset[:, 0:win]
y_train = train_dataset[:, [win]]
test_dataset = np.loadtxt(sys.argv[1], delimiter=",")
X_test = test_dataset[:, 0:win]
y_test = test_dataset[:, [win]]
y_train = np.reshape(y_train,len(y_train))
y_test = np.reshape(y_test,len(y_test))
# X_train, y_train = get_balanced_data(X_train, y_train)

print("Training ...") 

if (sys.argv[7] == '1'):
	clf = SVC(C=100, decision_function_shape='ovr', kernel='rbf', random_state=SEED, gamma='scale')
elif (sys.argv[7] == '2'):
	clf = DecisionTreeClassifier(max_depth=20)
elif (sys.argv[7] == '3'):
	clf = RandomForestClassifier(max_depth=25, random_state=SEED, n_estimators=100)
elif (sys.argv[7] == '0'):
	kernels = ['Polynomial', 'RBF', 'Sigmoid','Linear']
	#A function which returns the corresponding SVC model
	def getClassifier(ktype):
	    if ktype == 0:
	        # Polynomial kernal
	        return SVC(kernel='poly', degree=8, gamma="auto")
	    elif ktype == 1:
	        # Radial Basis Function kernel
	        return SVC(kernel='rbf', gamma="auto")
	    elif ktype == 2:
	        # Sigmoid kernel
	        return SVC(kernel='sigmoid', gamma="auto")
	    elif ktype == 3:
	        # Linear kernel
	        return SVC(kernel='linear', gamma="auto")

	for i in range(1, 4):
		# Train a SVC model using different kernels
	    svclassifier = getClassifier(i) 
	    svclassifier.fit(X_train, y_train)
		# Make prediction
	    y_pred = svclassifier.predict(X_test)
		# Evaluate model
	    print("Evaluation:", kernels[i], "kernel")
	    print(classification_report(y_test, y_pred))

	from sklearn.model_selection import GridSearchCV
	param_grid = {'C': [0.1, 1, 10], 'gamma': [1, 0.1, 0.01, 0.001],'kernel': ['rbf']}
	grid = GridSearchCV(SVC(), param_grid, refit=True, verbose=2)
	grid.fit(X_train, y_train)
	print(grid.best_estimator_)

	y_pred = grid.predict(X_test)
	print(confusion_matrix(y_test, y_pred))
	print(classification_report(y_test, y_pred))
	exit(0)
else:
	print('Error! train_SVM.py currently has no such filtering option.')
	print('Please try again (availiable options: 0-3)')
	exit(0)

# Predict the response for test dataset
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

print("THE SCORE: ", clf.score(X_test, y_test))
print("")

# creating a confusion matrix 
cm = confusion_matrix(y_test, y_pred) 
print('Confusion Matrix')
print(cm)
print("")

create_output_pred(y_pred, sys.argv[4], sys.argv[5], sys.argv[6])
