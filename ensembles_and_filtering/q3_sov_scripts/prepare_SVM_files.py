# Execute: python prepare_SVM_files.py <test_filename> <train_filename> <WINDOW> <out_test> <out_train>
import sys
EXCL_MIDDLE = False
#open TEST file to read data
with open(sys.argv[1],"r") as testfile:
    lines_test = testfile.readlines()
#open TRAIN file to read dat
with open(sys.argv[2],"r") as trainfile:
    lines_train = trainfile.readlines()
linenum = 1
window = int(sys.argv[3])
leftwindow = int(window/2)
#create train file
with open(sys.argv[5], "w") as svmtrain:
    for line in lines_train: 
        if linenum == 5: linenum = 1 
        if linenum == 3: 
            target_out = line 
            # if linenum == 4:
            for i in range(leftwindow): 
                zeros = leftwindow - i 
                for zer in range(zeros):
                    svmtrain.write("0,") 
                for rem in range(i):
                    if line[rem] == "C": svmtrain.write("0,")
                    if line[rem] == "E": svmtrain.write("1,")
                    if line[rem] == "H": svmtrain.write("2,")
                #place right aminos
                for j in range(leftwindow+1): 
                    if (EXCL_MIDDLE and j == 0): continue
                    if line[i+j] == "C": svmtrain.write("0,") 
                    if line[i+j] == "E": svmtrain.write("1,") 
                    if line[i+j] == "H": svmtrain.write("2,") 
                #place label at the end
                if target_out[i] == "C": svmtrain.write("0")
                if target_out[i] == "E": svmtrain.write("1")
                if target_out[i] == "H": svmtrain.write("2")
                svmtrain.write("\n")
            #place aminos with no boundary constraints
            for amino in range(leftwindow,len(line)-leftwindow-1):
                for curr in range(-leftwindow,leftwindow+1): 
                    if (EXCL_MIDDLE and curr == 0): continue
                    if line[amino+curr] == "C": svmtrain.write("0,") 
                    if line[amino+curr] == "E": svmtrain.write("1,") 
                    if line[amino+curr] == "H": svmtrain.write("2,") 
                #place label
                if target_out[amino] == "C": svmtrain.write("0")
                if target_out[amino] == "E": svmtrain.write("1")
                if target_out[amino] == "H": svmtrain.write("2")
                svmtrain.write("\n")
            #place last aminos with padding
            for i in range(len(line)-leftwindow-1,len(line)-1):
                printed=0
                for left in range(i-leftwindow-1,i):
                    if (EXCL_MIDDLE and left == i-1): continue 
                    if line[left] == "C": svmtrain.write("0,")
                    if line[left] == "E": svmtrain.write("1,")
                    if line[left] == "H": svmtrain.write("2,")
                for j in range(i,len(line)-1): 
                    if line[j] == "C": svmtrain.write("0,") 
                    if line[j] == "E": svmtrain.write("1,") 
                    if line[j] == "H": svmtrain.write("2,") 
                    printed=printed+1
                zeros = leftwindow-printed 
                for z in range(zeros):
                    svmtrain.write("0,")
                # place label
                if target_out[i] == "C": svmtrain.write("0")
                if target_out[i] == "E": svmtrain.write("1")
                if target_out[i] == "H": svmtrain.write("2")
                svmtrain.write("\n") 
        linenum += 1
    svmtrain.flush()
linenum=1
#create TEST file
with open(sys.argv[4], "w") as svmtest:
    for line in lines_test: 
        if linenum == 5: linenum = 1 
        if linenum == 3: target_out = line 
        if linenum == 4:
            for i in range(leftwindow): 
                zeros = leftwindow - i 
                for zer in range(zeros):
                    svmtest.write("0,") 
                for rem in range(i):
                    if line[rem] == "C": svmtest.write("0,")
                    if line[rem] == "E": svmtest.write("1,")
                    if line[rem] == "H": svmtest.write("2,")
                #place right aminos
                for j in range(leftwindow+1): 
                    if (EXCL_MIDDLE and j == 0): continue 
                    if line[i+j] == "C": svmtest.write("0,") 
                    if line[i+j] == "E": svmtest.write("1,") 
                    if line[i+j] == "H": svmtest.write("2,") 
                #place label at the end
                if target_out[i] == "C": svmtest.write("0")
                if target_out[i] == "E": svmtest.write("1")
                if target_out[i] == "H": svmtest.write("2") 
                svmtest.write("\n")
            #place aminos with no boundary constraints
            for amino in range(leftwindow,len(line)-leftwindow-1):
                for curr in range(-leftwindow,leftwindow+1): 
                    if (EXCL_MIDDLE and curr == 0): continue
                    if line[amino+curr] == "C": svmtest.write("0,")
                    if line[amino+curr] == "E": svmtest.write("1,")
                    if line[amino+curr] == "H": svmtest.write("2,")
                #place label
                if target_out[amino] == "C": svmtest.write("0")
                if target_out[amino] == "E": svmtest.write("1")
                if target_out[amino] == "H": svmtest.write("2")
                svmtest.write("\n")
            #place last aminos with padding
            for i in range(len(line)-leftwindow-1,len(line)-1):
                printed=0
                for left in range(i-leftwindow-1,i):
                    if (EXCL_MIDDLE and left == i-1): continue 
                    if line[left] == "C": svmtest.write("0,")
                    if line[left] == "E": svmtest.write("1,")
                    if line[left] == "H": svmtest.write("2,")
                for j in range(i,len(line)-1): 
                    if line[j] == "C": svmtest.write("0,") 
                    if line[j] == "E": svmtest.write("1,") 
                    if line[j] == "H": svmtest.write("2,") 
                    printed+=1
                zeros = leftwindow-printed 
                for z in range(zeros):
                    svmtest.write("0,") 
                # place label
                if target_out[i] == "C": svmtest.write("0")
                if target_out[i] == "E": svmtest.write("1") 
                if target_out[i] == "H": svmtest.write("2") 
                svmtest.write("\n")
        linenum += 1 
    svmtest.flush() 
