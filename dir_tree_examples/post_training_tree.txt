.
├── CB513_cross_validation
│   ├── fold0
│   │   └── test_fold0_execution8.txt
│   ├── fold1
│   │   └── test_fold1_execution3.txt
│   ├── fold2
│   │   └── test_fold2_execution6.txt
│   ├── fold3
│   │   └── test_fold3_execution6.txt
│   ├── fold4
│   │   └── test_fold4_execution5.txt
│   ├── fold5
│   │   └── test_fold5_execution5.txt
│   ├── fold6
│   │   └── test_fold6_execution4.txt
│   ├── fold7
│   │   └── test_fold7_execution2.txt
│   ├── fold8
│   │   └── test_fold8_execution6.txt
│   └── fold9
│       └── test_fold9_execution5.txt
├── CB513_runAll.sh
├── CB513_test_pred
│   ├── fold0
│   │   ├── test_fold0_execution1.txt
│   │   ├── test_fold0_execution2.txt
│   │   ├── test_fold0_execution4.txt
│   │   ├── test_fold0_execution5.txt
│   │   └── test_fold0_execution8.txt
│   ├── fold1
│   │   ├── test_fold1_execution1.txt
│   │   ├── test_fold1_execution2.txt
│   │   ├── test_fold1_execution3.txt
│   │   ├── test_fold1_execution5.txt
│   │   └── test_fold1_execution7.txt
│   ├── fold2
│   │   ├── test_fold2_execution1.txt
│   │   ├── test_fold2_execution3.txt
│   │   ├── test_fold2_execution4.txt
│   │   ├── test_fold2_execution6.txt
│   │   └── test_fold2_execution7.txt
│   ├── fold3
│   │   ├── test_fold3_execution1.txt
│   │   ├── test_fold3_execution3.txt
│   │   ├── test_fold3_execution4.txt
│   │   ├── test_fold3_execution5.txt
│   │   └── test_fold3_execution6.txt
│   ├── fold4
│   │   ├── test_fold4_execution1.txt
│   │   ├── test_fold4_execution3.txt
│   │   ├── test_fold4_execution5.txt
│   │   ├── test_fold4_execution6.txt
│   │   └── test_fold4_execution7.txt
│   ├── fold5
│   │   ├── test_fold5_execution1.txt
│   │   ├── test_fold5_execution3.txt
│   │   ├── test_fold5_execution5.txt
│   │   ├── test_fold5_execution7.txt
│   │   └── test_fold5_execution8.txt
│   ├── fold6
│   │   ├── test_fold6_execution2.txt
│   │   ├── test_fold6_execution4.txt
│   │   ├── test_fold6_execution5.txt
│   │   ├── test_fold6_execution6.txt
│   │   └── test_fold6_execution7.txt
│   ├── fold7
│   │   ├── test_fold7_execution1.txt
│   │   ├── test_fold7_execution2.txt
│   │   ├── test_fold7_execution4.txt
│   │   ├── test_fold7_execution5.txt
│   │   └── test_fold7_execution7.txt
│   ├── fold8
│   │   ├── test_fold8_execution1.txt
│   │   ├── test_fold8_execution2.txt
│   │   ├── test_fold8_execution4.txt
│   │   ├── test_fold8_execution6.txt
│   │   └── test_fold8_execution7.txt
│   └── fold9
│       ├── test_fold9_execution1.txt
│       ├── test_fold9_execution2.txt
│       ├── test_fold9_execution3.txt
│       ├── test_fold9_execution4.txt
│       └── test_fold9_execution6.txt
├── CB513_train_pred
│   ├── fold0_train_pred.txt
│   ├── fold1_train_pred.txt
│   ├── fold2_train_pred.txt
│   ├── fold3_train_pred.txt
│   ├── fold4_train_pred.txt
│   ├── fold5_train_pred.txt
│   ├── fold6_train_pred.txt
│   ├── fold7_train_pred.txt
│   ├── fold8_train_pred.txt
│   └── fold9_train_pred.txt
├── CB513_view_results.sh
├── PISCES_cross_validation
│   ├── fold1
│   │   └── pred_testSet1_ex7.txt
│   ├── fold2
│   │   └── pred_testSet2_ex1.txt
│   ├── fold3
│   │   └── pred_testSet3_ex7.txt
│   ├── fold4
│   │   └── pred_testSet4_ex3.txt
│   └── fold5
│       └── pred_testSet5_ex7.txt
├── PISCES_runAll.sh
├── PISCES_test_pred
│   ├── fold1
│   │   ├── pred_testSet1_ex1.txt
│   │   ├── pred_testSet1_ex5.txt
│   │   ├── pred_testSet1_ex6.txt
│   │   ├── pred_testSet1_ex7.txt
│   │   └── pred_testSet1_ex8.txt
│   ├── fold2
│   │   ├── pred_testSet2_ex1.txt
│   │   ├── pred_testSet2_ex5.txt
│   │   ├── pred_testSet2_ex6.txt
│   │   ├── pred_testSet2_ex7.txt
│   │   └── pred_testSet2_ex8.txt
│   ├── fold3
│   │   ├── pred_testSet3_ex2.txt
│   │   ├── pred_testSet3_ex4.txt
│   │   ├── pred_testSet3_ex5.txt
│   │   ├── pred_testSet3_ex6.txt
│   │   └── pred_testSet3_ex7.txt
│   ├── fold4
│   │   ├── pred_testSet4_ex3.txt
│   │   ├── pred_testSet4_ex4.txt
│   │   ├── pred_testSet4_ex5.txt
│   │   ├── pred_testSet4_ex6.txt
│   │   └── pred_testSet4_ex7.txt
│   └── fold5
│       ├── pred_testSet5_ex3.txt
│       ├── pred_testSet5_ex4.txt
│       ├── pred_testSet5_ex5.txt
│       ├── pred_testSet5_ex6.txt
│       └── pred_testSet5_ex7.txt
├── PISCES_train_pred
│   ├── fold1_train_pred.txt
│   ├── fold2_train_pred.txt
│   ├── fold3_train_pred.txt
│   ├── fold4_train_pred.txt
│   └── fold5_train_pred.txt
├── PISCES_view_results.sh
├── final_results_CB513.txt
├── final_results_PISCES.txt
└── q3_sov_scripts
    ├── calc_Q3.py
    ├── ensembles.py
    ├── externalRules.py
    ├── prepare_SVM_files.py
    ├── runSOV.c
    ├── sov.c
    └── train_SVM.py

37 directories, 118 files
