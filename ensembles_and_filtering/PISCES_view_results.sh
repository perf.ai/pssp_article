#!/bin/bash

# Path to file with ensembles and filtering results
if [ $# -ne 1 ]; then
	file="./final_results_PISCES.txt"
else
	file="$1"
fi

if [ ! -f "$file" ]; then
	echo "This file does not exist: $file"
	exit 1
fi

echo "Cross Validation"
echo -e "Q3_ALL\tQ3_H\tQ3_E\tQ3_C\tSOV_ALL\tSOV_H\tSOV_E\tSOV_C"
echo "-------------------------------------------------------------"
sed -n "/fold/,/-----/p" "$file" |  grep -E '[0-9]+' | grep -v '[a-zA-Z]' | tr -s " " | sed -e 's/^[ \t]*//' | awk -F' ' 'BEGIN{switch=1}{if (switch == 1) {v1=$1; v2=$2; v3=$3; v4=$4; switch=2;} else {printf "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", $1, $2, $3, $4, v1, v2, v3, v4; switch=1}}'
echo ""

echo "Ensembles Results"
echo -e "Q3_ALL\tQ3_H\tQ3_E\tQ3_C\tSOV_ALL\tSOV_H\tSOV_E\tSOV_C"
echo "-------------------------------------------------------------"
sed -n '/Ensembles Results/,/=====/p' "$file" | grep -E '[0-9]+'| grep -v '[a-zA-Z]' | tr -s " " | sed -e 's/^[ \t]*//' | awk -F' ' 'BEGIN{switch=1}{if (switch == 1) {v1=$1; v2=$2; v3=$3; v4=$4; switch=2;} else {printf "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", $1, $2, $3, $4, v1, v2, v3, v4; switch=1}}'
echo ""

echo "Ensembles + External Rules Results"
echo -e "Q3_ALL\tQ3_H\tQ3_E\tQ3_C\tSOV_ALL\tSOV_H\tSOV_E\tSOV_C"
echo "-------------------------------------------------------------"
sed -n '/Ensembles + External Rules Results/,/=====/p' "$file" | grep -E '[0-9]+'| grep -v '[a-zA-Z]' | tr -s " " | sed -e 's/^[ \t]*//' | awk -F' ' 'BEGIN{switch=1}{if (switch == 1) {v1=$1; v2=$2; v3=$3; v4=$4; switch=2;} else {printf "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", $1, $2, $3, $4, v1, v2, v3, v4; switch=1}}'
echo ""

echo "Ensembles + External Rules + Decision Tree Results"
echo -e "Q3_ALL\tQ3_H\tQ3_E\tQ3_C\tSOV_ALL\tSOV_H\tSOV_E\tSOV_C"
echo "-------------------------------------------------------------"
sed -n '/Ensembles + External Rules + Decision Tree Results/,/=====/p' "$file" | grep -E '[0-9]+'| grep -v '[a-zA-Z]' | tr -s " " | sed -e 's/^[ \t]*//' | awk -F' ' 'BEGIN{switch=1}{if (switch == 1) {v1=$1; v2=$2; v3=$3; v4=$4; switch=2;} else {printf "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", $1, $2, $3, $4, v1, v2, v3, v4; switch=1}}'
echo ""

echo "Ensembles + Decision Tree Results"
echo -e "Q3_ALL\tQ3_H\tQ3_E\tQ3_C\tSOV_ALL\tSOV_H\tSOV_E\tSOV_C"
echo "-------------------------------------------------------------"
sed -n '/Ensembles + Decision Tree Results/,/=====/p' "$file" | grep -E '[0-9]+'| grep -v '[a-zA-Z]' | tr -s " " | sed -e 's/^[ \t]*//' | awk -F' ' 'BEGIN{switch=1}{if (switch == 1) {v1=$1; v2=$2; v3=$3; v4=$4; switch=2;} else {printf "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", $1, $2, $3, $4, v1, v2, v3, v4; switch=1}}'
echo ""

echo "Ensembles + Decision Tree + External Rules Results"
echo -e "Q3_ALL\tQ3_H\tQ3_E\tQ3_C\tSOV_ALL\tSOV_H\tSOV_E\tSOV_C"
echo "-------------------------------------------------------------"
sed -n '/Ensembles + Decision Tree + External Rules Results/,/=====/p' "$file" | grep -E '[0-9]+'| grep -v '[a-zA-Z]' | tr -s " " | sed -e 's/^[ \t]*//' | awk -F' ' 'BEGIN{switch=1}{if (switch == 1) {v1=$1; v2=$2; v3=$3; v4=$4; switch=2;} else {printf "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", $1, $2, $3, $4, v1, v2, v3, v4; switch=1}}'
echo ""

echo "Ensembles + External Rules + Random Forest Results"
echo -e "Q3_ALL\tQ3_H\tQ3_E\tQ3_C\tSOV_ALL\tSOV_H\tSOV_E\tSOV_C"
echo "-------------------------------------------------------------"
sed -n '/Ensembles + External Rules + Random Forest Results/,/=====/p' "$file" | grep -E '[0-9]+'| grep -v '[a-zA-Z]' | tr -s " " | sed -e 's/^[ \t]*//' | awk -F' ' 'BEGIN{switch=1}{if (switch == 1) {v1=$1; v2=$2; v3=$3; v4=$4; switch=2;} else {printf "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", $1, $2, $3, $4, v1, v2, v3, v4; switch=1}}'
echo ""

echo "Ensembles + Random Forest Results"
echo -e "Q3_ALL\tQ3_H\tQ3_E\tQ3_C\tSOV_ALL\tSOV_H\tSOV_E\tSOV_C"
echo "-------------------------------------------------------------"
sed -n '/Ensembles + Random Forest Results/,/=====/p' "$file" | grep -E '[0-9]+'| grep -v '[a-zA-Z]' | tr -s " " | sed -e 's/^[ \t]*//' | awk -F' ' 'BEGIN{switch=1}{if (switch == 1) {v1=$1; v2=$2; v3=$3; v4=$4; switch=2;} else {printf "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", $1, $2, $3, $4, v1, v2, v3, v4; switch=1}}'
echo ""

echo "Ensembles + Random Forest + External Rules Results"
echo -e "Q3_ALL\tQ3_H\tQ3_E\tQ3_C\tSOV_ALL\tSOV_H\tSOV_E\tSOV_C"
echo "-------------------------------------------------------------"
sed -n '/Ensembles + Random Forest + External Rules Results/,/=====/p' "$file" | grep -E '[0-9]+'| grep -v '[a-zA-Z]' | tr -s " " | sed -e 's/^[ \t]*//' | awk -F' ' 'BEGIN{switch=1}{if (switch == 1) {v1=$1; v2=$2; v3=$3; v4=$4; switch=2;} else {printf "%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", $1, $2, $3, $4, v1, v2, v3, v4; switch=1}}'
