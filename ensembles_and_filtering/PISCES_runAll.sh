#!/bin/bash
# Author : Panayiotis Leontiou
# Since  : May 2020
# Version: 1.0
# Bugs   : No known bugs

TEST_FOLDER="./PISCES_test_pred"
TRAIN_FOLDER="./PISCES_train_pred"
WINDOW="15"
SVM_WIN="17"
filterOpt=( "2" "3" )
SCRIPTS="./q3_sov_scripts"

# Check if required scripts exist
declare -a REQUIRED_SCRIPTS=( "calc_Q3.py" "ensembles.py" "externalRules.py" "prepare_SVM_files.py" "runSOV.c" "sov.c" "train_SVM.py")
if [ ! -d "$SCRIPTS" ]; then
	echo "Error! $SCRIPTS directory could not be located."
	exit 1
else 
	for s in "${REQUIRED_SCRIPTS[@]}"
	do
		if [ ! -f "$SCRIPTS/$s" ]; then
			echo "Error! $SCRIPTS/$s file is missing."
			exit 1
		fi
	done
	[ -f "$SCRIPTS/runSOV" ] || gcc "$SCRIPTS/runSOV.c" -o "$SCRIPTS/runSOV"
	[ -f "$SCRIPTS/sov" ] || gcc "$SCRIPTS/sov.c" -o "$SCRIPTS/sov"
fi


echo "
                                                                              
                                                                              
PPPPPPPPPPPPPPPPP      SSSSSSSSSSSSSSS    SSSSSSSSSSSSSSS PPPPPPPPPPPPPPPPP   
P::::::::::::::::P   SS:::::::::::::::S SS:::::::::::::::SP::::::::::::::::P  
P::::::PPPPPP:::::P S:::::SSSSSS::::::SS:::::SSSSSS::::::SP::::::PPPPPP:::::P 
PP:::::P     P:::::PS:::::S     SSSSSSSS:::::S     SSSSSSSPP:::::P     P:::::P
  P::::P     P:::::PS:::::S            S:::::S              P::::P     P:::::P
  P::::P     P:::::PS:::::S            S:::::S              P::::P     P:::::P
  P::::PPPPPP:::::P  S::::SSSS          S::::SSSS           P::::PPPPPP:::::P 
  P:::::::::::::PP    SS::::::SSSSS      SS::::::SSSSS      P:::::::::::::PP  
  P::::PPPPPPPPP        SSS::::::::SS      SSS::::::::SS    P::::PPPPPPPPP    
  P::::P                   SSSSSS::::S        SSSSSS::::S   P::::P            
  P::::P                        S:::::S            S:::::S  P::::P            
  P::::P                        S:::::S            S:::::S  P::::P            
PP::::::PP          SSSSSSS     S:::::SSSSSSSS     S:::::SPP::::::PP          
P::::::::P          S::::::SSSSSS:::::SS::::::SSSSSS:::::SP::::::::P          
P::::::::P          S:::::::::::::::SS S:::::::::::::::SS P::::::::P          
PPPPPPPPPP           SSSSSSSSSSSSSSS    SSSSSSSSSSSSSSS   PPPPPPPPPP          
                                                                              

"

print_fold () {
	case $1 in 
		fold0)
			cat << 'EOF'
              ___     ___      _        ___                __   
    o O O    | __|   / _ \    | |      |   \              /  \  
   o         | _|   | (_) |   | |__    | |) |    ___     | () | 
  TS__[O]   _|_|_    \___/    |____|   |___/    |___|    _\__/  
 {======| _| """ | _|"""""| _|"""""| _|"""""| _|"""""| _|"""""| 
./o--000' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' 
EOF
			;;
		fold1)
			cat << "EOF"
              ___     ___      _        ___                _    
    o O O    | __|   / _ \    | |      |   \              / |   
   o         | _|   | (_) |   | |__    | |) |    ___      | |   
  TS__[O]   _|_|_    \___/    |____|   |___/    |___|    _|_|_  
 {======| _| """ | _|"""""| _|"""""| _|"""""| _|"""""| _|"""""| 
./o--000' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' 
EOF
			;;
		fold2)
			cat << "EOF"
              ___     ___      _        ___               ___   
    o O O    | __|   / _ \    | |      |   \             |_  )  
   o         | _|   | (_) |   | |__    | |) |    ___      / /   
  TS__[O]   _|_|_    \___/    |____|   |___/    |___|    /___|  
 {======| _| """ | _|"""""| _|"""""| _|"""""| _|"""""| _|"""""| 
./o--000' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' 
EOF
			;;
		fold3)
			cat << "EOF"
              ___     ___      _        ___               ____  
    o O O    | __|   / _ \    | |      |   \             |__ /  
   o         | _|   | (_) |   | |__    | |) |    ___      |_ \  
  TS__[O]   _|_|_    \___/    |____|   |___/    |___|    |___/  
 {======| _| """ | _|"""""| _|"""""| _|"""""| _|"""""| _|"""""| 
./o--000' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' 
EOF
			;;
		fold4)
			cat << "EOF"
              ___     ___      _        ___              _ _    
    o O O    | __|   / _ \    | |      |   \            | | |   
   o         | _|   | (_) |   | |__    | |) |    ___    |_  _|  
  TS__[O]   _|_|_    \___/    |____|   |___/    |___|    _|_|_  
 {======| _| """ | _|"""""| _|"""""| _|"""""| _|"""""| _|"""""| 
./o--000' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' 
EOF
			;;
		fold5)
			cat << "EOF"
              ___     ___      _        ___               ___   
    o O O    | __|   / _ \    | |      |   \             | __|  
   o         | _|   | (_) |   | |__    | |) |    ___     |__ \  
  TS__[O]   _|_|_    \___/    |____|   |___/    |___|    |___/  
 {======| _| """ | _|"""""| _|"""""| _|"""""| _|"""""| _|"""""| 
./o--000' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' 
EOF
			;;
		fold6)
			cat << "EOF"
              ___     ___      _        ___                __   
    o O O    | __|   / _ \    | |      |   \              / /   
   o         | _|   | (_) |   | |__    | |) |    ___     / _ \  
  TS__[O]   _|_|_    \___/    |____|   |___/    |___|    \___/  
 {======| _| """ | _|"""""| _|"""""| _|"""""| _|"""""| _|"""""| 
./o--000' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' 
EOF
			;;
		fold7)
			cat << "EOF"
              ___     ___      _        ___               ____  
    o O O    | __|   / _ \    | |      |   \             |__  | 
   o         | _|   | (_) |   | |__    | |) |    ___       / /  
  TS__[O]   _|_|_    \___/    |____|   |___/    |___|    _/_/_  
 {======| _| """ | _|"""""| _|"""""| _|"""""| _|"""""| _|"""""| 
./o--000' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' 
EOF
			;;
		fold8)
			cat << "EOF"
              ___     ___      _        ___               ___   
    o O O    | __|   / _ \    | |      |   \             ( _ )  
   o         | _|   | (_) |   | |__    | |) |    ___     / _ \  
  TS__[O]   _|_|_    \___/    |____|   |___/    |___|    \___/  
 {======| _| """ | _|"""""| _|"""""| _|"""""| _|"""""| _|"""""| 
./o--000' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' 
EOF
			;;
		fold9)
			cat << "EOF"
              ___     ___      _        ___               ___   
    o O O    | __|   / _ \    | |      |   \             / _ \  
   o         | _|   | (_) |   | |__    | |) |    ___     \_, /  
  TS__[O]   _|_|_    \___/    |____|   |___/    |___|    _/_/_  
 {======| _| """ | _|"""""| _|"""""| _|"""""| _|"""""| _|"""""| 
./o--000' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' 
EOF
			;;
		*)
			;;
	esac

}

print_SOV_score(){
	cat ./resultSOV.txt | grep -e 'SOV' | awk -F' ' '{sovAll += $3; sovH += $4; sovE += $5; sovC += $6} END {printf "\n    SOV_ALL    SOV_H      SOV_E      SOV_C\n    %.4f    %.4f    %.4f    %.4f\n", sovAll/NR, sovH/NR, sovE/NR, sovC/NR}'
}

get_filter_name(){
	case $1 in 
		"1")
			filter_name="SVM"
			;;
		"2")
			filter_name="Decision Tree"
			;;
		"3")
			filter_name="Random Forest"
			;;
		*)
			filter_name="Unknown Filter"
			;;
	esac
}

get_filter_abr(){
	case $1 in 
		"1")
			filter_abr="svm"
			;;
		"2")
			filter_abr="dtree"
			;;
		"3")
			filter_abr="rforest"
			;;
		*)
			filter_abr="unknown"
			;;
	esac
}

TEMP_FOLDER="./temp_runAll_PISCES"
RUN_ALL_FOLDER="./PISCES_runAll_out_files"
CROSS_VAL_FOLDER="./PISCES_cross_validation"
[ -d "$TEMP_FOLDER" ] || mkdir "$TEMP_FOLDER"
[ -d "$RUN_ALL_FOLDER" ] || mkdir "$RUN_ALL_FOLDER"
PRINT_CROSS_VAL=true

if [ "$PRINT_CROSS_VAL" = true ]; then
	echo "===================================================================="
	echo " >Cross Validation Results"
	echo "--------------------------------------------------------------------"
	for i in `ls "$CROSS_VAL_FOLDER"`
	do
		echo "$i"
		new_folder="$RUN_ALL_FOLDER/cross_val_res"
		[ -d "$new_folder" ] || mkdir "$new_folder"
		out_file=("$TEMP_FOLDER/$i""_cross_val.txt")
		for j in `ls "$CROSS_VAL_FOLDER/$i"`
		do
			echo "$CROSS_VAL_FOLDER/$i/$j"
		done > "$out_file"
		python "$SCRIPTS/ensembles.py" "$out_file" "$WINDOW" 1 "$new_folder/ens_pred.txt" "$new_folder/ens_sov.txt" "$new_folder/ens_weka.txt" 
		"$SCRIPTS/runSOV" "$new_folder/ens_sov.txt"
		print_SOV_score
		python "$SCRIPTS/calc_Q3.py" "$new_folder/ens_pred.txt"
		echo "--------------------------------------------------------------------"
	done
fi
echo "===================================================================="
echo ""
for i in `ls "$TEST_FOLDER"`
do
	print_fold $i
	new_folder="$RUN_ALL_FOLDER/$i""_results"
	[ -d "$new_folder" ] || mkdir "$new_folder"
	out_file=("$TEMP_FOLDER/$i""_files.txt")
	
	for j in `ls "$TEST_FOLDER/$i"`
	do
		echo "$TEST_FOLDER/$i/$j"
	done > "$out_file"
	echo "===================================================================="
	echo " >Ensembles Results"
	echo "--------------------------------------------------------------------"
	python "$SCRIPTS/ensembles.py" "$out_file" "$WINDOW" 1 "$new_folder/ensembles_pred.txt" "$new_folder/ensembles_sov.txt" "$new_folder/ensembles_weka.txt" > "$new_folder/ensembles_out.txt"
	"$SCRIPTS/runSOV" "$new_folder/ensembles_sov.txt"
	print_SOV_score
	python "$SCRIPTS/calc_Q3.py" "$new_folder/ensembles_pred.txt"
	echo "===================================================================="
	echo " >Ensembles + External Rules Results"
	echo "--------------------------------------------------------------------"
	python "$SCRIPTS/externalRules.py" "$new_folder/ensembles_pred.txt" "$new_folder/ens_rules_sov.txt" "$new_folder/ens_rules_pred.txt"
	"$SCRIPTS/runSOV" "$new_folder/ens_rules_sov.txt" 
	print_SOV_score
	python "$SCRIPTS/calc_Q3.py" "$new_folder/ens_rules_pred.txt"
	for filter in "${filterOpt[@]}"
	do
		get_filter_name $filter
		get_filter_abr $filter
		# echo "$filter $filter_name"
		train_preds=`ls "$TRAIN_FOLDER" | grep "$i" | head -n 1`
		echo "===================================================================="
		echo " >Ensembles + External Rules + $filter_name Results"
		echo "--------------------------------------------------------------------"
		python "$SCRIPTS/prepare_SVM_files.py" "$new_folder/ens_rules_pred.txt" "$TRAIN_FOLDER/$train_preds" "$SVM_WIN" "$new_folder/temp_svm_test.txt" "$new_folder/temp_svm_train.txt" 
		python "$SCRIPTS/train_SVM.py" "$new_folder/temp_svm_test.txt" "$new_folder/temp_svm_train.txt" "$SVM_WIN" "$new_folder/ens_rules_pred.txt" "$new_folder/ens_rules_$filter_abr""_pred.txt" "$new_folder/ens_rules_$filter_abr""_sov.txt" "$filter" > "$new_folder/ens_rules_$filter_abr""_out.txt"
		"$SCRIPTS/runSOV" "$new_folder/ens_rules_$filter_abr""_sov.txt"
		print_SOV_score
		python "$SCRIPTS/calc_Q3.py" "$new_folder/ens_rules_$filter_abr""_pred.txt"
		echo "===================================================================="
		echo " >Ensembles + $filter_name Results"
		echo "--------------------------------------------------------------------"
		python "$SCRIPTS/prepare_SVM_files.py" "$new_folder/ensembles_pred.txt" "$TRAIN_FOLDER/$train_preds" "$SVM_WIN" "$new_folder/temp_svm_test.txt" "$new_folder/temp_svm_train.txt" 
		python "$SCRIPTS/train_SVM.py" "$new_folder/temp_svm_test.txt" "$new_folder/temp_svm_train.txt" "$SVM_WIN" "$new_folder/ensembles_pred.txt" "$new_folder/ens_$filter_abr""_pred.txt" "$new_folder/ens_$filter_abr""_sov.txt" "$filter" > "$new_folder/ens_$filter_abr""_out.txt"
		"$SCRIPTS/runSOV" "$new_folder/ens_$filter_abr""_sov.txt"
		print_SOV_score
		python "$SCRIPTS/calc_Q3.py" "$new_folder/ens_$filter_abr""_pred.txt"
		echo "===================================================================="
		echo " >Ensembles + $filter_name + External Rules Results"
		echo "--------------------------------------------------------------------"
		python "$SCRIPTS/externalRules.py" "$new_folder/ens_$filter_abr""_pred.txt" "$new_folder/ens_$filter_abr""_rules_sov.txt" "$new_folder/ens_$filter_abr""_rules_pred.txt"
		"$SCRIPTS/runSOV" "$new_folder/ens_$filter_abr""_rules_sov.txt" 
		print_SOV_score
		python "$SCRIPTS/calc_Q3.py" "$new_folder/ens_$filter_abr""_rules_pred.txt"
	done
	echo "===================================================================="
	echo ""
	# exit 0
done

# Remove temp files
rm -rf "$TEMP_FOLDER"
rm resultSOV.txt
rm SOVinput.txt
